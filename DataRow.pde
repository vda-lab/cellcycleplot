class DataRow {
	String[] raw;
	int[] display_y;

	boolean isHidden = false;// excluded

	float[] logR;
	float[] copyNumber;
	int position;

	int domain; //TODO
	float dy_domain;



	DataRow (String[] s){
		logR = new float[_cell_stages];
		copyNumber = new float[_cell_stages];
		display_y = new int[_cell_stages]; //position and domain separate

		position = Integer.parseInt(s[1]);
		for(int i = 0; i< _cell_stages; i++){
			logR[i] = Float.parseFloat(s[2+i]);
			copyNumber[i] = Float.parseFloat(s[8+i]);
			
		}
	} 

	DataRow(int pos){
		this.position = pos;
		logR = new float[_cell_stages];
		copyNumber = new float[_cell_stages];
		display_y = new int[_cell_stages];
	}


	String toString(){
		return "position:"+position;
	}
}
