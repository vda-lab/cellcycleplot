//hovering
boolean hovering_l_handle = false;
boolean hovering_r_handle = false;


boolean selected_l_handle = false;
boolean selected_r_handle = false;
boolean selected_middle = false;
int m_pressed_x = 0;
int m_pressed_y = 0;

//color interaction
boolean color_picked = false;
int picked_color_index = -1;

//hightlight in scatter plot
boolean region_selected = false;
boolean region_drag = false;
int region_left, region_right;
int region_left_bp, region_right_bp;


boolean isStacking = false; /////stacking mode


void keyPressed(){
	if(!filesSelected){
		//sample count
		if(sampleCount_highlight){
			if((key == BACKSPACE)||(key == DELETE)){
			    if(sample_count_input.length() >0){
			        sample_count_input = sample_count_input.substring(0, sample_count_input.length()-1);
			    }
			}else if((key >='0') && (key <= '9')){
			    sample_count_input += key;
			}else if((key == ENTER)||(key==RETURN)){
			    if(sample_count_input.length() >0){
			        numOfSamples = Integer.parseInt(sample_count_input);
			        setupDataLoadingPage();
			        sampleCount_highlight = false;
			    }
			}
		}else if(sampleName_highlight){
			if((key == BACKSPACE)||(key == DELETE)){
			    if(sample_names[sampleName_hightlight_index].length() >0){
			        sample_names[sampleName_hightlight_index] = sample_names[sampleName_hightlight_index].substring(0, sample_names[sampleName_hightlight_index].length()-1);
			    }
			}else if((key >='.') && (key <= 'z')){
			    sample_names[sampleName_hightlight_index] += key;
			}else if((key == ENTER)||(key==RETURN)){
			    sampleName_highlight = false;
			    sampleName_hightlight_index = -1;
			}
		}


	}else if(!isLoadingData && !isPreprocessing){
		if(key == CODED){
			if(keyCode == RIGHT){
				_selected_chr_index = min(_selected_chr_index+1, chrKeyArray.size()-1);
				update_new_chromosome();
			}else if(keyCode == LEFT){
				_selected_chr_index = max(_selected_chr_index-1, 0);
				update_new_chromosome();
			}
		}

		if(output_note_active){
			if((key == BACKSPACE)||(key == DELETE)){
			    if(output_note_input.length() >0){
			        output_note_input = output_note_input.substring(0, output_note_input.length()-1);
			    }
			}else if(((key >='.') && (key <= 'z')) || (key == ' ')){
			    output_note_input += key;
			}else if((key == ENTER)||(key==RETURN)){
			    output_note_active = false;
			    //perhaps save test by this.... todo 
			}
			updateGUI();
			loop();
		}else if(output_file_name_active){
			if((key == BACKSPACE)||(key == DELETE)){
			    if(output_file_url.length() >0){
			        output_file_url = output_file_url.substring(0, output_file_url.length()-1);
			    }
			}else if(((key >='.') && (key <= 'z')) || (key == ' ')){
			    output_file_url += key;
			}else if((key == ENTER)||(key==RETURN)){
			    output_file_name_active = false;
			}
			updateGUI();
			loop();
		}
	}
}

void mouseMoved(){
	if(!isLoadingData && !isPreprocessing){
		saved_output = false;

		if(_chr_dy < mouseY && mouseY < _chr_dy+_karyotype_margin+_karyotype_height){
			//check distance
			float dist_left = abs(mouseX - left_handle_x);
			float dist_right = abs(mouseX - right_handle_x);

			if(dist_left < 10 && mouseX < left_handle_x){
				hovering_l_handle = true;
				cursor(HAND);
			}else if(dist_right < 10 && mouseX  > right_handle_x){
				hovering_r_handle = true;
				cursor(HAND);
			}else if( left_handle_x +5  < mouseX  && mouseX < right_handle_x -5){
				cursor(HAND);
			}else{
				hovering_l_handle = false;
				hovering_r_handle = false;
				cursor(ARROW);
			}
			loop();
		}else if(_scatter_dy < mouseY && mouseY <_scatter_dy+_scatter_height){
			cursor(CROSS);
			_draw_counter = 0;
			loop();
		}else{
			hovering_l_handle = false;
			hovering_r_handle = false;
			cursor(ARROW);
		}

	}
	//keep updating the view
	_draw_counter = 0;
}

void mousePressed(){
	if(!isLoadingData && !isPreprocessing){
		selected_l_handle = false;
		selected_r_handle = false;
		selected_middle = false;
		m_pressed_x = mouseX;
		m_pressed_y = mouseY;

		color_picked = false;
		picked_color_index = -1;

		region_drag = false;

		//gui area
		if(_cont_dy < mouseY && mouseY < (_cont_dy+_cont_height)){
			//legend area GUI
			//check color pallet 
			for(int i = 0; i < color_pallet.length; i++){
				if(color_pallet[i].contains(mouseX, mouseY- _cont_dy)){
					//selected
					picked_color_index = i;
					color_picked = true;
				}
			}
		}else if(_chr_dy < mouseY && mouseY < _chr_dy+_karyotype_margin+_karyotype_height){
			if(hovering_l_handle){
				selected_l_handle = true;
			}else if(hovering_r_handle){
				selected_r_handle = true;
			}else if( left_handle_x +5  < mouseX  && mouseX < right_handle_x -5){
				//middle
				selected_middle = true;
			}
		}else if(_scatter_dy < mouseY && mouseY <_scatter_dy+_scatter_height){
			if(!region_selected){
				println("debug: mousePressed");
				//set left
				region_left = mouseX;
				region_left_bp = getBpPosition(mouseX);//round(map(mouseX, _scatter_dx, _scatter_dx+_scatter_width, _start_pos,_end_pos));

				//intial value
				region_right = mouseX;
				region_right_bp = region_left_bp;
				region_drag = true;	
			}
		}
	}
}

void mouseDragged(){
	if(!isLoadingData && !isPreprocessing){
		region_selected = false;

		if(selected_l_handle){
			//update left position
			update_left_handle(mouseX);
			_draw_counter = 0;
			updateDomainView();
			redraw();
		}else if(selected_r_handle){
			//update right position
			update_right_handle(mouseX);
			_draw_counter = 0;
			updateDomainView();
			redraw();
		}else if(selected_middle){
			update_middle(m_pressed_x, mouseX);
		    m_pressed_x = mouseX;
		    updateDomainView();
			redraw();
		}else if(color_picked){
			_draw_counter = 0;
			redraw();
		}else if(region_drag){
			region_selected = true;
			// println("debug: mouseDragged");
			//update right
			region_right = mouseX;
			region_right_bp = getBpPosition(mouseX); //round(map(mouseX, _scatter_dx, _scatter_dx+_scatter_width, _start_pos,_end_pos));
			_draw_counter = 0;
			loop();
		}
	}
}


void mouseReleased(){
	if(!filesSelected){
		//update when clicked outside
		if(sampleCount_highlight){
			if(sample_count_input.length() >0){
			    numOfSamples = Integer.parseInt(sample_count_input);
			    setupDataLoadingPage();
			}
		}
		sampleCount_highlight = false;
		sampleName_highlight = false;
		sampleName_hightlight_index = -1;
		sample_selected_index = -1;
		if(sampleCount_rect.contains(mouseX, mouseY)){
			//sampleCount
			sampleCount_highlight = true;
		}else if(load_btn.contains(mouseX, mouseY)){
			//check if every field is filled
			boolean missingValue = false;
			String msg ="";
			for(int i = 0; i < sample_names.length; i++){
				if(sample_names[i].equals("")){
					msg += "*Sample name is missing for "+(i+1)+"\n";
					missingValue = true;
				}
			}
			for(int i = 0; i< sample_log_file.length; i++){
				if(sample_log_file[i] == null){
					msg += "*LogR file for "+(i+1)+" is missing \n";
					missingValue = true;
				}
			}
			for(int i = 0; i < sample_cn_file.length; i++){
				if(sample_cn_file[i] == null){
					msg += "*Copy Number file for "+(i+1)+" is missing \n";
					missingValue = true;
				}
			}

			if(missingValue){
				try {
				  //run a bit of code
				  javax.swing.JOptionPane.showMessageDialog(null, msg);
				} catch (Exception e) {
				   println("error on load_btn click:"+e);
				} 
			}else{
				//start loading
				println("start loading");
				filesSelected = true;
				isLoadingData = true;
				Runnable loadingFile = new FileLoader();
				new Thread(loadingFile).start();
			}
		}else if(domain_file_btn.contains(mouseX, mouseY)){
			//select domain file
			selectInput("Select your domain file:", "selectFile_domain");
		}else if(conf_file_btn.contains(mouseX, mouseY)){
			//configuration file
			selectInput("Select a configuration file:", "selectFile_conf");

		}else if(conf_save_btn.contains(mouseX, mouseY)){
			selectFolder("Select a folder to save the configuration file", "select_conf_folder");

		}else{
			//check rest of  buttons
			for(int i = 0; i< dl_rectangles.size(); i++){
				Rectangle[] rects = dl_rectangles.get(i);
				for(int j = 0; j < rects.length; j++){
					Rectangle r = rects[j];
					if(r.contains(mouseX, mouseY)){
						switch(j){
							case 0:
								//sample name
								sampleName_highlight = true;
								sampleName_hightlight_index = i;
								break;
							case 2:
								//logR load
								sample_selected_index = i;
								selectInput("Select your logR file:", "selectFile_logR");
								break;
							case 4:
								//cn load
								sample_selected_index = i;
								selectInput("Select your Copy Number file:", "selectFile_cn");
								break;
						}
						return;
					}
				}
			}
		}
	}else if(!isLoadingData && !isPreprocessing){
		if(color_picked){
			if(_cont_dy < mouseY && mouseY < (_cont_dy+_cont_height)){
				// GUI
				//sample buttons
				for(int i = 0; i< samples.length; i++){
					Rectangle r = sample_btns[i];
					if(r.contains(mouseX, mouseY - _cont_dy)){
						Sample s = samples[i];
						s.isShowing = true;
						//set color
						s.stroke_color = _color_array[picked_color_index];
						//reset
						color_picked = false;
						picked_color_index = -1;
						_draw_counter = 0;
						updateGUI();
						s.redraw();
						loop();
						return;
					}
				}
				//domain button
				for(int i = 0; i< domain_btns.length; i++){
					Rectangle r = domain_btns[i];
					if(r.contains(mouseX, mouseY- _cont_dy)){
						if(i == 0){ //late
							_late_domain_color = _color_array[picked_color_index];
							showing_late_domain = true;
						}else if(i == 1){ //early
							_early_domain_color = _color_array[picked_color_index];
							showing_early_domain = true;
						}
						//reset
						color_picked = false;
						picked_color_index = -1;
						_draw_counter = 0;
						updateGUI();
						updateDomainView();
						loop();
						return;
					}
				}
				
			}
		}else if(region_drag){
			println("debug: mouseReleased");
			//update right
			region_right = mouseX;
			region_right_bp = getBpPosition(mouseX);//round(map(mouseX, _scatter_dx, _scatter_dx+_scatter_width, _start_pos,_end_pos));
			//chect left and right
			if(region_right < region_left){
				//swap
				int temp = region_right;
				int temp_bp = region_right_bp;
				region_right = region_left;
				region_right_bp = region_left_bp;
				region_left = temp;
				region_left_bp = temp_bp;
			}

			//update out put view
			updateGUI();
			return;
		}

		output_note_active = false;
		output_file_name_active = false;

		if(mouseY  < _header_height){
			for(int i = 0; i< chr_btns.length; i++){
				Rectangle rect = chr_btns[i];
				if(rect.contains(mouseX, mouseY)){
					_selected_chr_index = i;
					update_new_chromosome();
					return;
				}
			}
		}else if(_scatter_dy < mouseY && mouseY <_scatter_dy+_scatter_height){
			//scatter plot
			if(region_selected){
				if(region_left  > mouseX || mouseX  > region_right){
					println("debug: mouseClicked");
					//unselect
					region_selected = false;
					_draw_counter = 0;
					loop();
					return;
				}
			}
		}else if(_cont_dy < mouseY && mouseY < (_cont_dy+_cont_height)){
			//legend area GUI
			//data type buttons
			for(int i= 0; i< datatype_btns.length; i++){
				Rectangle r = datatype_btns[i];
				if(r.contains(mouseX, mouseY- _cont_dy)){
					// println("click!!! "+mouseX+"  "+mouseY);
					switch(i){
						case 0:
							//seg
							showingSegData = !showingSegData;
							break;
						case 1:
							//raw
							showingRawData = !showingRawData;
							break;
					}
					_draw_counter = 0;
					updateGUI();
					loop();
					return;
				}
			}
			//sample buttons
			for(int i = 0; i< samples.length; i++){
				Rectangle r = sample_btns[i];
				if(r.contains(mouseX, mouseY - _cont_dy)){
					Sample s = samples[i];
					s.isShowing = !s.isShowing;
					_draw_counter = 0;
					update_stacking(); // stacking
					redraw_seg();
					updateGUI();
					loop();
					return;
				}
			}
			//domain
			if(hasDomainFile){
				for(int i = 0; i< domain_btns.length; i++){
					Rectangle r = domain_btns[i];
					if(r.contains(mouseX, mouseY- _cont_dy)){
						if(i == 0){ //late
							showing_late_domain = !showing_late_domain;
						}else if(i == 1){ //early
							showing_early_domain = !showing_early_domain;
						}
						//reset
						updateGUI();
						updateDomainView();
						loop();
						return;
					}
				}
			}

			//representation
			for(int i = 0; i < encoding_labels.length; i++){
				Rectangle r = encoding_btns[i];
				if(r.contains(mouseX, mouseY- _cont_dy)){
					//hit
					for(int j = 0; j < encoding_selected.length; j++){
						if(i == j){
							encoding_selected[j] = true;
						}else{
							encoding_selected[j] = false;
						}
					}
					_draw_counter = 0;
					updateGUI();
					for(Sample s:samples){
						s.redraw();
					}
					loop();
					return;
				}
			}

			//stacking options
			if(stacking_btns[0].contains(mouseX, mouseY - _cont_dy)){
				isStacking = !isStacking;
				_draw_counter = 0;
				update_stacking(); // stacking
				redraw_seg();
				updateGUI();
				loop();
				return;
			}

			//output
			if(output_file_name.contains(mouseX, mouseY - _cont_dy)){
				output_note_active = false;
				output_file_name_active = true;
			}else if(output_note_rect.contains(mouseX, mouseY - _cont_dy)){
				output_note_active = true;
				output_file_name_active = false;
				
			}else if(output_btn.contains(mouseX, mouseY - _cont_dy)){
				//save
				if(region_selected){
					save_note();
					output_note_active = false;
					output_file_name_active = false;
					output_note_input = "";
				}
			}


			//else
			updateGUI();
			loop();
			return;
		}
	}
	//reset
	color_picked = false;
	picked_color_index = -1;
}

// void mouseClicked(){
// 	// if(!filesSelected){
// 	// 	//update when clicked outside
// 	// 	if(sampleCount_highlight){
// 	// 		if(sample_count_input.length() >0){
// 	// 		    numOfSamples = Integer.parseInt(sample_count_input);
// 	// 		    setupDataLoadingPage();
// 	// 		}
// 	// 	}
// 	// 	sampleCount_highlight = false;
// 	// 	sampleName_highlight = false;
// 	// 	sampleName_hightlight_index = -1;
// 	// 	sample_selected_index = -1;
// 	// 	if(sampleCount_rect.contains(mouseX, mouseY)){
// 	// 		//sampleCount
// 	// 		sampleCount_highlight = true;
// 	// 	}else if(load_btn.contains(mouseX, mouseY)){
// 	// 		//check if every field is filled
// 	// 		boolean missingValue = false;
// 	// 		String msg ="";
// 	// 		for(int i = 0; i < sample_names.length; i++){
// 	// 			if(sample_names[i].equals("")){
// 	// 				msg += "*Sample name is missing for "+(i+1)+"\n";
// 	// 				missingValue = true;
// 	// 			}
// 	// 		}
// 	// 		for(int i = 0; i< sample_log_file.length; i++){
// 	// 			if(sample_log_file[i] == null){
// 	// 				msg += "*LogR file for "+(i+1)+" is missing \n";
// 	// 				missingValue = true;
// 	// 			}
// 	// 		}
// 	// 		for(int i = 0; i < sample_cn_file.length; i++){
// 	// 			if(sample_cn_file[i] == null){
// 	// 				msg += "*Copy Number file for "+(i+1)+" is missing \n";
// 	// 				missingValue = true;
// 	// 			}
// 	// 		}

// 	// 		if(missingValue){
// 	// 			try {
// 	// 			  //run a bit of code
// 	// 			  javax.swing.JOptionPane.showMessageDialog(null, msg);
// 	// 			} catch (Exception e) {
// 	// 			   println("error on load_btn click:"+e);
// 	// 			} 
// 	// 		}else{
// 	// 			//start loading
// 	// 			println("start loading");
// 	// 			filesSelected = true;
// 	// 			isLoadingData = true;
// 	// 			Runnable loadingFile = new FileLoader();
// 	// 			new Thread(loadingFile).start();
// 	// 		}
// 	// 	}else if(domain_file_btn.contains(mouseX, mouseY)){
// 	// 		//select domain file
// 	// 		selectInput("Select your domain file:", "selectFile_domain");
// 	// 	}else if(conf_file_btn.contains(mouseX, mouseY)){
// 	// 		//configuration file
// 	// 		selectInput("Select a configuration file:", "selectFile_conf");

// 	// 	}else if(conf_save_btn.contains(mouseX, mouseY)){
// 	// 		selectFolder("Select a folder to save the configuration file", "select_conf_folder");

// 	// 	}else{
// 	// 		//check rest of  buttons
// 	// 		for(int i = 0; i< dl_rectangles.size(); i++){
// 	// 			Rectangle[] rects = dl_rectangles.get(i);
// 	// 			for(int j = 0; j < rects.length; j++){
// 	// 				Rectangle r = rects[j];
// 	// 				if(r.contains(mouseX, mouseY)){
// 	// 					switch(j){
// 	// 						case 0:
// 	// 							//sample name
// 	// 							sampleName_highlight = true;
// 	// 							sampleName_hightlight_index = i;
// 	// 							break;
// 	// 						case 2:
// 	// 							//logR load
// 	// 							sample_selected_index = i;
// 	// 							selectInput("Select your logR file:", "selectFile_logR");
// 	// 							break;
// 	// 						case 4:
// 	// 							//cn load
// 	// 							sample_selected_index = i;
// 	// 							selectInput("Select your Copy Number file:", "selectFile_cn");
// 	// 							break;
// 	// 					}
// 	// 					return;
// 	// 				}
// 	// 			}
// 	// 		}
// 	// 	}
// 	}else if(!isLoadingData && !isPreprocessing){
// 		// output_note_active = false;
// 		// output_file_name_active = false;

// 		// if(mouseY  < _header_height){
// 		// 	for(int i = 0; i< chr_btns.length; i++){
// 		// 		Rectangle rect = chr_btns[i];
// 		// 		if(rect.contains(mouseX, mouseY)){
// 		// 			_selected_chr_index = i;
// 		// 			update_new_chromosome();
// 		// 			return;
// 		// 		}
// 		// 	}
// 		// }else if(_scatter_dy < mouseY && mouseY <_scatter_dy+_scatter_height){
// 		// 	//scatter plot
// 		// 	if(region_selected){
// 		// 		if(region_left  > mouseX || mouseX  > region_right){
// 		// 			println("debug: mouseClicked");
// 		// 			//unselect
// 		// 			region_selected = false;
// 		// 			_draw_counter = 0;
// 		// 			loop();
// 		// 			return;
// 		// 		}
// 		// 	}
// 		// }else if(_cont_dy < mouseY && mouseY < (_cont_dy+_cont_height)){
// 		// 	//legend area GUI
// 		// 	//data type buttons
// 		// 	for(int i= 0; i< datatype_btns.length; i++){
// 		// 		Rectangle r = datatype_btns[i];
// 		// 		if(r.contains(mouseX, mouseY- _cont_dy)){
// 		// 			// println("click!!! "+mouseX+"  "+mouseY);
// 		// 			switch(i){
// 		// 				case 0:
// 		// 					//seg
// 		// 					showingSegData = !showingSegData;
// 		// 					break;
// 		// 				case 1:
// 		// 					//raw
// 		// 					showingRawData = !showingRawData;
// 		// 					break;
// 		// 			}
// 		// 			_draw_counter = 0;
// 		// 			updateGUI();
// 		// 			loop();
// 		// 			return;
// 		// 		}
// 		// 	}
// 		// 	//sample buttons
// 		// 	for(int i = 0; i< samples.length; i++){
// 		// 		Rectangle r = sample_btns[i];
// 		// 		if(r.contains(mouseX, mouseY - _cont_dy)){
// 		// 			Sample s = samples[i];
// 		// 			s.isShowing = !s.isShowing;
// 		// 			_draw_counter = 0;
// 		// 			update_stacking(); // stacking
// 		// 			redraw_seg();
// 		// 			updateGUI();
// 		// 			loop();
// 		// 			return;
// 		// 		}
// 		// 	}
// 		// 	//domain
// 		// 	for(int i = 0; i< domain_btns.length; i++){
// 		// 		Rectangle r = domain_btns[i];
// 		// 		if(r.contains(mouseX, mouseY- _cont_dy)){
// 		// 			if(i == 0){ //late
// 		// 				showing_late_domain = !showing_late_domain;
// 		// 			}else if(i == 1){ //early
// 		// 				showing_early_domain = !showing_early_domain;
// 		// 			}
// 		// 			//reset
// 		// 			updateGUI();
// 		// 			updateDomainView();
// 		// 			loop();
// 		// 			return;
// 		// 		}
// 		// 	}

// 		// 	//representation
// 		// 	for(int i = 0; i < encoding_labels.length; i++){
// 		// 		Rectangle r = encoding_btns[i];
// 		// 		if(r.contains(mouseX, mouseY- _cont_dy)){
// 		// 			//hit
// 		// 			for(int j = 0; j < encoding_selected.length; j++){
// 		// 				if(i == j){
// 		// 					encoding_selected[j] = true;
// 		// 				}else{
// 		// 					encoding_selected[j] = false;
// 		// 				}
// 		// 			}
// 		// 			_draw_counter = 0;
// 		// 			updateGUI();
// 		// 			for(Sample s:samples){
// 		// 				s.redraw();
// 		// 			}
// 		// 			loop();
// 		// 			return;
// 		// 		}
// 		// 	}

// 		// 	//stacking options
// 		// 	if(stacking_btns[0].contains(mouseX, mouseY - _cont_dy)){
// 		// 		isStacking = !isStacking;
// 		// 		_draw_counter = 0;
// 		// 		update_stacking(); // stacking
// 		// 		redraw_seg();
// 		// 		updateGUI();
// 		// 		loop();
// 		// 		return;
// 		// 	}

// 		// 	//output
// 		// 	if(output_file_name.contains(mouseX, mouseY - _cont_dy)){
// 		// 		output_note_active = false;
// 		// 		output_file_name_active = true;
// 		// 	}else if(output_note_rect.contains(mouseX, mouseY - _cont_dy)){
// 		// 		output_note_active = true;
// 		// 		output_file_name_active = false;
				
// 		// 	}else if(output_btn.contains(mouseX, mouseY - _cont_dy)){
// 		// 		//save
// 		// 		if(region_selected){
// 		// 			save_note();
// 		// 			output_note_active = false;
// 		// 			output_file_name_active = false;
// 		// 			output_note_input = "";
// 		// 		}
// 		// 	}


// 		// 	//else
// 		// 	updateGUI();
// 		// 	loop();
// 		// 	return;
// 		// }
// 	}
// }

void selectFile_logR(File selection){
	if(selection == null){
		sample_log_file[sample_selected_index] = null;
	}else{
		sample_log_file[sample_selected_index] = selection;
	}
	//reset
	sample_selected_index = -1;
}

void selectFile_cn(File selection){
	if(selection == null){
		sample_cn_file[sample_selected_index] = null;
	}else{
		sample_cn_file[sample_selected_index] = selection;
	}
	//reset
	sample_selected_index = -1;
}

void selectFile_domain(File selection){
	if(selection == null){
		domain_file = null;
	}else{
		domain_file = selection;
	}
}

void selectFile_conf(File selection){
	if(selection == null){
		//do nothing
	}else{
		//parse and load files
		conf_file = selection;
		String[] lines = loadStrings(selection);
		int sampleLineCounter = 0;
		for(String line : lines){
			String[] s = split(line, TAB);
			if(s[0].trim().equals("#sample_count")){
				int sampleCount = Integer.parseInt(s[1]);
				numOfSamples = sampleCount;
				sample_count_input = ""+numOfSamples;
				setupDataLoadingPage();
			}else if(s[0].trim().equals("#domain")){
				if(s.length >1 && s[1] != ""){
					domain_file = new File(s[1].trim());
					println("debug:domain file:"+domain_file.getAbsolutePath());
				}
			}else if(s[0].trim().equals("#sample_name")){
				//header
			}else{
				sample_names[sampleLineCounter] = s[0].trim();
				sample_log_file[sampleLineCounter] = new File(s[1].trim());
				sample_cn_file[sampleLineCounter] = new File(s[2].trim());
				sampleLineCounter++;
			}
		}
	}
}


void select_conf_folder(File selection){
	String path = selection.getAbsolutePath();
	PrintWriter output = createWriter(path+"/"+conf_save_name+".txt");
	output.println("#sample_count\t"+numOfSamples);
	if(domain_file != null){
		output.println("#domain\t"+domain_file.getAbsolutePath());
	}
	output.println("#sample_name\tlogR\tcopy_number");
	for(int i = 0; i < numOfSamples; i++){
		String line = sample_names[i]+"\t";
		line += sample_log_file[i].getAbsolutePath() +"\t";
		line += sample_cn_file[i].getAbsolutePath();
		output.println(line);
	}


	output.flush();
	output.close();
}

int getBpPosition(int mousePos){
	return round(map(mouseX, _top_plot_x, _top_plot_x+_top_plot_width, _start_pos,_end_pos));
}
//wrtie to a file
void save_note(){
	println("debug:seve_note():");
	String chr = chrKeyArray.get(_selected_chr_index).replace("hs", "chr");
	String output = chr+"\t"+region_left_bp+"\t"+region_right_bp+"\t"+output_note_input;
	PrintWriter writer = null;
	// writer = createWriter(output_file_url+".txt");
	try{
		writer = new PrintWriter(new FileOutputStream(new File(sketchPath(output_file_url+".txt")), true));
	}catch(IOException e){

	}
	writer.println(output);
	writer.flush();
	writer.close();
	
	saved_output = true;
}


