
//karyotype
HashMap<String, Chromosome> refChrMap;
ArrayList<String> chrKeyArray;

Sample[] samples;
int _cell_stages = -1;

Chromosome _selected_chr = null;
int _selected_chr_index = 0;
int _start_pos = 0; //current selected range
int _end_pos = 0;

float _min_logR = -2.0f;
float _max_logR = 2.0f;

float _min_copyNumber = 0f;
float _max_copyNumber = 6.0f;

float left_handle_x = 0;
float right_handle_x = 0;

//domain data


void initData(){
	//karyotype
	refChrMap = new HashMap<String, Chromosome>();
	chrKeyArray = new ArrayList<String>();

}

void addSample(int i){
	if(samples == null){
		samples = new Sample[numOfSamples];
		_cell_stages = numOfSamples;
	}
	samples[i] = new Sample(i, sample_names[i]);
}


void setupSamples(String header){
	String[] s = split(header, TAB);
	//chromosome, position, data
	_cell_stages = (s.length -2)/2;
	println("debug: num of cell stages ="+_cell_stages);

	samples = new Sample[_cell_stages];
	for(int i = 0; i< _cell_stages; i++){
		String sample_label = s[i+2].replace("_logR", "");
		samples[i] = new Sample(i, sample_label);
	}
}

void preprocess(){
	//create PGraphics
	for(int i = 0; i<samples.length; i++){
		samples[i].setupDisplay();
		int colour =0;
		switch(i){
			case 0: //G1
				colour =  _color_g_seq[0];
				break;
			case 1: //G4
				colour = _color_g_seq[1];
				break;
			case 2: //S1
				colour = _color_b_seq[0];
				break;
			case 3: //S2
				colour = _color_b_seq[1];
				break;
			case 4: //S4
				colour = _color_b_seq[2];
				break;
			case 5: //S6
				colour = _color_b_seq[3];
				break;
			default:
				colour = 180;
				break;
		}
		//assign
		samples[i].stroke_color = colour;
	}

	//set initial parameters
	_selected_chr = (Chromosome) refChrMap.get(chrKeyArray.get(_selected_chr_index));
	_start_pos = 0;
	_end_pos =_selected_chr.end;
	resetHandles();

	//setupDomainView
	setupDomainView();
	updateDomainView();
	
	//karyotype view  
	preprocessKaryotype();
	setupKaryotypeView();
	updateKaryotypeView();

	//GUI
	setupGUI();
	updateGUI();


	isPreprocessing = false;
}

//find centromere
void preprocessKaryotype(){
	//find centromere start, center, end
	for(int i = 0; i<chrKeyArray.size(); i++){
		Chromosome chr = (Chromosome)refChrMap.get(chrKeyArray.get(i));
		ArrayList<Cytoband> bands = chr.cytobands;
		for(int j = 0; j<bands.size(); j++){
			Cytoband band = bands.get(j);
			if(band.isCentromere){
				if(chr.startAcen == null){
					chr.startAcen = band;
				}else{
					chr.endAcen = band;
				}
			}
			//beginning
			if(j==0){
				band.is_p_end = true;
			}else if(j == chrKeyArray.size()-1){
				band.is_q_end = true;
			}
		}
	}
}

//new chromosome is selected
void update_new_chromosome(){
	_selected_chr = (Chromosome) refChrMap.get(chrKeyArray.get(_selected_chr_index));
	_start_pos = 0;
	_end_pos =_selected_chr.end;
	left_handle_x = _karyotype_x1;
	right_handle_x = _karyotype_x2;
	for(Sample s: samples){
		//reset counters
		s.redraw();
	}
	//karyotype
	redrawKaryotype();
	//update domain
	updateDomainView();

	loop();
}

void resetHandles(){
	left_handle_x = _top_plot_x;
	right_handle_x = _top_plot_x + _top_plot_width;
}


//interaction
void update_left_handle(int m_x){
	//update handle position
	left_handle_x = constrain(m_x, _karyotype_x1, right_handle_x -_MARGIN);
	//update chromosome position
	_start_pos = round(map(left_handle_x, _karyotype_x1, _karyotype_x2, _selected_chr.start, _selected_chr.end));
	for(Sample s: samples){
		//reset counters
		s.redraw();
	}
}

void update_right_handle(int m_x){
	right_handle_x = constrain(m_x, left_handle_x+_MARGIN, _karyotype_x2);
	_end_pos = constrain(round(map(right_handle_x, _karyotype_x1, _karyotype_x2, _selected_chr.start, _selected_chr.end)),_selected_chr.start, _selected_chr.end) ;
	for(Sample s: samples){
		//reset counters
		s.redraw();
	}
}

void update_middle(int m_pressed, int m_x){
	int diff = m_x - m_pressed ;
	left_handle_x = constrain(left_handle_x+diff, _karyotype_x1, right_handle_x -_MARGIN);
	_start_pos = round(map(left_handle_x, _karyotype_x1, _karyotype_x2, _selected_chr.start, _selected_chr.end));
	
	right_handle_x = constrain(right_handle_x+diff, left_handle_x+_MARGIN, _karyotype_x2);
	_end_pos = constrain(round(map(right_handle_x, _karyotype_x1, _karyotype_x2, _selected_chr.start, _selected_chr.end)),_selected_chr.start, _selected_chr.end);
	
	// println("debug: diff ="+diff+"  pressed="+m_pressed+"  mouseX="+m_x);
	for(Sample s: samples){
		//reset counters
		s.redraw();
	}
}

//update stacking for each sample
void update_stacking(){
	// println("debug: update_stacking()");
	float runningY = 0;
	float stacking_gap = seg_line_weight;
	//adjust starting Y
	int showing_sample_count = 0;
	for(int i = 0; i < samples.length; i++){
		Sample s = samples[i];
		if(s.isShowing){
			showing_sample_count ++;
		}
	}
	runningY  = (seg_line_weight*showing_sample_count)/2 * -1;

	for(int i = 0; i < samples.length; i++){
		Sample s = samples[i];
		if(s.isShowing){
			s.stacking = runningY;
			runningY += stacking_gap;
		}else{
			s.stacking = 0;
		}
	}

	// println("debug: update_stacking() _____  end");
}


