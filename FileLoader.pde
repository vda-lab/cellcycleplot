long _total_data = 0;
long _current_data = 0;
int _current_percentage = 0;

String _current_load_message = "";
String _file_url = "AllRawData.txt"; 
String _seg_file_url = "AllSegmentationData.txt";

int _line_counter = 0;


boolean isTestMode = false;

class FileLoader implements Runnable{
	void run(){
		initData();
		//load karyotype data
		loadKaryotype();
		println("karyotype data loaded -- chr count ="+chrKeyArray.size()+":"+ chrKeyArray);

		if(isTestMode){
			//deprecated
		}else{
			//check the _total_data
			calculateTotalData();
			//load by samples
			for(int i = 0; i < sample_log_file.length; i++){
				File f = sample_log_file[i];
				loadLogR(f, i);
				f = sample_cn_file[i];
				loadCN(f, i);
			}

			if(domain_file != null){
				loadDomainFile();
			}
			println("debug: data loaded = "+_current_data);
				
		}


		//debug	

		// for(String key : chrKeyArray){
		// 	Chromosome chr =(Chromosome)refChrMap.get(key);
		// 	println("debug:"+chr.label);
		// }
		finishLoading();
	}
}

void calculateTotalData(){
	_total_data = 0;
	for(File f: sample_log_file){
		_total_data += f.length();
	}
	for(File f: sample_cn_file){
		_total_data += f.length();
	}
	println("debug: total file size is "+_total_data);
}

void finishLoading(){
	isLoadingData = false;
	isPreprocessing = true;
	println("debug: ----- finished loading ----------");
	//setup display parameters
	setupDisplay();
	preprocess();  //DataSet
}

void loadKaryotype(){
	File f = new File(dataPath("karyotype.human.hg19.txt"));
    try{
        BufferedReader reader = createReader(f);
        String line = null;
        while ((line = reader.readLine()) != null) {
            parseKaryotypetData(line);
        }
    } catch (IOException e) {
        e.printStackTrace();
    }
}
void parseKaryotypetData(String line){
	String[] s = split(line, " ");
	if(s[0].equals("chr")){
		String key = s[2].trim(); //e.g. hs1
		String label = s[6].trim(); //e.g. chr1
		int start = Integer.parseInt(s[4]);
		int end = Integer.parseInt(s[5]);

		Chromosome chr = new Chromosome(key, label, start, end);
		chrKeyArray.add(key);
		refChrMap.put(key, chr);
	}else if(s[0].equals("band")){
		String key = s[1].trim();
		String bandID = s[2].trim();
		int start = Integer.parseInt(s[4]);
		int end = Integer.parseInt(s[5]);
		String stain = s[6].trim();
		int stainValue = getStainValue(stain);

		//finc chromosome
		Chromosome chr = (Chromosome)refChrMap.get(key);
		if(chr == null){
			println("Error: chr is null.");
		}else{
			//create cytoband
			Cytoband cb = new Cytoband(start, end, bandID, stainValue);
			chr.add_cytoband(cb);
		}
	}else{
		println("debug: other unknown line:"+line);
	} 
}

void loadLogR(File f, int index){
	_line_counter = 0;
    try{
        BufferedReader reader = createReader(f);
        String line = null;
        while ((line = reader.readLine()) != null ) {
        	if(_line_counter == 0){
        		//header
        		println("debug: logR header="+line);
        		addSample(index);
        		_line_counter ++;
        		_current_data += line.length();
        	}else{
        		parseLogR(index, line);
        		_line_counter ++;
        		_current_data += line.length();
        	}

        }	
    } catch (IOException e) {
        e.printStackTrace();
    }
}

void parseLogR(int index, String line){
	String[] s = split(line, TAB);
	//check chromosome
	String chr_key = "hs"+s[0].trim();
	int position = Integer.parseInt(s[1]);
	float value = Float.parseFloat(s[2]);
	float seg = Float.parseFloat(s[3]);

	//get Chromosome
	Chromosome chr = (Chromosome) refChrMap.get(chr_key);
	if(chr == null){
		println("Error: Chromosome not found:"+chr_key);
	}else{
		//get DataRow
		DataRow row = (DataRow) chr.dataRowMap.get(new Integer(position));
		if(row == null){
			//new row
			row = new DataRow(position);
			chr.dataRowMap.put(new Integer(position), row);
			chr.rows.add(row);
		}
		//save data
		//logR
		row.logR[index] = value;
		//seegmentation
		samples[index].load_logR_seg(seg, row, chr_key);
	} 
}

void loadCN(File f, int index){
	_line_counter = 0;
    try{
        BufferedReader reader = createReader(f);
        String line = null;
        while ((line = reader.readLine()) != null ) {
        	if(_line_counter == 0){
        		//header
        		println("debug: CN header="+line);
        		_line_counter ++;
        		_current_data += line.length();
        	}else{
        		parseCN(index, line);
        		_line_counter ++;
        		_current_data += line.length();
        	}

        }	
    } catch (IOException e) {
        e.printStackTrace();
    }
}

void parseCN(int index, String line){
	String[] s = split(line, TAB);
	//check chromosome
	String chr_key = "hs"+s[0].trim();
	int position = Integer.parseInt(s[1]);
	float value = Float.parseFloat(s[2]);
	float seg = Float.parseFloat(s[3]);

	//get Chromosome
	Chromosome chr = (Chromosome) refChrMap.get(chr_key);
	if(chr == null){
		println("Error: Chromosome not found:"+chr_key);
	}else{
		//get DataRow
		DataRow row = (DataRow) chr.dataRowMap.get(new Integer(position));
		if(row == null){
			println("Error: DataRow is not found for CopyNumber:"+position);
			//new row
			// row = new DataRow(position);
			// chr.dataRowMap.put(new Integer(position), row);
		}
		//save datt
		//CN
		row.copyNumber[index] = value;
		//seegmentation
		samples[index].load_cn_seg(seg, row, chr_key);
	} 
}


//cytoband
int getStainValue(String stain){
	String gStain = stain.substring(4);
    int value;
        if(stain.equals("acen")){
        	//centromere
        	value = -1;
        }else if(stain.equals("gvar")){
        	value = 100;
        }else if(stain.equals("stalk")){
        	value = 0;
        }else if(gStain.equals("")){
	        value = 0;
        }else{
            value = Integer.parseInt(gStain);
        }
     return value;
}

//loading domain track
void loadDomainFile(){
	hasDomainFile = true;
	File f = domain_file;
	String[] lines = loadStrings(f);
	for(String line:lines){
		line = line.replaceAll("\"", ""); //get rid of quatation marks
		String[] s = split(line, TAB);
		if(s[0].equals("Chromosome")){
			//header
		}else{
			int chr_num = Integer.parseInt(s[0].trim());
			int start = Integer.parseInt(s[1].trim());
			int stop = Integer.parseInt(s[6].trim());
			int value = Integer.parseInt(s[5].trim());

			String chr_key = "hs"+chr_num;
			if(chr_num == 23){
				chr_key = "hsX";
			}else if(chr_num == 24){
				chr_key = "hsY";
			}
			//find chromosome
			Chromosome chr = (Chromosome) refChrMap.get(chr_key);
			Range r = new Range(chr_num, start, stop, value);
			chr.domains.add(r);
		}
	}

	println("Debug. domain information loaded!!!");
}



void loadSegData(String url){
	File f = new File(dataPath(url));
	_line_counter = 0;
    try{
        BufferedReader reader = createReader(f);
        String line = null;
        while ((line = reader.readLine()) != null ) {
        	if(_line_counter == 0){
        		//header
        		println("debug: header="+line);
        		_line_counter ++;
        	}else{
        		parseSegData(line);
        		_line_counter ++;
        	}

        }	
    } catch (IOException e) {
        e.printStackTrace();
    }	
    //debug
    //chr1, sample 0
    // ArrayList<Segment> segments_0 = (ArrayList<Segment>)samples[0].logSegMap.get("hs1");
    // println("debug:"+samples[0].label+", chr1 has "+segments_0.size()+" segments");

    println("debug:"+url+" has "+_line_counter+" lines");
}

void parseSegData(String line){
	String[] s = split(line, TAB);
	//check chromosome
	String chr_key = "hs"+s[0].trim();
	//get Chromosome
	Chromosome chr = (Chromosome) refChrMap.get(chr_key);
	if(chr == null){
		println("Error: Chromosome not found:"+chr_key);
	}else{
		//find DataRow
		int pos = Integer.parseInt(s[1]);
		DataRow dr = (DataRow) chr.dataRowMap.get(new Integer(pos));
		if(dr != null){
			//segmentation information
			for(int i = 0; i< samples.length; i++){
				float log_seg = Float.parseFloat(s[2+i]);
				float cn_seg = Float.parseFloat(s[8+i]);

				samples[i].load_logR_seg(log_seg, dr, chr_key);
				samples[i].load_cn_seg(cn_seg, dr, chr_key);
			}

		}else{
			//not found
		}
	}
}



