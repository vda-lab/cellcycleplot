//Karyotype
PGraphics _k;
int _karyotype_x1, _karyotype_x2;
int _karyotype_y1, _karyotype_y2;
int _karyotype_height = _MARGIN*2;
int _karyotype_margin = _MARGIN*2;

Rectangle _k_d_rect;//display


void setupKaryotypeView(){
	_k = createGraphics(_chr_width, _chr_height);
	_karyotype_x1 = _SIDE_MARGIN;
	_karyotype_x2 = _chr_width - _RIGHT_MARGIN;
	_karyotype_y1 = _karyotype_margin;
	_karyotype_y2 = _karyotype_y1 +_karyotype_height; 

	_k_d_rect = new Rectangle( _karyotype_x1, _chr_dy+_karyotype_y1, _top_plot_width, _karyotype_height);

}

void updateKaryotypeView(){
	_k.beginDraw();
	_k.background(200);
	Chromosome chr = _selected_chr;
	int chr_x1 = _karyotype_x1;
	int chr_x2 = _karyotype_x2;
	int chr_y1 = _karyotype_y1;
	int chr_y2 = _karyotype_y2;

	//cytoband
	_k.rectMode(CORNERS);
	for(Cytoband cb:chr.cytobands){
		float cb_start = map(cb.start, chr.start, chr.end, chr_x1, chr_x2);
		float cb_end = map(cb.end, chr.start, chr.end, chr_x1, chr_x2);
		_k.fill(255-cb.fill);
		_k.noStroke();
		_k.rect(cb_start, chr_y1, cb_end, chr_y2);
	}
	_k.rectMode(CORNER);

	//find centromere
	int acen_start_bp = chr.startAcen.start;
	int acen_middle_bp = (chr.endAcen.end + chr.startAcen.start)/2;
	int acen_end_bp = chr.endAcen.end;
	float acen_start_dx = map(acen_start_bp,chr.start, chr.end, chr_x1, chr_x2);
	float acen_middle_dx = map(acen_middle_bp, chr.start, chr.end, chr_x1, chr_x2);
	float acen_end_dx = map(acen_end_bp, chr.start, chr.end, chr_x1, chr_x2);

	//hide centromere background
	_k.fill(200);
	_k.noStroke();
	//top bit
	_k.beginShape();
	_k.vertex(acen_start_dx, chr_y1);
	_k.vertex(acen_middle_dx, chr_y1 + 5);
	_k.vertex(acen_end_dx, chr_y1);
	_k.vertex(acen_start_dx, chr_y1);
	_k.endShape();
	//bottom bit
	_k.beginShape();
	_k.vertex(acen_end_dx, chr_y2);
	_k.vertex(acen_middle_dx, chr_y2 - 5);
	_k.vertex(acen_start_dx, chr_y2);
	_k.vertex(acen_end_dx, chr_y2);
	_k.endShape();


	//outline
	_k.noFill();
	_k.stroke(120);
	_k.strokeWeight(2);
	_k.beginShape();
	_k.vertex(chr_x1, chr_y1);
	_k.vertex(acen_start_dx, chr_y1);
	_k.vertex(acen_middle_dx, chr_y1 + 5);
	_k.vertex(acen_end_dx, chr_y1);
	_k.vertex(chr_x2, chr_y1);
	_k.vertex(chr_x2, chr_y2);
	_k.vertex(acen_end_dx, chr_y2);
	_k.vertex(acen_middle_dx, chr_y2 - 5);
	_k.vertex(acen_start_dx, chr_y2);
	_k.vertex(chr_x1, chr_y2);
	_k.vertex(chr_x1, chr_y1);
	_k.endShape();

	_k.endDraw();
}

void redrawKaryotype(){
	_k.clear();
	updateKaryotypeView();
}
