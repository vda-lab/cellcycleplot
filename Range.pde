class Range implements Comparable<Range>{
	int chr;
	int start;
	int end;
	float value;


	Range(int chr, int start, int end, float value){
		if(start <= end){
			this.chr = chr;
			this.start = start;
			this.end = end;
			this.value = value;
		}else{
			println("error: "+chr+" "+start+" "+end+" "+value);
			throw new RuntimeException("invalid range");
		}
			
	}

	Range(int start, int end){
		if(start <= end){
			this.start = start;
			this.end = end;
		}else throw new RuntimeException("invalid range");
	}
	boolean intersects(Range that){
		if (that.end < this.start) return false;
        if (this.end < that.start) return false;
        return true;
	}

	boolean contains(int chr, int x) {
		if(chr == this.chr){
	        return (this.start <= x) && (x <= this.end);
		}else{
			return false;
		}
    }


	boolean contains(int x) {
        return (this.start <= x) && (x <= this.end);
    }

    int compareTo(Range that) {
        if      (this.start  < that.start)  return -1;
        else if (this.start  > that.start)  return +1;
        else if (this.end < that.end) return -1;
        else if (this.end < that.end) return +1;
        else                            return  0;
    }


}
