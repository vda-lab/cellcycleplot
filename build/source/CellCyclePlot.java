import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.awt.Rectangle; 
import java.awt.GraphicsEnvironment; 
import java.util.HashSet; 
import java.util.Iterator; 
import java.util.Map; 
import java.util.Collections; 
import java.util.Arrays; 
import java.util.Calendar; 
import java.util.Comparator; 
import java.io.BufferedInputStream; 
import java.io.FileInputStream; 
import java.io.BufferedWriter; 
import java.io.FileWriter; 
import java.io.FileOutputStream; 
import java.text.DecimalFormat; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class CellCyclePlot extends PApplet {

//tried loess curves








 










PFont font;
PFont label_font;

boolean filesSelected = false;
boolean isLoadingData = false;
boolean isPreprocessing = false;
boolean hasDomainFile = false;  //whether or not domain file is selected

boolean showingRawData = true;
boolean showingSegData = true;


boolean saved_output = false;



public void setup(){
	font = loadFont("LucidaSans-12.vlw");
	label_font = loadFont("Supernatural1001-10.vlw");

	textFont(font, 12);
	frameRate(30);
	frame.setTitle("CellCyclePlot");	

	_STAGE_WIDTH = displayWidth;
	_STAGE_HEIGHT = displayHeight - 50; //total window height

	// Rectangle rect = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
	// _STAGE_WIDTH = rect.width;
	// _STAGE_HEIGHT = rect.height;

	size(_STAGE_WIDTH, _STAGE_HEIGHT);
	if(frame != null){
		frame.setResizable(true);
	}

	//setup data loading page
	setupDataLoadingPage();

	//load data
	if(isTestMode){
		filesSelected = true;
		isLoadingData = true;
		Runnable loadingFile = new FileLoader();
		new Thread(loadingFile).start();		
	}

}

public String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$tm%1$td%1$tY", now);
}

public String timestamp_detail() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$tm%1$td%1$ty_%1$tH%1$tM", now);
}

class Chromosome{
	String key, label; //(key = hs1, label = chr1)
	int start, end;
	ArrayList<Cytoband> cytobands;
	Cytoband startAcen, endAcen;

	ArrayList<DataRow> rows;
	HashMap<Integer, DataRow> dataRowMap;


	ArrayList<Range> domains;

	Chromosome(String key, String label, int start, int end){
		this.key = key;
		this.label = label;
		this.start = start;
		this.end = end;
		this.cytobands = new ArrayList<Cytoband>();
		this.rows = new ArrayList<DataRow>();
		this.dataRowMap = new HashMap<Integer, DataRow>();

		this.domains = new ArrayList<Range>();
	}

	//copy constructor
	Chromosome(Chromosome that){
		this.key = that.key;
		this.label = that.label;
		this.start = that.start;
		this.end = that.end;
		this.cytobands = that.cytobands;
	}


	public void add_cytoband(Cytoband cb){
		this.cytobands.add(cb);
	}


	public String getCytobandName(int pos){
		for(Cytoband cb: cytobands){
			if(cb.contains(pos)){
				return cb.bandID;
			}
		}
		println("finidng position error: pos="+pos +" end of chromosome ="+end);
		return "???";
	}
} 
//hovering
boolean hovering_l_handle = false;
boolean hovering_r_handle = false;


boolean selected_l_handle = false;
boolean selected_r_handle = false;
boolean selected_middle = false;
int m_pressed_x = 0;
int m_pressed_y = 0;

//color interaction
boolean color_picked = false;
int picked_color_index = -1;

//hightlight in scatter plot
boolean region_selected = false;
boolean region_drag = false;
int region_left, region_right;
int region_left_bp, region_right_bp;


boolean isStacking = false; /////stacking mode


public void keyPressed(){
	if(!filesSelected){
		//sample count
		if(sampleCount_highlight){
			if((key == BACKSPACE)||(key == DELETE)){
			    if(sample_count_input.length() >0){
			        sample_count_input = sample_count_input.substring(0, sample_count_input.length()-1);
			    }
			}else if((key >='0') && (key <= '9')){
			    sample_count_input += key;
			}else if((key == ENTER)||(key==RETURN)){
			    if(sample_count_input.length() >0){
			        numOfSamples = Integer.parseInt(sample_count_input);
			        setupDataLoadingPage();
			        sampleCount_highlight = false;
			    }
			}
		}else if(sampleName_highlight){
			if((key == BACKSPACE)||(key == DELETE)){
			    if(sample_names[sampleName_hightlight_index].length() >0){
			        sample_names[sampleName_hightlight_index] = sample_names[sampleName_hightlight_index].substring(0, sample_names[sampleName_hightlight_index].length()-1);
			    }
			}else if((key >='.') && (key <= 'z')){
			    sample_names[sampleName_hightlight_index] += key;
			}else if((key == ENTER)||(key==RETURN)){
			    sampleName_highlight = false;
			    sampleName_hightlight_index = -1;
			}
		}


	}else if(!isLoadingData && !isPreprocessing){
		if(key == CODED){
			if(keyCode == RIGHT){
				_selected_chr_index = min(_selected_chr_index+1, chrKeyArray.size()-1);
				update_new_chromosome();
			}else if(keyCode == LEFT){
				_selected_chr_index = max(_selected_chr_index-1, 0);
				update_new_chromosome();
			}
		}

		if(output_note_active){
			if((key == BACKSPACE)||(key == DELETE)){
			    if(output_note_input.length() >0){
			        output_note_input = output_note_input.substring(0, output_note_input.length()-1);
			    }
			}else if(((key >='.') && (key <= 'z')) || (key == ' ')){
			    output_note_input += key;
			}else if((key == ENTER)||(key==RETURN)){
			    output_note_active = false;
			    //perhaps save test by this.... todo 
			}
			updateGUI();
			loop();
		}else if(output_file_name_active){
			if((key == BACKSPACE)||(key == DELETE)){
			    if(output_file_url.length() >0){
			        output_file_url = output_file_url.substring(0, output_file_url.length()-1);
			    }
			}else if(((key >='.') && (key <= 'z')) || (key == ' ')){
			    output_file_url += key;
			}else if((key == ENTER)||(key==RETURN)){
			    output_file_name_active = false;
			}
			updateGUI();
			loop();
		}
	}
}

public void mouseMoved(){
	if(!isLoadingData && !isPreprocessing){
		saved_output = false;

		if(_chr_dy < mouseY && mouseY < _chr_dy+_karyotype_margin+_karyotype_height){
			//check distance
			float dist_left = abs(mouseX - left_handle_x);
			float dist_right = abs(mouseX - right_handle_x);

			if(dist_left < 10 && mouseX < left_handle_x){
				hovering_l_handle = true;
				cursor(HAND);
			}else if(dist_right < 10 && mouseX  > right_handle_x){
				hovering_r_handle = true;
				cursor(HAND);
			}else if( left_handle_x +5  < mouseX  && mouseX < right_handle_x -5){
				cursor(HAND);
			}else{
				hovering_l_handle = false;
				hovering_r_handle = false;
				cursor(ARROW);
			}
			loop();
		}else if(_scatter_dy < mouseY && mouseY <_scatter_dy+_scatter_height){
			cursor(CROSS);
			_draw_counter = 0;
			loop();
		}else{
			hovering_l_handle = false;
			hovering_r_handle = false;
			cursor(ARROW);
		}

	}
	//keep updating the view
	_draw_counter = 0;
}

public void mousePressed(){
	if(!isLoadingData && !isPreprocessing){
		selected_l_handle = false;
		selected_r_handle = false;
		selected_middle = false;
		m_pressed_x = mouseX;
		m_pressed_y = mouseY;

		color_picked = false;
		picked_color_index = -1;

		region_drag = false;

		//gui area
		if(_cont_dy < mouseY && mouseY < (_cont_dy+_cont_height)){
			//legend area GUI
			//check color pallet 
			for(int i = 0; i < color_pallet.length; i++){
				if(color_pallet[i].contains(mouseX, mouseY- _cont_dy)){
					//selected
					picked_color_index = i;
					color_picked = true;
				}
			}
		}else if(_chr_dy < mouseY && mouseY < _chr_dy+_karyotype_margin+_karyotype_height){
			if(hovering_l_handle){
				selected_l_handle = true;
			}else if(hovering_r_handle){
				selected_r_handle = true;
			}else if( left_handle_x +5  < mouseX  && mouseX < right_handle_x -5){
				//middle
				selected_middle = true;
			}
		}else if(_scatter_dy < mouseY && mouseY <_scatter_dy+_scatter_height){
			if(!region_selected){
				println("debug: mousePressed");
				//set left
				region_left = mouseX;
				region_left_bp = getBpPosition(mouseX);//round(map(mouseX, _scatter_dx, _scatter_dx+_scatter_width, _start_pos,_end_pos));

				//intial value
				region_right = mouseX;
				region_right_bp = region_left_bp;
				region_drag = true;	
			}
		}
	}
}

public void mouseDragged(){
	if(!isLoadingData && !isPreprocessing){
		region_selected = false;

		if(selected_l_handle){
			//update left position
			update_left_handle(mouseX);
			_draw_counter = 0;
			updateDomainView();
			redraw();
		}else if(selected_r_handle){
			//update right position
			update_right_handle(mouseX);
			_draw_counter = 0;
			updateDomainView();
			redraw();
		}else if(selected_middle){
			update_middle(m_pressed_x, mouseX);
		    m_pressed_x = mouseX;
		    updateDomainView();
			redraw();
		}else if(color_picked){
			_draw_counter = 0;
			redraw();
		}else if(region_drag){
			region_selected = true;
			// println("debug: mouseDragged");
			//update right
			region_right = mouseX;
			region_right_bp = getBpPosition(mouseX); //round(map(mouseX, _scatter_dx, _scatter_dx+_scatter_width, _start_pos,_end_pos));
			_draw_counter = 0;
			loop();
		}
	}
}


public void mouseReleased(){
	if(!filesSelected){
		//update when clicked outside
		if(sampleCount_highlight){
			if(sample_count_input.length() >0){
			    numOfSamples = Integer.parseInt(sample_count_input);
			    setupDataLoadingPage();
			}
		}
		sampleCount_highlight = false;
		sampleName_highlight = false;
		sampleName_hightlight_index = -1;
		sample_selected_index = -1;
		if(sampleCount_rect.contains(mouseX, mouseY)){
			//sampleCount
			sampleCount_highlight = true;
		}else if(load_btn.contains(mouseX, mouseY)){
			//check if every field is filled
			boolean missingValue = false;
			String msg ="";
			for(int i = 0; i < sample_names.length; i++){
				if(sample_names[i].equals("")){
					msg += "*Sample name is missing for "+(i+1)+"\n";
					missingValue = true;
				}
			}
			for(int i = 0; i< sample_log_file.length; i++){
				if(sample_log_file[i] == null){
					msg += "*LogR file for "+(i+1)+" is missing \n";
					missingValue = true;
				}
			}
			for(int i = 0; i < sample_cn_file.length; i++){
				if(sample_cn_file[i] == null){
					msg += "*Copy Number file for "+(i+1)+" is missing \n";
					missingValue = true;
				}
			}

			if(missingValue){
				try {
				  //run a bit of code
				  javax.swing.JOptionPane.showMessageDialog(null, msg);
				} catch (Exception e) {
				   println("error on load_btn click:"+e);
				} 
			}else{
				//start loading
				println("start loading");
				filesSelected = true;
				isLoadingData = true;
				Runnable loadingFile = new FileLoader();
				new Thread(loadingFile).start();
			}
		}else if(domain_file_btn.contains(mouseX, mouseY)){
			//select domain file
			selectInput("Select your domain file:", "selectFile_domain");
		}else if(conf_file_btn.contains(mouseX, mouseY)){
			//configuration file
			selectInput("Select a configuration file:", "selectFile_conf");

		}else if(conf_save_btn.contains(mouseX, mouseY)){
			selectFolder("Select a folder to save the configuration file", "select_conf_folder");

		}else{
			//check rest of  buttons
			for(int i = 0; i< dl_rectangles.size(); i++){
				Rectangle[] rects = dl_rectangles.get(i);
				for(int j = 0; j < rects.length; j++){
					Rectangle r = rects[j];
					if(r.contains(mouseX, mouseY)){
						switch(j){
							case 0:
								//sample name
								sampleName_highlight = true;
								sampleName_hightlight_index = i;
								break;
							case 2:
								//logR load
								sample_selected_index = i;
								selectInput("Select your logR file:", "selectFile_logR");
								break;
							case 4:
								//cn load
								sample_selected_index = i;
								selectInput("Select your Copy Number file:", "selectFile_cn");
								break;
						}
						return;
					}
				}
			}
		}
	}else if(!isLoadingData && !isPreprocessing){
		if(color_picked){
			if(_cont_dy < mouseY && mouseY < (_cont_dy+_cont_height)){
				// GUI
				//sample buttons
				for(int i = 0; i< samples.length; i++){
					Rectangle r = sample_btns[i];
					if(r.contains(mouseX, mouseY - _cont_dy)){
						Sample s = samples[i];
						s.isShowing = true;
						//set color
						s.stroke_color = _color_array[picked_color_index];
						//reset
						color_picked = false;
						picked_color_index = -1;
						_draw_counter = 0;
						updateGUI();
						s.redraw();
						loop();
						return;
					}
				}
				//domain button
				for(int i = 0; i< domain_btns.length; i++){
					Rectangle r = domain_btns[i];
					if(r.contains(mouseX, mouseY- _cont_dy)){
						if(i == 0){ //late
							_late_domain_color = _color_array[picked_color_index];
							showing_late_domain = true;
						}else if(i == 1){ //early
							_early_domain_color = _color_array[picked_color_index];
							showing_early_domain = true;
						}
						//reset
						color_picked = false;
						picked_color_index = -1;
						_draw_counter = 0;
						updateGUI();
						updateDomainView();
						loop();
						return;
					}
				}
				
			}
		}else if(region_drag){
			println("debug: mouseReleased");
			//update right
			region_right = mouseX;
			region_right_bp = getBpPosition(mouseX);//round(map(mouseX, _scatter_dx, _scatter_dx+_scatter_width, _start_pos,_end_pos));
			//chect left and right
			if(region_right < region_left){
				//swap
				int temp = region_right;
				int temp_bp = region_right_bp;
				region_right = region_left;
				region_right_bp = region_left_bp;
				region_left = temp;
				region_left_bp = temp_bp;
			}

			//update out put view
			updateGUI();
			return;
		}

		output_note_active = false;
		output_file_name_active = false;

		if(mouseY  < _header_height){
			for(int i = 0; i< chr_btns.length; i++){
				Rectangle rect = chr_btns[i];
				if(rect.contains(mouseX, mouseY)){
					_selected_chr_index = i;
					update_new_chromosome();
					return;
				}
			}
		}else if(_scatter_dy < mouseY && mouseY <_scatter_dy+_scatter_height){
			//scatter plot
			if(region_selected){
				if(region_left  > mouseX || mouseX  > region_right){
					println("debug: mouseClicked");
					//unselect
					region_selected = false;
					_draw_counter = 0;
					loop();
					return;
				}
			}
		}else if(_cont_dy < mouseY && mouseY < (_cont_dy+_cont_height)){
			//legend area GUI
			//data type buttons
			for(int i= 0; i< datatype_btns.length; i++){
				Rectangle r = datatype_btns[i];
				if(r.contains(mouseX, mouseY- _cont_dy)){
					// println("click!!! "+mouseX+"  "+mouseY);
					switch(i){
						case 0:
							//seg
							showingSegData = !showingSegData;
							break;
						case 1:
							//raw
							showingRawData = !showingRawData;
							break;
					}
					_draw_counter = 0;
					updateGUI();
					loop();
					return;
				}
			}
			//sample buttons
			for(int i = 0; i< samples.length; i++){
				Rectangle r = sample_btns[i];
				if(r.contains(mouseX, mouseY - _cont_dy)){
					Sample s = samples[i];
					s.isShowing = !s.isShowing;
					_draw_counter = 0;
					update_stacking(); // stacking
					redraw_seg();
					updateGUI();
					loop();
					return;
				}
			}
			//domain
			if(hasDomainFile){
				for(int i = 0; i< domain_btns.length; i++){
					Rectangle r = domain_btns[i];
					if(r.contains(mouseX, mouseY- _cont_dy)){
						if(i == 0){ //late
							showing_late_domain = !showing_late_domain;
						}else if(i == 1){ //early
							showing_early_domain = !showing_early_domain;
						}
						//reset
						updateGUI();
						updateDomainView();
						loop();
						return;
					}
				}
			}

			//representation
			for(int i = 0; i < encoding_labels.length; i++){
				Rectangle r = encoding_btns[i];
				if(r.contains(mouseX, mouseY- _cont_dy)){
					//hit
					for(int j = 0; j < encoding_selected.length; j++){
						if(i == j){
							encoding_selected[j] = true;
						}else{
							encoding_selected[j] = false;
						}
					}
					_draw_counter = 0;
					updateGUI();
					for(Sample s:samples){
						s.redraw();
					}
					loop();
					return;
				}
			}

			//stacking options
			if(stacking_btns[0].contains(mouseX, mouseY - _cont_dy)){
				isStacking = !isStacking;
				_draw_counter = 0;
				update_stacking(); // stacking
				redraw_seg();
				updateGUI();
				loop();
				return;
			}

			//output
			if(output_file_name.contains(mouseX, mouseY - _cont_dy)){
				output_note_active = false;
				output_file_name_active = true;
			}else if(output_note_rect.contains(mouseX, mouseY - _cont_dy)){
				output_note_active = true;
				output_file_name_active = false;
				
			}else if(output_btn.contains(mouseX, mouseY - _cont_dy)){
				//save
				if(region_selected){
					save_note();
					output_note_active = false;
					output_file_name_active = false;
					output_note_input = "";
				}
			}


			//else
			updateGUI();
			loop();
			return;
		}
	}
	//reset
	color_picked = false;
	picked_color_index = -1;
}

// void mouseClicked(){
// 	// if(!filesSelected){
// 	// 	//update when clicked outside
// 	// 	if(sampleCount_highlight){
// 	// 		if(sample_count_input.length() >0){
// 	// 		    numOfSamples = Integer.parseInt(sample_count_input);
// 	// 		    setupDataLoadingPage();
// 	// 		}
// 	// 	}
// 	// 	sampleCount_highlight = false;
// 	// 	sampleName_highlight = false;
// 	// 	sampleName_hightlight_index = -1;
// 	// 	sample_selected_index = -1;
// 	// 	if(sampleCount_rect.contains(mouseX, mouseY)){
// 	// 		//sampleCount
// 	// 		sampleCount_highlight = true;
// 	// 	}else if(load_btn.contains(mouseX, mouseY)){
// 	// 		//check if every field is filled
// 	// 		boolean missingValue = false;
// 	// 		String msg ="";
// 	// 		for(int i = 0; i < sample_names.length; i++){
// 	// 			if(sample_names[i].equals("")){
// 	// 				msg += "*Sample name is missing for "+(i+1)+"\n";
// 	// 				missingValue = true;
// 	// 			}
// 	// 		}
// 	// 		for(int i = 0; i< sample_log_file.length; i++){
// 	// 			if(sample_log_file[i] == null){
// 	// 				msg += "*LogR file for "+(i+1)+" is missing \n";
// 	// 				missingValue = true;
// 	// 			}
// 	// 		}
// 	// 		for(int i = 0; i < sample_cn_file.length; i++){
// 	// 			if(sample_cn_file[i] == null){
// 	// 				msg += "*Copy Number file for "+(i+1)+" is missing \n";
// 	// 				missingValue = true;
// 	// 			}
// 	// 		}

// 	// 		if(missingValue){
// 	// 			try {
// 	// 			  //run a bit of code
// 	// 			  javax.swing.JOptionPane.showMessageDialog(null, msg);
// 	// 			} catch (Exception e) {
// 	// 			   println("error on load_btn click:"+e);
// 	// 			} 
// 	// 		}else{
// 	// 			//start loading
// 	// 			println("start loading");
// 	// 			filesSelected = true;
// 	// 			isLoadingData = true;
// 	// 			Runnable loadingFile = new FileLoader();
// 	// 			new Thread(loadingFile).start();
// 	// 		}
// 	// 	}else if(domain_file_btn.contains(mouseX, mouseY)){
// 	// 		//select domain file
// 	// 		selectInput("Select your domain file:", "selectFile_domain");
// 	// 	}else if(conf_file_btn.contains(mouseX, mouseY)){
// 	// 		//configuration file
// 	// 		selectInput("Select a configuration file:", "selectFile_conf");

// 	// 	}else if(conf_save_btn.contains(mouseX, mouseY)){
// 	// 		selectFolder("Select a folder to save the configuration file", "select_conf_folder");

// 	// 	}else{
// 	// 		//check rest of  buttons
// 	// 		for(int i = 0; i< dl_rectangles.size(); i++){
// 	// 			Rectangle[] rects = dl_rectangles.get(i);
// 	// 			for(int j = 0; j < rects.length; j++){
// 	// 				Rectangle r = rects[j];
// 	// 				if(r.contains(mouseX, mouseY)){
// 	// 					switch(j){
// 	// 						case 0:
// 	// 							//sample name
// 	// 							sampleName_highlight = true;
// 	// 							sampleName_hightlight_index = i;
// 	// 							break;
// 	// 						case 2:
// 	// 							//logR load
// 	// 							sample_selected_index = i;
// 	// 							selectInput("Select your logR file:", "selectFile_logR");
// 	// 							break;
// 	// 						case 4:
// 	// 							//cn load
// 	// 							sample_selected_index = i;
// 	// 							selectInput("Select your Copy Number file:", "selectFile_cn");
// 	// 							break;
// 	// 					}
// 	// 					return;
// 	// 				}
// 	// 			}
// 	// 		}
// 	// 	}
// 	}else if(!isLoadingData && !isPreprocessing){
// 		// output_note_active = false;
// 		// output_file_name_active = false;

// 		// if(mouseY  < _header_height){
// 		// 	for(int i = 0; i< chr_btns.length; i++){
// 		// 		Rectangle rect = chr_btns[i];
// 		// 		if(rect.contains(mouseX, mouseY)){
// 		// 			_selected_chr_index = i;
// 		// 			update_new_chromosome();
// 		// 			return;
// 		// 		}
// 		// 	}
// 		// }else if(_scatter_dy < mouseY && mouseY <_scatter_dy+_scatter_height){
// 		// 	//scatter plot
// 		// 	if(region_selected){
// 		// 		if(region_left  > mouseX || mouseX  > region_right){
// 		// 			println("debug: mouseClicked");
// 		// 			//unselect
// 		// 			region_selected = false;
// 		// 			_draw_counter = 0;
// 		// 			loop();
// 		// 			return;
// 		// 		}
// 		// 	}
// 		// }else if(_cont_dy < mouseY && mouseY < (_cont_dy+_cont_height)){
// 		// 	//legend area GUI
// 		// 	//data type buttons
// 		// 	for(int i= 0; i< datatype_btns.length; i++){
// 		// 		Rectangle r = datatype_btns[i];
// 		// 		if(r.contains(mouseX, mouseY- _cont_dy)){
// 		// 			// println("click!!! "+mouseX+"  "+mouseY);
// 		// 			switch(i){
// 		// 				case 0:
// 		// 					//seg
// 		// 					showingSegData = !showingSegData;
// 		// 					break;
// 		// 				case 1:
// 		// 					//raw
// 		// 					showingRawData = !showingRawData;
// 		// 					break;
// 		// 			}
// 		// 			_draw_counter = 0;
// 		// 			updateGUI();
// 		// 			loop();
// 		// 			return;
// 		// 		}
// 		// 	}
// 		// 	//sample buttons
// 		// 	for(int i = 0; i< samples.length; i++){
// 		// 		Rectangle r = sample_btns[i];
// 		// 		if(r.contains(mouseX, mouseY - _cont_dy)){
// 		// 			Sample s = samples[i];
// 		// 			s.isShowing = !s.isShowing;
// 		// 			_draw_counter = 0;
// 		// 			update_stacking(); // stacking
// 		// 			redraw_seg();
// 		// 			updateGUI();
// 		// 			loop();
// 		// 			return;
// 		// 		}
// 		// 	}
// 		// 	//domain
// 		// 	for(int i = 0; i< domain_btns.length; i++){
// 		// 		Rectangle r = domain_btns[i];
// 		// 		if(r.contains(mouseX, mouseY- _cont_dy)){
// 		// 			if(i == 0){ //late
// 		// 				showing_late_domain = !showing_late_domain;
// 		// 			}else if(i == 1){ //early
// 		// 				showing_early_domain = !showing_early_domain;
// 		// 			}
// 		// 			//reset
// 		// 			updateGUI();
// 		// 			updateDomainView();
// 		// 			loop();
// 		// 			return;
// 		// 		}
// 		// 	}

// 		// 	//representation
// 		// 	for(int i = 0; i < encoding_labels.length; i++){
// 		// 		Rectangle r = encoding_btns[i];
// 		// 		if(r.contains(mouseX, mouseY- _cont_dy)){
// 		// 			//hit
// 		// 			for(int j = 0; j < encoding_selected.length; j++){
// 		// 				if(i == j){
// 		// 					encoding_selected[j] = true;
// 		// 				}else{
// 		// 					encoding_selected[j] = false;
// 		// 				}
// 		// 			}
// 		// 			_draw_counter = 0;
// 		// 			updateGUI();
// 		// 			for(Sample s:samples){
// 		// 				s.redraw();
// 		// 			}
// 		// 			loop();
// 		// 			return;
// 		// 		}
// 		// 	}

// 		// 	//stacking options
// 		// 	if(stacking_btns[0].contains(mouseX, mouseY - _cont_dy)){
// 		// 		isStacking = !isStacking;
// 		// 		_draw_counter = 0;
// 		// 		update_stacking(); // stacking
// 		// 		redraw_seg();
// 		// 		updateGUI();
// 		// 		loop();
// 		// 		return;
// 		// 	}

// 		// 	//output
// 		// 	if(output_file_name.contains(mouseX, mouseY - _cont_dy)){
// 		// 		output_note_active = false;
// 		// 		output_file_name_active = true;
// 		// 	}else if(output_note_rect.contains(mouseX, mouseY - _cont_dy)){
// 		// 		output_note_active = true;
// 		// 		output_file_name_active = false;
				
// 		// 	}else if(output_btn.contains(mouseX, mouseY - _cont_dy)){
// 		// 		//save
// 		// 		if(region_selected){
// 		// 			save_note();
// 		// 			output_note_active = false;
// 		// 			output_file_name_active = false;
// 		// 			output_note_input = "";
// 		// 		}
// 		// 	}


// 		// 	//else
// 		// 	updateGUI();
// 		// 	loop();
// 		// 	return;
// 		// }
// 	}
// }

public void selectFile_logR(File selection){
	if(selection == null){
		sample_log_file[sample_selected_index] = null;
	}else{
		sample_log_file[sample_selected_index] = selection;
	}
	//reset
	sample_selected_index = -1;
}

public void selectFile_cn(File selection){
	if(selection == null){
		sample_cn_file[sample_selected_index] = null;
	}else{
		sample_cn_file[sample_selected_index] = selection;
	}
	//reset
	sample_selected_index = -1;
}

public void selectFile_domain(File selection){
	if(selection == null){
		domain_file = null;
	}else{
		domain_file = selection;
	}
}

public void selectFile_conf(File selection){
	if(selection == null){
		//do nothing
	}else{
		//parse and load files
		conf_file = selection;
		String[] lines = loadStrings(selection);
		int sampleLineCounter = 0;
		for(String line : lines){
			String[] s = split(line, TAB);
			if(s[0].trim().equals("#sample_count")){
				int sampleCount = Integer.parseInt(s[1]);
				numOfSamples = sampleCount;
				sample_count_input = ""+numOfSamples;
				setupDataLoadingPage();
			}else if(s[0].trim().equals("#domain")){
				if(s.length >1 && s[1] != ""){
					domain_file = new File(s[1].trim());
					println("debug:domain file:"+domain_file.getAbsolutePath());
				}
			}else if(s[0].trim().equals("#sample_name")){
				//header
			}else{
				sample_names[sampleLineCounter] = s[0].trim();
				sample_log_file[sampleLineCounter] = new File(s[1].trim());
				sample_cn_file[sampleLineCounter] = new File(s[2].trim());
				sampleLineCounter++;
			}
		}
	}
}


public void select_conf_folder(File selection){
	String path = selection.getAbsolutePath();
	PrintWriter output = createWriter(path+"/"+conf_save_name+".txt");
	output.println("#sample_count\t"+numOfSamples);
	if(domain_file != null){
		output.println("#domain\t"+domain_file.getAbsolutePath());
	}
	output.println("#sample_name\tlogR\tcopy_number");
	for(int i = 0; i < numOfSamples; i++){
		String line = sample_names[i]+"\t";
		line += sample_log_file[i].getAbsolutePath() +"\t";
		line += sample_cn_file[i].getAbsolutePath();
		output.println(line);
	}


	output.flush();
	output.close();
}

public int getBpPosition(int mousePos){
	return round(map(mouseX, _top_plot_x, _top_plot_x+_top_plot_width, _start_pos,_end_pos));
}
//wrtie to a file
public void save_note(){
	println("debug:seve_note():");
	String chr = chrKeyArray.get(_selected_chr_index).replace("hs", "chr");
	String output = chr+"\t"+region_left_bp+"\t"+region_right_bp+"\t"+output_note_input;
	PrintWriter writer = null;
	// writer = createWriter(output_file_url+".txt");
	try{
		writer = new PrintWriter(new FileOutputStream(new File(sketchPath(output_file_url+".txt")), true));
	}catch(IOException e){

	}
	writer.println(output);
	writer.flush();
	writer.close();
	
	saved_output = true;
}


class Cytoband{
	int start, end, fill;
	String bandID;
	boolean isCentromere = false;
	boolean is_p_end = false;
	boolean is_q_end = false;

	Cytoband(int start, int end, String bandID, int stainValue){
		this.start = start;
		this.end = end;
		this.bandID = bandID;
		this.fill = stainValue;
		if(fill == -1){
			//centromere
			isCentromere = true;
			fill = 0;
		}
	}
	//chech if intersect
	public boolean intersects(int s, int e){
		if(e <=this.start) return false;
		if(this.end <= s) return false;
		return true;
	}
	public boolean contains(int s){
		if(this.start <= s && s <= this.end){
			return true;
		}
		return false;
	}


	public String toString(){
		return start+" "+end+" "+bandID;
	}
}
String dl_t_1 = "Number of samples:";
String[] dl_labels = {"Sample Name", "LogR", "Copy Number"};
String[] dl_btn_labels ={"Select", "Load"};


int numOfSamples = 6;

int dl_t_1_x, dl_t_1_y;
int[] dl_labels_x;
int dl_labels_y;

int btn_width;
int btn_height;
int sampleName_width;
int fileName_width;

Rectangle sampleCount_rect;
ArrayList<Rectangle[]> dl_rectangles;
Rectangle load_btn;

//data input
String[] sample_names;
File[] sample_log_file;
File[] sample_cn_file;
String sample_count_input = "6";
File domain_file;
File conf_file;


boolean sampleCount_highlight = false;
boolean sampleName_highlight = false;
int sampleName_hightlight_index = -1;
int sample_selected_index = -1;

String domain_label = "Optional: Domain File";
Rectangle domain_file_name_rect;
Rectangle domain_file_btn;

String configuration_label = "Optional: Configuration File";
Rectangle conf_file_name_rect;
Rectangle conf_file_btn;

String configuration_save ="Save the configuration";
Rectangle conf_save_name_rect;
Rectangle conf_save_btn;
String conf_save_name ="";



public void setupDataLoadingPage(){
	//reset input valuses
	sample_names = new String[numOfSamples];
	sample_log_file = new File[numOfSamples];
	sample_cn_file = new File[numOfSamples];

	Arrays.fill(sample_names,"");
	Arrays.fill(sample_log_file,null);
	Arrays.fill(sample_cn_file,null);

	btn_width = _MARGIN*10;
	btn_height = _MARGIN*2;
	sampleName_width = _MARGIN*10;
	fileName_width = _MARGIN*30;

	int runningX = _SIDE_MARGIN + _MARGIN*2;
	int runningY = _MARGIN*8;

	dl_t_1_x = runningX;
	dl_t_1_y = runningY;

	float textWidth = textWidth(dl_t_1);
	runningX += textWidth + _MARGIN;
	sampleCount_rect = new Rectangle(runningX, runningY, btn_width, btn_height);

	//rectangles
	runningX = _SIDE_MARGIN;
	runningY += btn_height +_MARGIN;
	//label position
	dl_labels_y = runningY;
	Rectangle[] rects = new Rectangle[5];
	//numbering
	runningX += _MARGIN*2;
	runningY += _MARGIN*2;
	rects[0] = new Rectangle(runningX, runningY, sampleName_width, btn_height);
	runningX += sampleName_width+_MARGIN*3;
	rects[1] = new Rectangle(runningX, runningY, fileName_width, btn_height);
	runningX += fileName_width + _MARGIN;
	rects[2] = new Rectangle(runningX, runningY, btn_width, btn_height);
	runningX += btn_width +_MARGIN*3;
	rects[3] = new Rectangle(runningX, runningY, fileName_width, btn_height);
	runningX += fileName_width + _MARGIN;
	rects[4] = new Rectangle(runningX, runningY, btn_width, btn_height);


	dl_rectangles = new ArrayList<Rectangle[]>();
	for(int i = 0; i < numOfSamples; i++){
		Rectangle[] rectangles = new Rectangle[5];
		rectangles[0] = new Rectangle(rects[0].x, runningY, sampleName_width, btn_height);
		rectangles[1] = new Rectangle(rects[1].x, runningY, fileName_width, btn_height);
		rectangles[2] = new Rectangle(rects[2].x, runningY, btn_width, btn_height);
		rectangles[3] = new Rectangle(rects[3].x, runningY, fileName_width, btn_height);
		rectangles[4] = new Rectangle(rects[4].x, runningY, btn_width, btn_height);
		dl_rectangles.add(rectangles);
		runningY += btn_height +_MARGIN;
	}
	
	dl_labels_x = new int[3];
	dl_labels_x[0] = rects[0].x; //sample name
	dl_labels_x[1] = rects[1].x; //logR
	dl_labels_x[2] = rects[3].x; //copy number

	//domain file
	runningY += _MARGIN*3;
	domain_file_name_rect = new Rectangle(rects[0].x, runningY, fileName_width+_MARGIN*2, btn_height);
	domain_file_btn = new Rectangle(rects[0].x+fileName_width+_MARGIN*3, runningY, btn_width, btn_height);
	runningY += btn_height + _MARGIN;
	//conf file
	runningY += _MARGIN*3;
	conf_file_name_rect = new Rectangle(rects[0].x, runningY, fileName_width+_MARGIN*2, btn_height);
	conf_file_btn = new Rectangle(rects[0].x+fileName_width+_MARGIN*3, runningY, btn_width, btn_height);
	runningY += btn_height + _MARGIN;

	//conf save
	runningY += _MARGIN*3;
	conf_save_name_rect = new Rectangle(rects[0].x, runningY, fileName_width+_MARGIN*2, btn_height);
	conf_save_btn = new Rectangle(rects[0].x+fileName_width+_MARGIN*3, runningY, btn_width, btn_height);
	//file name
	conf_save_name = "conf_"+timestamp_detail();
	// println("debug:configuration file name :"+conf_save_name);



	//load btn
	load_btn = new Rectangle(rects[4].x, runningY+_MARGIN, btn_width, btn_height);
}


public void drawDataLoadingPage(){
	background(240);
	//number of sample
	fill(120);
	textAlign(LEFT, CENTER);
	text(dl_t_1, dl_t_1_x, (float)sampleCount_rect.getCenterY());
	//text box
	fill(255);
	if(sampleCount_highlight){
		strokeWeight(2);
		stroke(color_cyan);
	}else{
		strokeWeight(1);
		stroke(180);
	}
	rect(sampleCount_rect.x, sampleCount_rect.y, sampleCount_rect.width, sampleCount_rect.height);
	//number of samples
	fill(120);
	textAlign(RIGHT, CENTER);
	text(sample_count_input, sampleCount_rect.x+sampleCount_rect.width -4, (float)sampleCount_rect.getCenterY());

	//labels
	fill(120);
	textAlign(LEFT, TOP);
	for(int i = 0; i < dl_labels.length; i++){
		text(dl_labels[i], dl_labels_x[i], dl_labels_y);
	}

	//Rectangles
	for(int i = 0; i< dl_rectangles.size(); i++){
		Rectangle[] array = dl_rectangles.get(i);
		//text box
		fill(255);
		stroke(180);
		strokeWeight(1);
		rect(array[0].x, array[0].y, array[0].width, array[0].height);
		rect(array[1].x, array[1].y, array[1].width, array[1].height);
		rect(array[3].x, array[3].y, array[3].width, array[3].height);
		//buttons
		fill(180);
		noStroke();
		rect(array[2].x, array[2].y, array[2].width, array[2].height);
		rect(array[4].x, array[4].y, array[4].width, array[4].height);
		rect(load_btn.x, load_btn.y, load_btn.width, load_btn.height);
		//text
		fill(255);
		textAlign(CENTER, CENTER);
		text(dl_btn_labels[0], (float)array[2].getCenterX(), (float)array[2].getCenterY());
		text(dl_btn_labels[0], (float)array[4].getCenterX(), (float)array[4].getCenterY());
		text(dl_btn_labels[1], (float)load_btn.getCenterX(), (float)load_btn.getCenterY());
		
		//sample name
		fill(120);
		textAlign(LEFT, CENTER);
		text(sample_names[i], array[0].x +4, (float)array[2].getCenterY());
		

		//row number
		fill(120);
		textAlign(RIGHT, CENTER);
		text((i+1), array[0].x -4, (float)array[0].getCenterY());

		if(sampleName_highlight && i == sampleName_hightlight_index){
			noFill();
			stroke(color_cyan);
			strokeWeight(2);
			rect(array[0].x, array[0].y, array[0].width, array[0].height);
		}

		//logR file
		if(sample_log_file[i] != null){
			fill(120);
			textAlign(LEFT, CENTER);
			String file_url = sample_log_file[i].getAbsolutePath();
			if(file_url.length() > 40){
				file_url = "..."+file_url.substring((file_url.length()-37), (file_url.length()));
			}
			text(file_url, array[1].x+4, (float)array[1].getCenterY());
		}
		if(sample_cn_file[i] != null){
			fill(120);
			textAlign(LEFT, CENTER);
			String file_url = sample_cn_file[i].getAbsolutePath();
			if(file_url.length() > 40){
				file_url = "..."+file_url.substring((file_url.length()-37), (file_url.length()));
			}
			text(file_url, array[3].x+4, (float)array[3].getCenterY());
		}
	}

	//domain file
	fill(120);
	textAlign(LEFT, TOP);
	text(domain_label, domain_file_name_rect.x, domain_file_name_rect.y - _MARGIN*2);
	fill(255);
	stroke(180);
	strokeWeight(1);
	rect(domain_file_name_rect.x, domain_file_name_rect.y, domain_file_name_rect.width, domain_file_name_rect.height);	
	fill(180);
	noStroke();
	rect(domain_file_btn.x, domain_file_btn.y, domain_file_btn.width, domain_file_btn.height);
	//text
	fill(255);
	textAlign(CENTER, CENTER);
	text(dl_btn_labels[0], (float)domain_file_btn.getCenterX(), (float)domain_file_btn.getCenterY());
	//file name
	if(domain_file != null){
		fill(120);
		String file_url = domain_file.getAbsolutePath();
		if(file_url.length() > 40){
			file_url = "..."+file_url.substring((file_url.length()-37), (file_url.length()));
		}
		textAlign(LEFT, CENTER);
		text(file_url, domain_file_name_rect.x+4, (float)domain_file_name_rect.getCenterY());
	}


	//conf file
	fill(120);
	textAlign(LEFT, TOP);
	text(configuration_label, conf_file_name_rect.x, conf_file_name_rect.y - _MARGIN*2);
	fill(255);
	stroke(180);
	strokeWeight(1);
	rect(conf_file_name_rect.x, conf_file_name_rect.y, conf_file_name_rect.width, conf_file_name_rect.height);	
	fill(180);
	noStroke();
	rect(conf_file_btn.x, conf_file_btn.y, conf_file_btn.width, conf_file_btn.height);
	//btn text
	fill(255);
	textAlign(CENTER, CENTER);
	text(dl_btn_labels[0], (float)conf_file_btn.getCenterX(), (float)conf_file_btn.getCenterY());
	if(conf_file != null){
		fill(120);
		String file_url = conf_file.getAbsolutePath();
		if(file_url.length() > 40){
			file_url = "..."+file_url.substring((file_url.length()-37), (file_url.length()));
		}
		textAlign(LEFT, CENTER);
		text(file_url, conf_file_name_rect.x+4, (float)conf_file_name_rect.getCenterY());
	}

	//conf_save
	fill(120);
	textAlign(LEFT, TOP);
	text(configuration_save, conf_save_name_rect.x, conf_save_name_rect.y -_MARGIN*2);
	fill(255);
	stroke(180);
	strokeWeight(1);
	rect(conf_save_name_rect.x, conf_save_name_rect.y, conf_save_name_rect.width, conf_save_name_rect.height);
	fill(180);
	noStroke();
	rect(conf_save_btn.x, conf_save_btn.y, conf_save_btn.width, conf_save_btn.height);
	fill(255);
	textAlign(CENTER, CENTER);
	text("Save", (float)conf_save_btn.getCenterX(), (float)conf_save_btn.getCenterY());
	//conf_save file name
	fill(120);
	textAlign(LEFT, CENTER);
	text(conf_save_name, conf_save_name_rect.x+4, (float)conf_save_name_rect.getCenterY());
}


class DataRow {
	String[] raw;
	int[] display_y;

	boolean isHidden = false;// excluded

	float[] logR;
	float[] copyNumber;
	int position;

	int domain; //TODO
	float dy_domain;



	DataRow (String[] s){
		logR = new float[_cell_stages];
		copyNumber = new float[_cell_stages];
		display_y = new int[_cell_stages]; //position and domain separate

		position = Integer.parseInt(s[1]);
		for(int i = 0; i< _cell_stages; i++){
			logR[i] = Float.parseFloat(s[2+i]);
			copyNumber[i] = Float.parseFloat(s[8+i]);
			
		}
	} 

	DataRow(int pos){
		this.position = pos;
		logR = new float[_cell_stages];
		copyNumber = new float[_cell_stages];
		display_y = new int[_cell_stages];
	}


	public String toString(){
		return "position:"+position;
	}
}

//karyotype
HashMap<String, Chromosome> refChrMap;
ArrayList<String> chrKeyArray;

Sample[] samples;
int _cell_stages = -1;

Chromosome _selected_chr = null;
int _selected_chr_index = 0;
int _start_pos = 0; //current selected range
int _end_pos = 0;

float _min_logR = -2.0f;
float _max_logR = 2.0f;

float _min_copyNumber = 0f;
float _max_copyNumber = 6.0f;

float left_handle_x = 0;
float right_handle_x = 0;

//domain data


public void initData(){
	//karyotype
	refChrMap = new HashMap<String, Chromosome>();
	chrKeyArray = new ArrayList<String>();

}

public void addSample(int i){
	if(samples == null){
		samples = new Sample[numOfSamples];
		_cell_stages = numOfSamples;
	}
	samples[i] = new Sample(i, sample_names[i]);
}


public void setupSamples(String header){
	String[] s = split(header, TAB);
	//chromosome, position, data
	_cell_stages = (s.length -2)/2;
	println("debug: num of cell stages ="+_cell_stages);

	samples = new Sample[_cell_stages];
	for(int i = 0; i< _cell_stages; i++){
		String sample_label = s[i+2].replace("_logR", "");
		samples[i] = new Sample(i, sample_label);
	}
}

public void preprocess(){
	//create PGraphics
	for(int i = 0; i<samples.length; i++){
		samples[i].setupDisplay();
		int colour =0;
		switch(i){
			case 0: //G1
				colour =  _color_g_seq[0];
				break;
			case 1: //G4
				colour = _color_g_seq[1];
				break;
			case 2: //S1
				colour = _color_b_seq[0];
				break;
			case 3: //S2
				colour = _color_b_seq[1];
				break;
			case 4: //S4
				colour = _color_b_seq[2];
				break;
			case 5: //S6
				colour = _color_b_seq[3];
				break;
			default:
				colour = 180;
				break;
		}
		//assign
		samples[i].stroke_color = colour;
	}

	//set initial parameters
	_selected_chr = (Chromosome) refChrMap.get(chrKeyArray.get(_selected_chr_index));
	_start_pos = 0;
	_end_pos =_selected_chr.end;
	resetHandles();

	//setupDomainView
	setupDomainView();
	updateDomainView();
	
	//karyotype view  
	preprocessKaryotype();
	setupKaryotypeView();
	updateKaryotypeView();

	//GUI
	setupGUI();
	updateGUI();


	isPreprocessing = false;
}

//find centromere
public void preprocessKaryotype(){
	//find centromere start, center, end
	for(int i = 0; i<chrKeyArray.size(); i++){
		Chromosome chr = (Chromosome)refChrMap.get(chrKeyArray.get(i));
		ArrayList<Cytoband> bands = chr.cytobands;
		for(int j = 0; j<bands.size(); j++){
			Cytoband band = bands.get(j);
			if(band.isCentromere){
				if(chr.startAcen == null){
					chr.startAcen = band;
				}else{
					chr.endAcen = band;
				}
			}
			//beginning
			if(j==0){
				band.is_p_end = true;
			}else if(j == chrKeyArray.size()-1){
				band.is_q_end = true;
			}
		}
	}
}

//new chromosome is selected
public void update_new_chromosome(){
	_selected_chr = (Chromosome) refChrMap.get(chrKeyArray.get(_selected_chr_index));
	_start_pos = 0;
	_end_pos =_selected_chr.end;
	left_handle_x = _karyotype_x1;
	right_handle_x = _karyotype_x2;
	for(Sample s: samples){
		//reset counters
		s.redraw();
	}
	//karyotype
	redrawKaryotype();
	//update domain
	updateDomainView();

	loop();
}

public void resetHandles(){
	left_handle_x = _top_plot_x;
	right_handle_x = _top_plot_x + _top_plot_width;
}


//interaction
public void update_left_handle(int m_x){
	//update handle position
	left_handle_x = constrain(m_x, _karyotype_x1, right_handle_x -_MARGIN);
	//update chromosome position
	_start_pos = round(map(left_handle_x, _karyotype_x1, _karyotype_x2, _selected_chr.start, _selected_chr.end));
	for(Sample s: samples){
		//reset counters
		s.redraw();
	}
}

public void update_right_handle(int m_x){
	right_handle_x = constrain(m_x, left_handle_x+_MARGIN, _karyotype_x2);
	_end_pos = constrain(round(map(right_handle_x, _karyotype_x1, _karyotype_x2, _selected_chr.start, _selected_chr.end)),_selected_chr.start, _selected_chr.end) ;
	for(Sample s: samples){
		//reset counters
		s.redraw();
	}
}

public void update_middle(int m_pressed, int m_x){
	int diff = m_x - m_pressed ;
	left_handle_x = constrain(left_handle_x+diff, _karyotype_x1, right_handle_x -_MARGIN);
	_start_pos = round(map(left_handle_x, _karyotype_x1, _karyotype_x2, _selected_chr.start, _selected_chr.end));
	
	right_handle_x = constrain(right_handle_x+diff, left_handle_x+_MARGIN, _karyotype_x2);
	_end_pos = constrain(round(map(right_handle_x, _karyotype_x1, _karyotype_x2, _selected_chr.start, _selected_chr.end)),_selected_chr.start, _selected_chr.end);
	
	// println("debug: diff ="+diff+"  pressed="+m_pressed+"  mouseX="+m_x);
	for(Sample s: samples){
		//reset counters
		s.redraw();
	}
}

//update stacking for each sample
public void update_stacking(){
	// println("debug: update_stacking()");
	float runningY = 0;
	float stacking_gap = seg_line_weight;
	//adjust starting Y
	int showing_sample_count = 0;
	for(int i = 0; i < samples.length; i++){
		Sample s = samples[i];
		if(s.isShowing){
			showing_sample_count ++;
		}
	}
	runningY  = (seg_line_weight*showing_sample_count)/2 * -1;

	for(int i = 0; i < samples.length; i++){
		Sample s = samples[i];
		if(s.isShowing){
			s.stacking = runningY;
			runningY += stacking_gap;
		}else{
			s.stacking = 0;
		}
	}

	// println("debug: update_stacking() _____  end");
}


int _MARGIN = 10;
int _SIDE_MARGIN = _MARGIN*4;
int _RIGHT_MARGIN = _MARGIN*4;

int _STAGE_WIDTH, _STAGE_HEIGHT;

int _header_height = _MARGIN*3;
int _header_dx, _header_dy, _header_width;
Rectangle[] chr_btns;


//scatter plot section
int _scatter_dx;
int _scatter_dy;
int _scatter_height;
int _scatter_width;

int _top_plot_x;
int _top_plot_y;
int _top_plot_width;
int _top_plot_height;

int _bottom_plot_x;
int _bottom_plot_y;
int _bottom_plot_width;
int _bottom_plot_height;

int _plot_top_margin = _MARGIN*3;
int _plot_bottom_margin = _MARGIN;

int seg_line_weight = 5;

//domain track
int _domain_bar_height = _MARGIN*2;
int _domain_d_height = _domain_bar_height + _MARGIN*4;
int _domain_dx, _domain_dy; //display position


//chromosome vie
int _chr_width;
int _chr_height;
int _chr_dx, _chr_dy;

//controller
int _cont_dx, _cont_dy;
int _cont_dx_normal;
int _cont_dx_expanded;
int _cont_height, _cont_width;


//colours
int color_cyan = color(0, 174, 237);
int color_magenta = color(231, 41, 138);
//blue
int _color_b_seq_1 = color(189,215,231);
int _color_b_seq_2 = color(107,174,214);
int _color_b_seq_3 = color(49,130,189);
int _color_b_seq_4 = color(8,81,156);
int[] _color_b_seq = {_color_b_seq_1, _color_b_seq_2, _color_b_seq_3, _color_b_seq_4};
//green
int _color_g_seq_1 = color(186,228,179);
int _color_g_seq_2 = color(116,196,118);
int _color_g_seq_3 = color(49,163,84);
int _color_g_seq_4 = color(0,109,44);
int[] _color_g_seq = {_color_g_seq_1, _color_g_seq_2, _color_g_seq_3, _color_g_seq_4};
//red
int _color_r_seq_1 = color(252, 174, 145);
int _color_r_seq_2 = color(251, 106, 74);
int _color_r_seq_3 = color(222, 45, 38);
int _color_r_seq_4 = color(165, 15, 21);
int[] _color_r_seq = {_color_r_seq_1, _color_r_seq_2, _color_r_seq_3, _color_r_seq_4};

//gray
int _color_gray_seq_1 = color(0, 37);
int _color_gray_seq_2 = color(0, 99);
int _color_gray_seq_3 = color(0, 150);
int _color_gray_seq_4 = color(0, 204);

//purple
int _color_p_seq_1 = color(203,201,226);
int _color_p_seq_2 = color(158,154,200);
int _color_p_seq_3 = color(117,107,177);
int _color_p_seq_4 = color(84,39,143);

//orange
int _color_o_seq_1 = color(253,190,133);
int _color_o_seq_2 = color(253,141,60);
int _color_o_seq_3 = color(230,85,13);
int _color_o_seq_4 = color(166,54,3);
//

int[] _color_array = {
					  _color_g_seq_1, _color_g_seq_2, _color_g_seq_3, _color_g_seq_4, 
					  _color_b_seq_1, _color_b_seq_2, _color_b_seq_3, _color_b_seq_4,
					  _color_p_seq_1, _color_p_seq_2, _color_p_seq_3, _color_p_seq_4,
					  _color_r_seq_1, _color_r_seq_2, _color_r_seq_3, _color_r_seq_4,
					  _color_o_seq_1, _color_o_seq_2, _color_o_seq_3, _color_o_seq_4,
					  _color_gray_seq_1, _color_gray_seq_2, _color_gray_seq_3, _color_gray_seq_4};




//large number formatting
char[] c = new char[]{'k', 'm', 'b', 't'};
String[] suffix = new String[]{"","k", "m", "b", "t"};
int MAX_LENGTH = 4;


int _draw_counter = 0; //counter


public void setupDisplay(){
	println("display size:"+displayWidth+" "+displayHeight);

	_header_dx = 0;
	_header_dy = 0;
	_header_width = displayWidth;

	//karyotype
	_chr_width = displayWidth;
	_chr_height = _karyotype_margin*2 + _karyotype_height;
	_chr_dx = 0;
	_chr_dy = _header_height;

	//determine ratio
	int remaining_height_pixel = _STAGE_HEIGHT - _header_height - _chr_height;

	//scatter plot section
	_scatter_dx = 0;
	_scatter_dy = _header_height + _chr_height;
	// _scatter_height = round(remaining_height_pixel*0.7);
	_scatter_height = remaining_height_pixel - _MARGIN*15; //configuration height
	_scatter_width = displayWidth;

	//check if domain is loaded
	int plot_height;
	if(hasDomainFile){
		int remaining = _scatter_height - _domain_d_height;
		plot_height  = remaining/2;
		_domain_dx = 0;
		_domain_dy = _header_height + _chr_height + plot_height*2;
	}else{
		plot_height = _scatter_height/2;
	}

	_top_plot_x = _SIDE_MARGIN;
	_top_plot_y = _scatter_dy +_plot_top_margin;
	_top_plot_width = _scatter_width -_SIDE_MARGIN -_RIGHT_MARGIN;
	_top_plot_height = plot_height - _plot_top_margin - _plot_bottom_margin; 

	_bottom_plot_x = _SIDE_MARGIN;
	_bottom_plot_y = _scatter_dy+plot_height+_plot_top_margin;
	_bottom_plot_width = _top_plot_width;
	_bottom_plot_height = _top_plot_height;

	//controller
	_cont_dx = 0;
	_cont_dy = _scatter_dy + _scatter_height;
	_cont_width = _STAGE_WIDTH;
	_cont_height = _STAGE_HEIGHT - _cont_dy;


	//header buttons
	int runningX = _SIDE_MARGIN;
	int runningY = 0;
	chr_btns = new Rectangle[chrKeyArray.size()];
	for(int i = 0; i< chrKeyArray.size(); i++){
		String label = chrKeyArray.get(i).replace("hs", "chr");
		int text_width = round(textWidth(label));
		chr_btns[i] = new Rectangle(runningX, runningY, text_width, _header_height);
		runningX += text_width +_MARGIN*2;
	}

	//update stacking
	update_stacking();


}

public void draw(){
	background(240);
	// line(_chr_dx, _chr_dy, _chr_dx+_chr_width, _chr_dy);
	// println("top:"+_top_plot_x+","+_top_plot_y+","+_top_plot_width+","+_top_plot_height);
	if(!filesSelected){
		drawDataLoadingPage();
	}else{
		if(isLoadingData){
			//loading
			fill(180);
			textAlign(CENTER, CENTER);
			text("Loading Data ... ", displayWidth/2, displayHeight/2);
		}else if(isPreprocessing){
			fill(180);
			textAlign(CENTER, CENTER);
			text("Processing Data ... ", displayWidth/2, displayHeight/2);
		}else{
			//header area
			fill(180);
			noStroke();
			rect(_header_dx, _header_dy, _header_width, _header_height);
			//header text
			drawHeader();
			
			//karyotype
			image(_k, _chr_dx, _chr_dy);

			//plot area
			fill(255);
			noStroke();
			rectMode(CORNER);
			rect(_top_plot_x, _top_plot_y, _top_plot_width, _top_plot_height);
			rect(_bottom_plot_x, _bottom_plot_y, _bottom_plot_width, _bottom_plot_height);
			
			//drawing grid
			drawGrid();

			// //update each sample view and add to screen
			boolean updated = false;

			for(int i = 0; i < samples.length; i++){
			// for(int i = 0; i < 1; i++){
				Sample s = samples[i];
				if(s.isShowing){
					if(showingRawData){
						updated |= s.updateRawDataView();
						image(s._raw, _scatter_dx, _scatter_dy);
					}
					if(showingSegData){
						updated |= s.updateSegDataView();
						image(s._seg, _scatter_dx, _scatter_dy);  
					}
				}
				// s.updateLoessDataView();
			}
			//domain
			if(hasDomainFile){
				image(_domain, _domain_dx, _domain_dy);
			}

			//GUI
			image(_gui, _cont_dx, _cont_dy);


			//draw label and overflow
			drawLabelAndOverflow();

			drawInteraction();

			//
			drawMousePosition();


			drawMessage();

			if(!updated){
				_draw_counter ++;
				if(_draw_counter > 10){
					// println("debug: pause loop ------");
					noLoop();
				}
			}
		}
		
	}
}

public void drawGrid(){
	//logR
	//y axis
	int tick_count = 6;
	float span_float = _max_logR - _min_logR;
	float step_int = pow(10, floor(log(span_float/tick_count)/log(10)));
	float err_int = (float)tick_count/(float)span_float * step_int;

	if(err_int <= 0.15f){
		step_int *= 10;
	}else if(err_int <= 0.35f){
		step_int *= 5;
	}else if(err_int <= 0.75f){
		step_int *= 2;
	}
	// println("step ="+step_int+"  err="+err_int );
	float extent_1_int = ceil(_min_logR/step_int)*step_int;
	float extent_2_int = floor(_max_logR/step_int)*step_int+step_int*0.5f;

	//font
	textFont(label_font, 10);

	for(float f = extent_1_int; f < extent_2_int; f+=step_int){
		float dy = map(f, _min_logR, _max_logR,  _top_plot_y+_top_plot_height, _top_plot_y);
		stroke(240);
		strokeWeight(1);
		noFill();
		line(_top_plot_x, dy, _top_plot_x+_top_plot_width, dy);
		//tick value
		// fill(120);
		// textAlign(RIGHT, CENTER);
		// text(nf(f, 0, 1), _top_plot_x, dy);
	}

	//copy number
	tick_count = 6;
	span_float = _max_copyNumber - _min_copyNumber;
	step_int = pow(10, floor(log(span_float/tick_count)/log(10)));
	err_int = (float)tick_count/(float)span_float * step_int;

	if(err_int <= 0.15f){
		step_int *= 10;
	}else if(err_int <= 0.35f){
		step_int *= 5;
	}else if(err_int <= 0.75f){
		step_int *= 2;
	}
	// println("step ="+step_int+"  err="+err_int );
	extent_1_int = ceil(_min_copyNumber/step_int)*step_int;
	extent_2_int = floor(_max_copyNumber/step_int)*step_int+step_int*0.5f;

	//font
	textFont(label_font, 10);

	for(float f = extent_1_int; f < extent_2_int; f+=step_int){
		float dy = map(f, _min_copyNumber, _max_copyNumber,  _bottom_plot_y+_bottom_plot_height, _bottom_plot_y);
		stroke(240);
		strokeWeight(1);
		noFill();
		line(_bottom_plot_x, dy, _bottom_plot_x+_bottom_plot_width, dy);
		//tick value
		// fill(120);
		// textAlign(RIGHT, CENTER);
		// text(nf(f, 0, 0), _bottom_plot_x, dy);
	}
	//x axis
	// tick_count = 10;
	// int span_int = _end_pos - _start_pos;
	// step_int = pow(10, floor(log(span_int/tick_count)/log(10)));
	// err_int = (float)tick_count/(float)span_int * step_int;


	// if(err_int <= 0.15){
	// 	step_int *= 10;
	// }else if(err_int <= 0.35){
	// 	step_int *= 5;
	// }else if(err_int <= 0.75){
	// 	step_int *= 2;
	// }
	// // println("step ="+step_int+"  err="+err_int );
	// extent_1_int = ceil(_start_pos/step_int)*step_int;
	// extent_2_int = floor(_end_pos/step_int)*step_int+step_int*0.5;
	// for(float f = extent_1_int; f < extent_2_int; f+=step_int){
	// 	float dx = map(f, _start_pos, _end_pos,  _top_plot_x, _top_plot_x +_top_plot_width);
	// 	stroke(220);
	// 	strokeWeight(1);
	// 	noFill();
	// 	line(dx, _top_plot_y, dx, _top_plot_y+_top_plot_height);
	// 	//tick value
	// 	fill(120);
	// 	textAlign(CENTER, TOP);
	// 	text(largeNumberFormat(f,0), dx, _top_plot_y+_top_plot_height);
	// }

	//set font back
	textFont(font, 12);
}

public void drawLabelAndOverflow(){
	//over flow
	noStroke();
	fill(240);
	rect(_scatter_dx, _scatter_dy, _SIDE_MARGIN, _scatter_height);
	rect(_top_plot_x+_top_plot_width, _scatter_dy, _SIDE_MARGIN, _scatter_height);

	//Labels
	//logR
	//y axis
	int tick_count = 6;
	float span_float = _max_logR - _min_logR;
	float step_int = pow(10, floor(log(span_float/tick_count)/log(10)));
	float err_int = (float)tick_count/(float)span_float * step_int;

	if(err_int <= 0.15f){
		step_int *= 10;
	}else if(err_int <= 0.35f){
		step_int *= 5;
	}else if(err_int <= 0.75f){
		step_int *= 2;
	}
	// println("step ="+step_int+"  err="+err_int );
	float extent_1_int = ceil(_min_logR/step_int)*step_int;
	float extent_2_int = floor(_max_logR/step_int)*step_int+step_int*0.5f;

	//font
	textFont(label_font, 10);

	for(float f = extent_1_int; f < extent_2_int; f+=step_int){
		float dy = map(f, _min_logR, _max_logR,  _top_plot_y+_top_plot_height, _top_plot_y);
		//tick value
		fill(120);
		textAlign(RIGHT, CENTER);
		text(nf(f, 0, 1), _top_plot_x, dy);
	}

	//copy number
	tick_count = 6;
	span_float = _max_copyNumber - _min_copyNumber;
	step_int = pow(10, floor(log(span_float/tick_count)/log(10)));
	err_int = (float)tick_count/(float)span_float * step_int;

	if(err_int <= 0.15f){
		step_int *= 10;
	}else if(err_int <= 0.35f){
		step_int *= 5;
	}else if(err_int <= 0.75f){
		step_int *= 2;
	}
	// println("step ="+step_int+"  err="+err_int );
	extent_1_int = ceil(_min_copyNumber/step_int)*step_int;
	extent_2_int = floor(_max_copyNumber/step_int)*step_int+step_int*0.5f;

	//font
	textFont(label_font, 10);

	for(float f = extent_1_int; f < extent_2_int; f+=step_int){
		float dy = map(f, _min_copyNumber, _max_copyNumber,  _bottom_plot_y+_bottom_plot_height, _bottom_plot_y);
		//tick value
		fill(120);
		textAlign(RIGHT, CENTER);
		text(nf(f, 0, 0), _bottom_plot_x, dy);
	}

	//labels
	textAlign(CENTER, TOP);
	pushMatrix();
	translate(0  + _MARGIN, _top_plot_y+_top_plot_height/2);
	rotate(-HALF_PI);
	text("LogR", 0, 0);
	popMatrix();
	textAlign(CENTER, TOP);
	pushMatrix();
	translate(0  + _MARGIN, _bottom_plot_y+_bottom_plot_height/2);
	rotate(-HALF_PI);
	text("Copy Number", 0, 0);
	popMatrix();

	if(hasDomainFile){
		textAlign(LEFT, TOP);
		pushMatrix();
		translate(0  + _MARGIN, _domain_dy+_MARGIN+_domain_bar_height);
		rotate(-HALF_PI);
		text("Domain", 0, 0);
		popMatrix();
	}
}


public void drawHeader(){
	//header background
	textAlign(CENTER, CENTER);
	for(int i = 0; i< chrKeyArray.size(); i++){
		String label = chrKeyArray.get(i).replace("hs", "chr");
		Rectangle rect = chr_btns[i];
		if(i == _selected_chr_index){
			fill(20);
		}else{
			fill(255);
		}
		text(label, (float)rect.getCenterX(), (float)rect.getCenterY());
	}
}

public void drawInteraction(){
	//font
	textFont(label_font, 10);
	//draw handles
	String left_cb = _selected_chr.getCytobandName(_start_pos);
	String right_cb = _selected_chr.getCytobandName(_end_pos);

	//left
	fill(80);
	textAlign(RIGHT, BOTTOM);
	text(left_cb, left_handle_x -2, _k_d_rect.y);
	//bp
	text(largeNumberFormat(_start_pos), left_handle_x-2, _k_d_rect.y+_k_d_rect.height+_MARGIN+2);


	//right
	textAlign(LEFT, BOTTOM);
	text(right_cb, right_handle_x +2, _k_d_rect.y);
	//bp
	text(largeNumberFormat(_end_pos), right_handle_x +2, _k_d_rect.y+_k_d_rect.height+_MARGIN+2);

	//selected region
	if(_start_pos > _selected_chr.start || _end_pos < _selected_chr.end){
		//draw selected region
		stroke(255);
		strokeWeight(3);
		fill(255, 60);
		rectMode(CORNERS);
		rect(left_handle_x, _k_d_rect.y-3, right_handle_x, _k_d_rect.y+_k_d_rect.height+3);
		rectMode(CORNER);
	}


	//hovering
	if(hovering_l_handle){
		stroke(color_cyan);
		strokeWeight(3);
		line(left_handle_x, _k_d_rect.y, left_handle_x, _k_d_rect.y+_k_d_rect.height);
	}else if(hovering_r_handle){
		stroke(color_cyan);
		strokeWeight(3);
		line(right_handle_x, _k_d_rect.y, right_handle_x, _k_d_rect.y+_k_d_rect.height);
	}

	//dragging color
	if(color_picked){
		rectMode(CENTER);
		fill(_color_array[picked_color_index]);
		stroke(_color_array[picked_color_index]);
		rect(mouseX, mouseY, _MARGIN, _MARGIN);
		rectMode(CORNER);
	}

	//set font back
	textFont(font, 12);
}

public void drawMousePosition(){
	textFont(label_font, 10);
	if(region_selected){
		//draw highlighted region
		noFill();
		stroke(color_magenta);
		strokeWeight(1);
		line(region_left, _scatter_dy, region_left, _scatter_dy+_scatter_height);
		line(region_right, _scatter_dy, region_right, _scatter_dy+_scatter_height);

		fill(color_magenta);
		textAlign(RIGHT, BOTTOM);
		text(region_left_bp, region_left, _scatter_dy+_scatter_height);
		textAlign(LEFT, BOTTOM);
		text(region_right_bp, region_right+2, _scatter_dy+_scatter_height);

		//length
		int length = region_right_bp - region_left_bp;
		float mid_point = (region_left+region_right)/2;
		textAlign(CENTER, TOP);
		text(largeNumberFormat(length)+" bp", mid_point, _scatter_dy+_scatter_height+2);

	}else{
		//hoverling action
		if(_scatter_dy < mouseY  && mouseY < _scatter_dy+_scatter_height){
			if(_top_plot_x < mouseX && mouseX < _top_plot_x+_top_plot_width){
				noFill();
				stroke(color_magenta);
				strokeWeight(1);
				line(mouseX, _scatter_dy, mouseX, _scatter_dy+_scatter_height);

				int current_bp = getBpPosition(mouseX);//round(map(mouseX, _top_plot_x, _top_plot_x+_top_plot_width, _start_pos,_end_pos));
				fill(color_magenta);
				textAlign(RIGHT, BOTTOM);
				text(current_bp, mouseX, _scatter_dy+_scatter_height);
			}

		}
		
	}
	textFont(font, 12);
}


public String largeNumberFormat(double n, int iteration){
	// double d = ((long) n / 100) / 10.0;
	// boolean isRound = (d * 10) %10 == 0;//true if the decimal part is equal to 0 (then it's trimmed anyway)
	
	double d = ((long) n / 100) / 10.0f;
	boolean isRound = (d * 10) %10 == 0;//true if the decimal part is equal to 0 (then it's trimmed anyway)
		return (d < 1000? //this determines the class, i.e. 'k', 'm' etc
	        ((d > 99.9f || isRound || (!isRound && d > 9.99f)? //this decides whether to trim the decimals
	         (int) d * 10 / 10 : d + "" // (int) d * 10 / 10 drops the decimal
	         ) + "" + c[iteration]) 
	        : largeNumberFormat((float)d, iteration+1));
}

public String largeNumberFormat(double number) {
    String r = new DecimalFormat("##0E0").format(number);
    r = r.replaceAll("E[0-9]", suffix[Character.getNumericValue(r.charAt(r.length()-1))/3]);
    return r.length()>MAX_LENGTH ?  r.replaceAll("\\.[0-9]+", "") : r;
}


public void drawMessage(){
	if(saved_output){
		fill(180);
		textAlign(LEFT, TOP);
		// textFont(label_font, 10);
		text("saved note", output_x, _cont_dy+output_btn.y);
		// textFont(font, 12);
	}
}

public void redraw_seg(){
	for(int i = 0; i < samples.length; i++){
		Sample s = samples[i];
		if(s.isShowing){
			s.redraw_seg();
		}
	}
}


//Domain view
PGraphics _domain;

// int _domain_bar_height = _MARGIN*2;
// int _domain_d_height = _domain_bar_height + _MARGIN*2;

int _domain_x1, _domain_x2;
int _domain_y1, _domain_y2;

int _late_domain_color, _early_domain_color;

boolean showing_late_domain = true;
boolean showing_early_domain = true;



public void setupDomainView(){
	_domain = createGraphics(displayWidth, _domain_d_height);
	_domain_x1 = _SIDE_MARGIN;
	_domain_x2 = displayWidth - _RIGHT_MARGIN;
	_domain_y1 = _MARGIN;
	_domain_y2 = _domain_y1 + _domain_bar_height;

	//default color settin
	_late_domain_color = _color_gray_seq_2;
	_early_domain_color = _color_gray_seq_3;
}


public void updateDomainView(){
	_domain.beginDraw();
	_domain.clear();
	Chromosome chr = _selected_chr;

	//backgound
	_domain.rectMode(CORNERS);
	_domain.fill(255);
	_domain.noStroke();
	_domain.rect(_domain_x1, _domain_y1, _domain_x2, _domain_y2);

	//domains
	for(Range range: chr.domains){
		float r_start = map(range.start, _start_pos, _end_pos, _domain_x1, _domain_x2);
		float r_end = map(range.end, _start_pos, _end_pos, _domain_x1, _domain_x2);
		_domain.fill(range.value == 0 ?_late_domain_color:_early_domain_color); //0 is late

		if(range.value == 0){
			//late
			if(showing_late_domain){
				_domain.fill(_late_domain_color);
				_domain.rect(r_start, _domain_y1, r_end, _domain_y2);
			}
		}else{
			//early
			if(showing_early_domain){
				_domain.fill(_early_domain_color);
				_domain.rect(r_start, _domain_y1, r_end, _domain_y2);
			}
		}

	}


	_domain.endDraw();
}
long _total_data = 0;
long _current_data = 0;
int _current_percentage = 0;

String _current_load_message = "";
String _file_url = "AllRawData.txt"; 
String _seg_file_url = "AllSegmentationData.txt";

int _line_counter = 0;


boolean isTestMode = false;

class FileLoader implements Runnable{
	public void run(){
		initData();
		//load karyotype data
		loadKaryotype();
		println("karyotype data loaded -- chr count ="+chrKeyArray.size()+":"+ chrKeyArray);

		if(isTestMode){
			//deprecated
		}else{
			//check the _total_data
			calculateTotalData();
			//load by samples
			for(int i = 0; i < sample_log_file.length; i++){
				File f = sample_log_file[i];
				loadLogR(f, i);
				f = sample_cn_file[i];
				loadCN(f, i);
			}

			if(domain_file != null){
				loadDomainFile();
			}
			println("debug: data loaded = "+_current_data);
				
		}


		//debug	

		// for(String key : chrKeyArray){
		// 	Chromosome chr =(Chromosome)refChrMap.get(key);
		// 	println("debug:"+chr.label);
		// }
		finishLoading();
	}
}

public void calculateTotalData(){
	_total_data = 0;
	for(File f: sample_log_file){
		_total_data += f.length();
	}
	for(File f: sample_cn_file){
		_total_data += f.length();
	}
	println("debug: total file size is "+_total_data);
}

public void finishLoading(){
	isLoadingData = false;
	isPreprocessing = true;
	println("debug: ----- finished loading ----------");
	//setup display parameters
	setupDisplay();
	preprocess();  //DataSet
}

public void loadKaryotype(){
	File f = new File(dataPath("karyotype.human.hg19.txt"));
    try{
        BufferedReader reader = createReader(f);
        String line = null;
        while ((line = reader.readLine()) != null) {
            parseKaryotypetData(line);
        }
    } catch (IOException e) {
        e.printStackTrace();
    }
}
public void parseKaryotypetData(String line){
	String[] s = split(line, " ");
	if(s[0].equals("chr")){
		String key = s[2].trim(); //e.g. hs1
		String label = s[6].trim(); //e.g. chr1
		int start = Integer.parseInt(s[4]);
		int end = Integer.parseInt(s[5]);

		Chromosome chr = new Chromosome(key, label, start, end);
		chrKeyArray.add(key);
		refChrMap.put(key, chr);
	}else if(s[0].equals("band")){
		String key = s[1].trim();
		String bandID = s[2].trim();
		int start = Integer.parseInt(s[4]);
		int end = Integer.parseInt(s[5]);
		String stain = s[6].trim();
		int stainValue = getStainValue(stain);

		//finc chromosome
		Chromosome chr = (Chromosome)refChrMap.get(key);
		if(chr == null){
			println("Error: chr is null.");
		}else{
			//create cytoband
			Cytoband cb = new Cytoband(start, end, bandID, stainValue);
			chr.add_cytoband(cb);
		}
	}else{
		println("debug: other unknown line:"+line);
	} 
}

public void loadLogR(File f, int index){
	_line_counter = 0;
    try{
        BufferedReader reader = createReader(f);
        String line = null;
        while ((line = reader.readLine()) != null ) {
        	if(_line_counter == 0){
        		//header
        		println("debug: logR header="+line);
        		addSample(index);
        		_line_counter ++;
        		_current_data += line.length();
        	}else{
        		parseLogR(index, line);
        		_line_counter ++;
        		_current_data += line.length();
        	}

        }	
    } catch (IOException e) {
        e.printStackTrace();
    }
}

public void parseLogR(int index, String line){
	String[] s = split(line, TAB);
	//check chromosome
	String chr_key = "hs"+s[0].trim();
	int position = Integer.parseInt(s[1]);
	float value = Float.parseFloat(s[2]);
	float seg = Float.parseFloat(s[3]);

	//get Chromosome
	Chromosome chr = (Chromosome) refChrMap.get(chr_key);
	if(chr == null){
		println("Error: Chromosome not found:"+chr_key);
	}else{
		//get DataRow
		DataRow row = (DataRow) chr.dataRowMap.get(new Integer(position));
		if(row == null){
			//new row
			row = new DataRow(position);
			chr.dataRowMap.put(new Integer(position), row);
			chr.rows.add(row);
		}
		//save data
		//logR
		row.logR[index] = value;
		//seegmentation
		samples[index].load_logR_seg(seg, row, chr_key);
	} 
}

public void loadCN(File f, int index){
	_line_counter = 0;
    try{
        BufferedReader reader = createReader(f);
        String line = null;
        while ((line = reader.readLine()) != null ) {
        	if(_line_counter == 0){
        		//header
        		println("debug: CN header="+line);
        		_line_counter ++;
        		_current_data += line.length();
        	}else{
        		parseCN(index, line);
        		_line_counter ++;
        		_current_data += line.length();
        	}

        }	
    } catch (IOException e) {
        e.printStackTrace();
    }
}

public void parseCN(int index, String line){
	String[] s = split(line, TAB);
	//check chromosome
	String chr_key = "hs"+s[0].trim();
	int position = Integer.parseInt(s[1]);
	float value = Float.parseFloat(s[2]);
	float seg = Float.parseFloat(s[3]);

	//get Chromosome
	Chromosome chr = (Chromosome) refChrMap.get(chr_key);
	if(chr == null){
		println("Error: Chromosome not found:"+chr_key);
	}else{
		//get DataRow
		DataRow row = (DataRow) chr.dataRowMap.get(new Integer(position));
		if(row == null){
			println("Error: DataRow is not found for CopyNumber:"+position);
			//new row
			// row = new DataRow(position);
			// chr.dataRowMap.put(new Integer(position), row);
		}
		//save datt
		//CN
		row.copyNumber[index] = value;
		//seegmentation
		samples[index].load_cn_seg(seg, row, chr_key);
	} 
}


//cytoband
public int getStainValue(String stain){
	String gStain = stain.substring(4);
    int value;
        if(stain.equals("acen")){
        	//centromere
        	value = -1;
        }else if(stain.equals("gvar")){
        	value = 100;
        }else if(stain.equals("stalk")){
        	value = 0;
        }else if(gStain.equals("")){
	        value = 0;
        }else{
            value = Integer.parseInt(gStain);
        }
     return value;
}

//loading domain track
public void loadDomainFile(){
	hasDomainFile = true;
	File f = domain_file;
	String[] lines = loadStrings(f);
	for(String line:lines){
		line = line.replaceAll("\"", ""); //get rid of quatation marks
		String[] s = split(line, TAB);
		if(s[0].equals("Chromosome")){
			//header
		}else{
			int chr_num = Integer.parseInt(s[0].trim());
			int start = Integer.parseInt(s[1].trim());
			int stop = Integer.parseInt(s[6].trim());
			int value = Integer.parseInt(s[5].trim());

			String chr_key = "hs"+chr_num;
			if(chr_num == 23){
				chr_key = "hsX";
			}else if(chr_num == 24){
				chr_key = "hsY";
			}
			//find chromosome
			Chromosome chr = (Chromosome) refChrMap.get(chr_key);
			Range r = new Range(chr_num, start, stop, value);
			chr.domains.add(r);
		}
	}

	println("Debug. domain information loaded!!!");
}



public void loadSegData(String url){
	File f = new File(dataPath(url));
	_line_counter = 0;
    try{
        BufferedReader reader = createReader(f);
        String line = null;
        while ((line = reader.readLine()) != null ) {
        	if(_line_counter == 0){
        		//header
        		println("debug: header="+line);
        		_line_counter ++;
        	}else{
        		parseSegData(line);
        		_line_counter ++;
        	}

        }	
    } catch (IOException e) {
        e.printStackTrace();
    }	
    //debug
    //chr1, sample 0
    // ArrayList<Segment> segments_0 = (ArrayList<Segment>)samples[0].logSegMap.get("hs1");
    // println("debug:"+samples[0].label+", chr1 has "+segments_0.size()+" segments");

    println("debug:"+url+" has "+_line_counter+" lines");
}

public void parseSegData(String line){
	String[] s = split(line, TAB);
	//check chromosome
	String chr_key = "hs"+s[0].trim();
	//get Chromosome
	Chromosome chr = (Chromosome) refChrMap.get(chr_key);
	if(chr == null){
		println("Error: Chromosome not found:"+chr_key);
	}else{
		//find DataRow
		int pos = Integer.parseInt(s[1]);
		DataRow dr = (DataRow) chr.dataRowMap.get(new Integer(pos));
		if(dr != null){
			//segmentation information
			for(int i = 0; i< samples.length; i++){
				float log_seg = Float.parseFloat(s[2+i]);
				float cn_seg = Float.parseFloat(s[8+i]);

				samples[i].load_logR_seg(log_seg, dr, chr_key);
				samples[i].load_cn_seg(cn_seg, dr, chr_key);
			}

		}else{
			//not found
		}
	}
}



//legend  and options
PGraphics _gui;
boolean gui_expanded = false;

Rectangle[] datatype_btns; //seg or raw
String[]  datatypes_labels = {"SEG", "RAW"};
int gap_bw_btns = 2;

String[] gui_columns = {"TYPES", "SAMPLES","DOMAIN", "COLOUR", "REPRESENTATION", "STACKING"};
int[] gui_label_x;
int gui_label_y;
int gui_label_height = 12;

Rectangle[] sample_btns;
Rectangle[] domain_btns;
Rectangle[] color_pallet;

String[] encoding_labels = {"ELLIPSE", "DOT", "LINE"};
boolean[] encoding_selected = {true, false, false};
Rectangle[] encoding_btns;

//stacking
String[] stacking_labels = {"CN"};
Rectangle[] stacking_btns;


//output 
Rectangle output_file_name;
String[] output_header= {"chr", "start", "stop", "note"};
Rectangle output_note_rect;
Rectangle output_btn;
int output_x;
int[] output_label_x;
int output_label_y;

boolean output_file_name_active = false;
boolean output_note_active = false;
String output_file_url= "";
String output_note_input = "";



public void setupGUI(){
	_gui = createGraphics(_cont_width, _cont_height);
	//set up button positions etc...
	//data type
	int runningX = _SIDE_MARGIN;
	int runningY = _MARGIN*2; //header;
	gui_label_x = new int[gui_columns.length];
	//data types
	gui_label_x[0] = runningX;
	gui_label_y = runningY;
	runningY += gui_label_height;

	datatype_btns = new Rectangle[2];
	datatype_btns[0] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
	runningY += _MARGIN + gap_bw_btns;
	datatype_btns[1] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
	runningY += _MARGIN + gap_bw_btns;

	//sample buttons
	runningX += round(textWidth(gui_columns[0]))+_MARGIN*2;
	runningY = _MARGIN*2+gui_label_height;
	gui_label_x[1] = runningX;
	sample_btns = new Rectangle[samples.length];
	for(int i = 0; i < samples.length;i++){
		if(i%6 == 0 && i >0){ //more than 6 samples
			runningX += round(textWidth(gui_columns[1]));
			runningY = _MARGIN*2+gui_label_height;
		}
		sample_btns[i] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
		runningY += _MARGIN + gap_bw_btns;
	}

	//domain buttons
	if(hasDomainFile){
		runningX += round(textWidth(gui_columns[1]))+_MARGIN*2;
		runningY = _MARGIN*2+gui_label_height;
		gui_label_x[2] = runningX;
		domain_btns = new Rectangle[2];
		domain_btns[0] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
		runningY += _MARGIN + gap_bw_btns;
		domain_btns[1] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
	}


	//color pallet
	color_pallet = new Rectangle[_color_array.length];
	if(hasDomainFile){
		runningX += round(textWidth(gui_columns[2]))+_MARGIN*2;
	}else{
		runningX += round(textWidth(gui_columns[1]))+_MARGIN*2;		
	}
	runningY = _MARGIN*2+gui_label_height;
	gui_label_x[3] = runningX;

	int indexCounter = 0;
	int initial_x = runningX;
	//sequential 6 sets
	for(int i = 0 ; i < 6; i++){
		//4 colours
		for(int j = 0; j<4; j++){
			color_pallet[indexCounter] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
			runningX += _MARGIN+gap_bw_btns;
			indexCounter ++;
		}
		//next row
		runningX = initial_x;
		runningY += _MARGIN+gap_bw_btns;
	}

	//encoding
	runningX += round(textWidth(gui_columns[3]))+_MARGIN*2;
	runningY = _MARGIN*2+gui_label_height;
	gui_label_x[4] = runningX;
	encoding_btns = new Rectangle[encoding_labels.length];
	for(int i = 0; i < encoding_labels.length; i++){
		encoding_btns[i] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
		runningY += _MARGIN+gap_bw_btns;
	}

	//stacking
	runningX += round(textWidth(gui_columns[4]));
	runningY = _MARGIN*2+gui_label_height;
	gui_label_x[5] = runningX;
	stacking_btns = new Rectangle[stacking_labels.length];
	for(int i = 0; i < stacking_labels.length; i++){
		stacking_btns[i] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
		runningY += _MARGIN+gap_bw_btns;
	}

	//output file name
	runningX = _STAGE_WIDTH - _SIDE_MARGIN - fileName_width;
	runningY = _MARGIN*2;
	output_x = runningX;
	output_file_name = new Rectangle(runningX, runningY, fileName_width, round(_MARGIN*1.5f));
	
	//ouptut
	runningY += _MARGIN*2;
	output_label_x = new int[3];
	output_label_x[0] = runningX;
	runningX += _MARGIN*5;
	output_label_x[1] = runningX;
	runningX += _MARGIN*10;
	output_label_x[2] = runningX;
	output_label_y = runningY;

	runningX = _STAGE_WIDTH - _SIDE_MARGIN - fileName_width;
	runningY += _MARGIN*2.5f;

	//note
	output_note_rect = new Rectangle(runningX, runningY, fileName_width, round(_MARGIN*1.5f));
	runningY+= _MARGIN*2 ;

	//output button
	runningX = _STAGE_WIDTH - _SIDE_MARGIN - btn_width;
	output_btn = new Rectangle(runningX, runningY, btn_width, btn_height);

	//output file neme
	output_file_url = "analysis_"+timestamp();

}

public void updateGUI(){
	_gui.beginDraw();
	_gui.clear();
	_gui.background(220);
	//font
	_gui.textFont(label_font, 10);
	//datatype buttons
	_gui.fill(120);
	_gui.textAlign(LEFT, TOP);
	_gui.text(gui_columns[0], gui_label_x[0], gui_label_y);
	//segmentation
	_gui.noFill();
	_gui.stroke(120);
	_gui.rect(datatype_btns[0].x, datatype_btns[0].y, datatype_btns[0].width, datatype_btns[0].height);
	if(showingSegData){
		//draw cross
		_gui.line(datatype_btns[0].x, datatype_btns[0].y,datatype_btns[0].x+datatype_btns[0].width, datatype_btns[0].y +datatype_btns[0].height);
		_gui.line(datatype_btns[0].x+datatype_btns[0].width, datatype_btns[0].y ,datatype_btns[0].x, datatype_btns[0].y +datatype_btns[0].height);
	}
	_gui.fill(120);
	_gui.textAlign(LEFT, CENTER);
	_gui.text(datatypes_labels[0], datatype_btns[0].x+datatype_btns[0].width + 5, (float)datatype_btns[0].getCenterY());

	//logR
	_gui.noFill();
	_gui.stroke(120);
	_gui.rect(datatype_btns[1].x, datatype_btns[1].y, datatype_btns[1].width, datatype_btns[1].height);
	if(showingRawData){
		//draw cross
		_gui.line(datatype_btns[1].x, datatype_btns[1].y,datatype_btns[1].x+datatype_btns[1].width, datatype_btns[1].y +datatype_btns[1].height);
		_gui.line(datatype_btns[1].x+datatype_btns[1].width, datatype_btns[1].y ,datatype_btns[1].x, datatype_btns[1].y +datatype_btns[1].height);
	}
	_gui.fill(120);
	_gui.textAlign(LEFT, CENTER);
	_gui.text(datatypes_labels[1], datatype_btns[1].x+datatype_btns[1].width + 5, (float)datatype_btns[1].getCenterY());


	//sample buttons
	_gui.fill(120);
	_gui.textAlign(LEFT, TOP);
	_gui.text(gui_columns[1], gui_label_x[1], gui_label_y);
	for(int i = 0; i < samples.length;i++){
		Sample s = samples[i];
		Rectangle rect = sample_btns[i];
		_gui.stroke(s.stroke_color);
		if(s.isShowing){
			_gui.fill(s.stroke_color);
		}else{
			_gui.noFill();
		}
		_gui.rect(rect.x, rect.y, rect.width, rect.height);
		//text
		_gui.fill(120);
		_gui.textAlign(LEFT, CENTER);
		_gui.text(s.label, rect.x+rect.width + 5, (float)rect.getCenterY());
	}
	//domain buttons
	if(hasDomainFile){
		_gui.fill(120);
		_gui.textAlign(LEFT, TOP);
		_gui.text(gui_columns[2], gui_label_x[2], gui_label_y);
		Rectangle rect = domain_btns[0];
		_gui.stroke(_late_domain_color);
		if(showing_late_domain){
			_gui.fill(_late_domain_color);
		}else{
			_gui.noFill();
		}
		_gui.rect(rect.x, rect.y, rect.width, rect.height);
		//text
		_gui.fill(120);
		_gui.textAlign(LEFT, CENTER);
		_gui.text("LATE", rect.x+rect.width + 5, (float)rect.getCenterY());

		//early domain
		rect = domain_btns[1];
		_gui.stroke(_early_domain_color);
		if(showing_early_domain){
			_gui.fill(_early_domain_color);
		}else{
			_gui.noFill();
		}
		_gui.rect(rect.x, rect.y, rect.width, rect.height);
		//text
		_gui.fill(120);
		_gui.textAlign(LEFT, CENTER);
		_gui.text("EARLY", rect.x+rect.width + 5, (float)rect.getCenterY());
	}

	//draw color pallet
	//sequential
	_gui.fill(120);
	_gui.textAlign(LEFT, TOP);
	_gui.text(gui_columns[3], gui_label_x[3], gui_label_y);
	for(int i= 0; i < color_pallet.length; i++){
		Rectangle r = color_pallet[i];
		int col = _color_array[i];
		_gui.fill(col);
		_gui.stroke(col);
		_gui.strokeWeight(1);
		_gui.rect(r.x, r.y, r.width, r.height);
	}

	//reporesntaion
	_gui.fill(120);
	_gui.textAlign(LEFT, TOP);
	_gui.text(gui_columns[4], gui_label_x[4], gui_label_y);
	for(int i =0; i<encoding_labels.length; i++){
		_gui.noFill();
		_gui.stroke(120);
		_gui.rect(encoding_btns[i].x, encoding_btns[i].y, encoding_btns[i].width, encoding_btns[i].height);
		if(encoding_selected[i]){
			//draw cross
			_gui.line(encoding_btns[i].x, encoding_btns[i].y,encoding_btns[i].x+encoding_btns[i].width, encoding_btns[i].y +encoding_btns[i].height);
			_gui.line(encoding_btns[i].x+encoding_btns[i].width, encoding_btns[i].y ,encoding_btns[i].x, encoding_btns[i].y +encoding_btns[i].height);
		}
		_gui.fill(120);
		_gui.textAlign(LEFT, CENTER);
		_gui.text(encoding_labels[i], encoding_btns[i].x+encoding_btns[i].width + 5, (float)encoding_btns[i].getCenterY());
	}

	//stacking
	_gui.fill(120);
	_gui.textAlign(LEFT, TOP);
	_gui.text(gui_columns[5], gui_label_x[5], gui_label_y);
	for(int i =0; i<stacking_labels.length; i++){
		_gui.noFill();
		_gui.stroke(120);
		_gui.rect(stacking_btns[i].x, stacking_btns[i].y, stacking_btns[i].width, stacking_btns[i].height);
		if(isStacking){
			//draw cross
			_gui.line(stacking_btns[i].x, stacking_btns[i].y,stacking_btns[i].x+stacking_btns[i].width, stacking_btns[i].y +stacking_btns[i].height);
			_gui.line(stacking_btns[i].x+stacking_btns[i].width, stacking_btns[i].y ,stacking_btns[i].x, stacking_btns[i].y +stacking_btns[i].height);
		}
		_gui.fill(120);
		_gui.textAlign(LEFT, CENTER);
		_gui.text(stacking_labels[i], stacking_btns[i].x+stacking_btns[i].width + 5, (float)stacking_btns[i].getCenterY());
	}



	//output
	//file name
	_gui.fill(240);
	if(output_file_name_active){
		_gui.stroke(180);
		_gui.strokeWeight(1);
	}else{
		_gui.noStroke();	
	}
	_gui.rect(output_file_name.x, output_file_name.y, output_file_name.width, output_file_name.height);
	_gui.fill(80);
	_gui.textAlign(LEFT, CENTER);
	_gui.text(output_file_url, output_file_name.x+2, (float) output_file_name.getCenterY());

	//test labels
	_gui.fill(120);
	_gui.textAlign(RIGHT, CENTER);
	_gui.text("FILE NAME: ", output_file_name.x, (float)output_file_name.getCenterY());
	_gui.text("OUTPUT: ", output_label_x[0], output_label_y + _MARGIN/2);
	_gui.textAlign(LEFT, TOP);
	_gui.text(output_header[0], output_label_x[0], output_label_y);
	_gui.text(output_header[1], output_label_x[1], output_label_y);
	_gui.text(output_header[2], output_label_x[2], output_label_y);

	//note
	_gui.fill(240);
	if(output_note_active){
		_gui.stroke(180);
		_gui.strokeWeight(1);
	}else{
		_gui.noStroke();
	}
	_gui.rect(output_note_rect.x, output_note_rect.y, output_note_rect.width, output_note_rect.height);
	//note input
	_gui.fill(color_magenta);
	_gui.textAlign(LEFT, CENTER);
	_gui.text(output_note_input, output_note_rect.x +2, (float)output_note_rect.getCenterY());



	_gui.fill(120);
	_gui.textAlign(RIGHT, CENTER);
	_gui.text("NOTE: ", output_note_rect.x, (float)output_note_rect.getCenterY());

	//output button
	_gui.fill(120);
	_gui.noStroke();
	_gui.rect(output_btn.x, output_btn.y, output_btn.width, output_btn.height);
	_gui.fill(255);
	_gui.textAlign(CENTER, CENTER);
	_gui.text("SAVE", (float)output_btn.getCenterX(), (float)output_btn.getCenterY());

	if(region_selected){
		_gui.fill(color_magenta);
		_gui.textAlign(LEFT, TOP);
		String chr = chrKeyArray.get(_selected_chr_index).replace("hs", "chr");
		_gui.text(chr, output_label_x[0], output_label_y+ _MARGIN);
		_gui.text(region_left_bp, output_label_x[1], output_label_y+ _MARGIN);
		_gui.text(region_right_bp, output_label_x[2], output_label_y+ _MARGIN);
	}


	_gui.endDraw();
}
//Karyotype
PGraphics _k;
int _karyotype_x1, _karyotype_x2;
int _karyotype_y1, _karyotype_y2;
int _karyotype_height = _MARGIN*2;
int _karyotype_margin = _MARGIN*2;

Rectangle _k_d_rect;//display


public void setupKaryotypeView(){
	_k = createGraphics(_chr_width, _chr_height);
	_karyotype_x1 = _SIDE_MARGIN;
	_karyotype_x2 = _chr_width - _RIGHT_MARGIN;
	_karyotype_y1 = _karyotype_margin;
	_karyotype_y2 = _karyotype_y1 +_karyotype_height; 

	_k_d_rect = new Rectangle( _karyotype_x1, _chr_dy+_karyotype_y1, _top_plot_width, _karyotype_height);

}

public void updateKaryotypeView(){
	_k.beginDraw();
	_k.background(200);
	Chromosome chr = _selected_chr;
	int chr_x1 = _karyotype_x1;
	int chr_x2 = _karyotype_x2;
	int chr_y1 = _karyotype_y1;
	int chr_y2 = _karyotype_y2;

	//cytoband
	_k.rectMode(CORNERS);
	for(Cytoband cb:chr.cytobands){
		float cb_start = map(cb.start, chr.start, chr.end, chr_x1, chr_x2);
		float cb_end = map(cb.end, chr.start, chr.end, chr_x1, chr_x2);
		_k.fill(255-cb.fill);
		_k.noStroke();
		_k.rect(cb_start, chr_y1, cb_end, chr_y2);
	}
	_k.rectMode(CORNER);

	//find centromere
	int acen_start_bp = chr.startAcen.start;
	int acen_middle_bp = (chr.endAcen.end + chr.startAcen.start)/2;
	int acen_end_bp = chr.endAcen.end;
	float acen_start_dx = map(acen_start_bp,chr.start, chr.end, chr_x1, chr_x2);
	float acen_middle_dx = map(acen_middle_bp, chr.start, chr.end, chr_x1, chr_x2);
	float acen_end_dx = map(acen_end_bp, chr.start, chr.end, chr_x1, chr_x2);

	//hide centromere background
	_k.fill(200);
	_k.noStroke();
	//top bit
	_k.beginShape();
	_k.vertex(acen_start_dx, chr_y1);
	_k.vertex(acen_middle_dx, chr_y1 + 5);
	_k.vertex(acen_end_dx, chr_y1);
	_k.vertex(acen_start_dx, chr_y1);
	_k.endShape();
	//bottom bit
	_k.beginShape();
	_k.vertex(acen_end_dx, chr_y2);
	_k.vertex(acen_middle_dx, chr_y2 - 5);
	_k.vertex(acen_start_dx, chr_y2);
	_k.vertex(acen_end_dx, chr_y2);
	_k.endShape();


	//outline
	_k.noFill();
	_k.stroke(120);
	_k.strokeWeight(2);
	_k.beginShape();
	_k.vertex(chr_x1, chr_y1);
	_k.vertex(acen_start_dx, chr_y1);
	_k.vertex(acen_middle_dx, chr_y1 + 5);
	_k.vertex(acen_end_dx, chr_y1);
	_k.vertex(chr_x2, chr_y1);
	_k.vertex(chr_x2, chr_y2);
	_k.vertex(acen_end_dx, chr_y2);
	_k.vertex(acen_middle_dx, chr_y2 - 5);
	_k.vertex(acen_start_dx, chr_y2);
	_k.vertex(chr_x1, chr_y2);
	_k.vertex(chr_x1, chr_y1);
	_k.endShape();

	_k.endDraw();
}

public void redrawKaryotype(){
	_k.clear();
	updateKaryotypeView();
}
class Range implements Comparable<Range>{
	int chr;
	int start;
	int end;
	float value;


	Range(int chr, int start, int end, float value){
		if(start <= end){
			this.chr = chr;
			this.start = start;
			this.end = end;
			this.value = value;
		}else{
			println("error: "+chr+" "+start+" "+end+" "+value);
			throw new RuntimeException("invalid range");
		}
			
	}

	Range(int start, int end){
		if(start <= end){
			this.start = start;
			this.end = end;
		}else throw new RuntimeException("invalid range");
	}
	public boolean intersects(Range that){
		if (that.end < this.start) return false;
        if (this.end < that.start) return false;
        return true;
	}

	public boolean contains(int chr, int x) {
		if(chr == this.chr){
	        return (this.start <= x) && (x <= this.end);
		}else{
			return false;
		}
    }


	public boolean contains(int x) {
        return (this.start <= x) && (x <= this.end);
    }

    public int compareTo(Range that) {
        if      (this.start  < that.start)  return -1;
        else if (this.start  > that.start)  return +1;
        else if (this.end < that.end) return -1;
        else if (this.end < that.end) return +1;
        else                            return  0;
    }


}
class Sample{
	int index;
	String label;
	PGraphics _raw;
	PGraphics _seg; //segmentation

	int dataPointCounter = 0;
	int numOfDotsPerFrame = 100;

	//dot 
	int stroke_color = color(120, 120);
	int line_alpha = 255;
	float outline_weight = 1;
	int seg_alpha = 255;
	int diameter = 4;

	//Rectangle
	Rectangle logR_rect;
	Rectangle cn_rect;

	//Segment information
	HashMap<String, ArrayList<Segment>> logSegMap;
	HashMap<String, ArrayList<Segment>> cnSegMap;
	Segment prev_segment = null;
	Segment prev_segment_cn = null;
	String current_chr =""; //for loading
	String current_chr_cn = ""; //for loading
	boolean segUpdated = false;



	//to display or not
	boolean isShowing = true;

	//stacking value
	float stacking = 0;




	Sample(int index, String label){
		this.index = index;
		this.label = label;

		this.logSegMap = new HashMap<String, ArrayList<Segment>>();
		this.cnSegMap = new HashMap<String, ArrayList<Segment>>();
	}


	public void setupDisplay(){
		// println("debug: setupDiplay() for sample:"+index);
		_raw = createGraphics(_scatter_width, _scatter_height);
		_seg = createGraphics(_scatter_width, _scatter_height);
		logR_rect = new Rectangle(_top_plot_x, _top_plot_y - _scatter_dy, _top_plot_width, _top_plot_height);
		cn_rect = new Rectangle(_bottom_plot_x, _bottom_plot_y - _scatter_dy, _bottom_plot_width, _bottom_plot_height);
	}	



	public boolean updateRawDataView(){
		// println("debug: udpateRowDataView() for sample:"+index +"  datapointCounter ="+dataPointCounter);
		boolean updated = false;
		_raw.beginDraw();
		_raw.smooth();
		if(dataPointCounter < _selected_chr.rows.size()){
			updated = true;
			//draw dots
			int currentIndex = dataPointCounter;
			int endIndex =  min(currentIndex + numOfDotsPerFrame, _selected_chr.rows.size());

			if(encoding_selected[0]){
				//ellipse
				_raw.strokeWeight(outline_weight);
				_raw.stroke(stroke_color, line_alpha);
				_raw.noFill();
				_raw.ellipseMode(CENTER);
				boolean drewSomething = false;
				for(int i = currentIndex; i < endIndex; i++){
					DataRow dr = _selected_chr.rows.get(i);
					int pos = dr.position;
					if(_start_pos  <= pos  && pos <= _end_pos){
						//LogR
						float logR_value  = dr.logR[index];
						float dx = map(pos, _start_pos, _end_pos, logR_rect.x, logR_rect.x+logR_rect.width);
						float log_dy = constrain(map(logR_value, _min_logR, _max_logR, logR_rect.y+logR_rect.height, logR_rect.y), logR_rect.y,logR_rect.y+logR_rect.height);
						_raw.ellipse(dx, log_dy, diameter, diameter);
						//Copy umber
						float cn_value = dr.copyNumber[index];
						float cn_dy = constrain(map(cn_value, _min_copyNumber, _max_copyNumber, cn_rect.y+cn_rect.height, cn_rect.y), cn_rect.y, cn_rect.y+cn_rect.height);
						_raw.ellipse(dx, cn_dy, diameter, diameter);	
						//flag
						drewSomething = true;				
					}
					dataPointCounter++;
				}
			}else if(encoding_selected[1]){
				//dot
				_raw.strokeWeight(5);
				_raw.stroke(stroke_color, line_alpha);
				_raw.noFill();
				// _raw.beginShape();
				boolean drewSomething = false;
				for(int i = currentIndex; i < endIndex; i++){
					DataRow dr = _selected_chr.rows.get(i);
					int pos = dr.position;
					if(_start_pos  <= pos  && pos <= _end_pos){
						//LogR
						float logR_value  = dr.logR[index];
						float dx = map(pos, _start_pos, _end_pos, logR_rect.x, logR_rect.x+logR_rect.width);
						float log_dy = constrain(map(logR_value, _min_logR, _max_logR, logR_rect.y+logR_rect.height, logR_rect.y), logR_rect.y,logR_rect.y+logR_rect.height);
						_raw.point(dx, log_dy);
						//Copy umber
						float cn_value = dr.copyNumber[index];
						float cn_dy = constrain(map(cn_value, _min_copyNumber, _max_copyNumber, cn_rect.y+cn_rect.height, cn_rect.y), cn_rect.y, cn_rect.y+cn_rect.height);
						_raw.point(dx, cn_dy);
						//flag
						drewSomething = true;				
					}
					dataPointCounter++;
				}
			}else if(encoding_selected[2]){
				//line
				_raw.strokeWeight(outline_weight);
				_raw.stroke(stroke_color, line_alpha);
				_raw.noFill();

				//LogR
				_raw.beginShape();
				boolean drewSomething = false;
				for(int i = currentIndex; i < endIndex; i++){
					DataRow dr = _selected_chr.rows.get(i);
					int pos = dr.position;
					if(_start_pos  <= pos  && pos <= _end_pos){
						float logR_value  = dr.logR[index];
						float dx = map(pos, _start_pos, _end_pos, logR_rect.x, logR_rect.x+logR_rect.width);
						float log_dy = constrain(map(logR_value, _min_logR, _max_logR, logR_rect.y+logR_rect.height, logR_rect.y), logR_rect.y,logR_rect.y+logR_rect.height);
						_raw.vertex(dx, log_dy);
						//flag
						drewSomething = true;				
					}
					dataPointCounter++;
				}
				_raw.endShape();

				//copy number
				_raw.beginShape();
				for(int i = currentIndex; i < endIndex; i++){
					DataRow dr = _selected_chr.rows.get(i);
					int pos = dr.position;
					if(_start_pos  <= pos  && pos <= _end_pos){
						float cn_value = dr.copyNumber[index];
						float dx = map(pos, _start_pos, _end_pos, logR_rect.x, logR_rect.x+logR_rect.width);
						float cn_dy = constrain(map(cn_value, _min_copyNumber, _max_copyNumber, cn_rect.y+cn_rect.height, cn_rect.y), cn_rect.y, cn_rect.y+cn_rect.height);
						_raw.vertex(dx, cn_dy);
					}
				}
				_raw.endShape();
			}
		}else{
			//finished drawing
		}	


		_raw.endDraw();
		return updated;
	}

	public boolean updateSegDataView(){
		if(segUpdated){
			return false;
		}else{
			_seg.beginDraw();
			_seg.smooth();
			//LogR
			ArrayList<Segment> segments = (ArrayList<Segment>)logSegMap.get(_selected_chr.key);
			// println("debug: "+label+" has "+segments.size()+" segments");
			//draw connected
			_seg.stroke(stroke_color, seg_alpha);
			_seg.strokeCap(SQUARE);
			_seg.noFill();
			_seg.beginShape();
			float prev_dy = 0;
			if(segments != null){
				for(int i = 0; i<segments.size(); i++){
					Segment seg = segments.get(i);
					// println("debug:"+index+":"+seg.toString());
					float start_dx = map(seg.start, _start_pos, _end_pos, logR_rect.x, logR_rect.x+logR_rect.width);
					float end_dx = map(seg.end, _start_pos, _end_pos, logR_rect.x, logR_rect.x+logR_rect.width);
					float dy = constrain(map(seg.value, _min_logR, _max_logR, logR_rect.y+logR_rect.height, logR_rect.y), logR_rect.y,logR_rect.y+logR_rect.height);
					_seg.strokeWeight(seg_line_weight);
					_seg.line(start_dx, dy, end_dx, dy);

					if(i==0){
						prev_dy = dy;
					}else{
						_seg.strokeWeight(1);
						_seg.line(start_dx, prev_dy, start_dx, dy);
						prev_dy = dy;
					}
				}
			}
			_seg.endShape();

			//Copy number
			segments = (ArrayList<Segment>)cnSegMap.get(_selected_chr.key);
			// println("debug: "+label+" has "+segments.size()+" cn segments");
			//draw connected
			_seg.stroke(stroke_color,seg_alpha);
			_seg.strokeCap(SQUARE);
			_seg.noFill();
			_seg.beginShape();
			prev_dy = 0;
			if(segments != null){
				for(int i = 0; i<segments.size(); i++){
					Segment seg = segments.get(i);
					float start_dx = map(seg.start, _start_pos, _end_pos, cn_rect.x, cn_rect.x+cn_rect.width);
					float end_dx = map(seg.end, _start_pos, _end_pos, cn_rect.x, cn_rect.x+cn_rect.width);
					float dy = constrain(map(seg.value, _min_copyNumber, _max_copyNumber, cn_rect.y+cn_rect.height, cn_rect.y), cn_rect.y, cn_rect.y+cn_rect.height);
					
					if(isStacking){
						dy += stacking;
					}
					_seg.strokeWeight(seg_line_weight);
					_seg.line(start_dx, dy, end_dx, dy);

					if(i==0){
						prev_dy = dy;
					}else{
						_seg.strokeWeight(1);
						_seg.line(start_dx, prev_dy, start_dx, dy);
						prev_dy = dy;
					}
				}
			}
			_seg.endShape();


			_seg.endDraw();
			segUpdated = true;
			return true;
		}
	}




	//reset drawing counters
	public void redraw(){
		dataPointCounter = 0;
		segUpdated = false;
		//clear canvas
		_raw.clear();
		_seg.clear();
	}

	public void redraw_seg(){
		segUpdated = false;
		//clear canvas
		_seg.clear();
	}

	public void load_logR_seg(float value, DataRow dr, String chr_key){
		if(prev_segment == null){
			//first time
			// println("debug: load_logR_seg(): "+chr_key+" value="+value+ " pos ="+dr.position);
			prev_segment = new Segment(value);
			prev_segment.addRow(dr);
			current_chr = chr_key;
			ArrayList<Segment> segs = new ArrayList<Segment>();
			this.logSegMap.put(chr_key, segs);
			segs.add(prev_segment);
		}else{
			//check if it is same chromosome
			if(this.current_chr.equals(chr_key)){
				//same
				//check if the value is the same
				if(value == prev_segment.value){
					//same
					prev_segment.addRow(dr);
				}else{
					//different value
					prev_segment.end = dr.position-1; //save the end of position
					prev_segment = new Segment(value);
					prev_segment.addRow(dr);
					ArrayList<Segment> segs = (ArrayList<Segment>)logSegMap.get(chr_key);
					segs.add(prev_segment);
				}
			}else{
				//different chromosome
				prev_segment = new Segment(value);
				prev_segment.addRow(dr);
				current_chr = chr_key;
				ArrayList<Segment> segs = new ArrayList<Segment>();
				segs.add(prev_segment);
				this.logSegMap.put(chr_key, segs);
			}
		}
	}

	public void load_cn_seg(float value, DataRow dr, String chr_key){
		if(prev_segment_cn == null){
			//first time
			prev_segment_cn = new Segment(value);
			prev_segment_cn.addRow(dr);
			current_chr_cn = chr_key;
			ArrayList<Segment> segs = new ArrayList<Segment>();
			this.cnSegMap.put(chr_key, segs);
			segs.add(prev_segment_cn);
		}else{
			//check if it is same chromosome
			if(this.current_chr_cn.equals(chr_key)){
				//same
				//check if the value is the same
				if(value == prev_segment_cn.value){
					//same
					prev_segment_cn.addRow(dr);
				}else{
					//different value
					prev_segment_cn.end = dr.position-1; //save the end of position
					prev_segment_cn = new Segment(value);
					prev_segment_cn.addRow(dr);
					ArrayList<Segment> segs = (ArrayList<Segment>)cnSegMap.get(chr_key);
					segs.add(prev_segment_cn);
				}
			}else{
				//different chromosome
				prev_segment_cn = new Segment(value);
				prev_segment_cn.addRow(dr);
				current_chr_cn = chr_key;
				ArrayList<Segment> segs = new ArrayList<Segment>();
				segs.add(prev_segment_cn);
				this.cnSegMap.put(chr_key, segs);
			}
		}
	}
}
class Segment{
	int start, end;
	ArrayList<DataRow> rows;
	float value;

	Segment(float value){
		this.value = value;
		this.rows = new ArrayList<DataRow>();
	}


	public void addRow(DataRow dr){
		this.rows.add(dr);
		if(this.rows.size()==1){
			this.start = dr.position;
			this.end = dr.position;
			// println("\taddRow(): new row to segment: "+dr.position);

		}else{
			this.end = max(this.end, dr.position);
		}
	}

	public String toString(){
		return "Segment:start="+start+" end="+end+" number of rows="+rows.size()+" value="+value;
	}
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "CellCyclePlot" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
