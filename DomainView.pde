//Domain view
PGraphics _domain;

// int _domain_bar_height = _MARGIN*2;
// int _domain_d_height = _domain_bar_height + _MARGIN*2;

int _domain_x1, _domain_x2;
int _domain_y1, _domain_y2;

int _late_domain_color, _early_domain_color;

boolean showing_late_domain = true;
boolean showing_early_domain = true;



void setupDomainView(){
	_domain = createGraphics(displayWidth, _domain_d_height);
	_domain_x1 = _SIDE_MARGIN;
	_domain_x2 = displayWidth - _RIGHT_MARGIN;
	_domain_y1 = _MARGIN;
	_domain_y2 = _domain_y1 + _domain_bar_height;

	//default color settin
	_late_domain_color = _color_gray_seq_2;
	_early_domain_color = _color_gray_seq_3;
}


void updateDomainView(){
	_domain.beginDraw();
	_domain.clear();
	Chromosome chr = _selected_chr;

	//backgound
	_domain.rectMode(CORNERS);
	_domain.fill(255);
	_domain.noStroke();
	_domain.rect(_domain_x1, _domain_y1, _domain_x2, _domain_y2);

	//domains
	for(Range range: chr.domains){
		float r_start = map(range.start, _start_pos, _end_pos, _domain_x1, _domain_x2);
		float r_end = map(range.end, _start_pos, _end_pos, _domain_x1, _domain_x2);
		_domain.fill(range.value == 0 ?_late_domain_color:_early_domain_color); //0 is late

		if(range.value == 0){
			//late
			if(showing_late_domain){
				_domain.fill(_late_domain_color);
				_domain.rect(r_start, _domain_y1, r_end, _domain_y2);
			}
		}else{
			//early
			if(showing_early_domain){
				_domain.fill(_early_domain_color);
				_domain.rect(r_start, _domain_y1, r_end, _domain_y2);
			}
		}

	}


	_domain.endDraw();
}
