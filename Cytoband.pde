class Cytoband{
	int start, end, fill;
	String bandID;
	boolean isCentromere = false;
	boolean is_p_end = false;
	boolean is_q_end = false;

	Cytoband(int start, int end, String bandID, int stainValue){
		this.start = start;
		this.end = end;
		this.bandID = bandID;
		this.fill = stainValue;
		if(fill == -1){
			//centromere
			isCentromere = true;
			fill = 0;
		}
	}
	//chech if intersect
	boolean intersects(int s, int e){
		if(e <=this.start) return false;
		if(this.end <= s) return false;
		return true;
	}
	boolean contains(int s){
		if(this.start <= s && s <= this.end){
			return true;
		}
		return false;
	}


	String toString(){
		return start+" "+end+" "+bandID;
	}
}
