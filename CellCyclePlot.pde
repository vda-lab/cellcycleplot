//tried loess curves

import java.awt.Rectangle;
import java.awt.GraphicsEnvironment;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Collections;
import java.util.Arrays;
import java.util.Calendar; 
import java.util.Comparator;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileOutputStream;
import java.text.DecimalFormat;



PFont font;
PFont label_font;

boolean filesSelected = false;
boolean isLoadingData = false;
boolean isPreprocessing = false;
boolean hasDomainFile = false;  //whether or not domain file is selected

boolean showingRawData = true;
boolean showingSegData = true;


boolean saved_output = false;



void setup(){
	font = loadFont("LucidaSans-12.vlw");
	label_font = loadFont("Supernatural1001-10.vlw");

	textFont(font, 12);
	frameRate(30);
	frame.setTitle("CellCyclePlot");	

	_STAGE_WIDTH = displayWidth;
	_STAGE_HEIGHT = displayHeight - 50; //total window height

	// Rectangle rect = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
	// _STAGE_WIDTH = rect.width;
	// _STAGE_HEIGHT = rect.height;

	size(_STAGE_WIDTH, _STAGE_HEIGHT);
	if(frame != null){
		frame.setResizable(true);
	}

	//setup data loading page
	setupDataLoadingPage();

	//load data
	if(isTestMode){
		filesSelected = true;
		isLoadingData = true;
		Runnable loadingFile = new FileLoader();
		new Thread(loadingFile).start();		
	}

}

String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$tm%1$td%1$tY", now);
}

String timestamp_detail() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$tm%1$td%1$ty_%1$tH%1$tM", now);
}

