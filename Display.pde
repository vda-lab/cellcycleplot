int _MARGIN = 10;
int _SIDE_MARGIN = _MARGIN*4;
int _RIGHT_MARGIN = _MARGIN*4;

int _STAGE_WIDTH, _STAGE_HEIGHT;

int _header_height = _MARGIN*3;
int _header_dx, _header_dy, _header_width;
Rectangle[] chr_btns;


//scatter plot section
int _scatter_dx;
int _scatter_dy;
int _scatter_height;
int _scatter_width;

int _top_plot_x;
int _top_plot_y;
int _top_plot_width;
int _top_plot_height;

int _bottom_plot_x;
int _bottom_plot_y;
int _bottom_plot_width;
int _bottom_plot_height;

int _plot_top_margin = _MARGIN*3;
int _plot_bottom_margin = _MARGIN;

int seg_line_weight = 5;

//domain track
int _domain_bar_height = _MARGIN*2;
int _domain_d_height = _domain_bar_height + _MARGIN*4;
int _domain_dx, _domain_dy; //display position


//chromosome vie
int _chr_width;
int _chr_height;
int _chr_dx, _chr_dy;

//controller
int _cont_dx, _cont_dy;
int _cont_dx_normal;
int _cont_dx_expanded;
int _cont_height, _cont_width;


//colours
int color_cyan = color(0, 174, 237);
int color_magenta = color(231, 41, 138);
//blue
int _color_b_seq_1 = color(189,215,231);
int _color_b_seq_2 = color(107,174,214);
int _color_b_seq_3 = color(49,130,189);
int _color_b_seq_4 = color(8,81,156);
int[] _color_b_seq = {_color_b_seq_1, _color_b_seq_2, _color_b_seq_3, _color_b_seq_4};
//green
int _color_g_seq_1 = color(186,228,179);
int _color_g_seq_2 = color(116,196,118);
int _color_g_seq_3 = color(49,163,84);
int _color_g_seq_4 = color(0,109,44);
int[] _color_g_seq = {_color_g_seq_1, _color_g_seq_2, _color_g_seq_3, _color_g_seq_4};
//red
int _color_r_seq_1 = color(252, 174, 145);
int _color_r_seq_2 = color(251, 106, 74);
int _color_r_seq_3 = color(222, 45, 38);
int _color_r_seq_4 = color(165, 15, 21);
int[] _color_r_seq = {_color_r_seq_1, _color_r_seq_2, _color_r_seq_3, _color_r_seq_4};

//gray
int _color_gray_seq_1 = color(0, 37);
int _color_gray_seq_2 = color(0, 99);
int _color_gray_seq_3 = color(0, 150);
int _color_gray_seq_4 = color(0, 204);

//purple
int _color_p_seq_1 = color(203,201,226);
int _color_p_seq_2 = color(158,154,200);
int _color_p_seq_3 = color(117,107,177);
int _color_p_seq_4 = color(84,39,143);

//orange
int _color_o_seq_1 = color(253,190,133);
int _color_o_seq_2 = color(253,141,60);
int _color_o_seq_3 = color(230,85,13);
int _color_o_seq_4 = color(166,54,3);
//

int[] _color_array = {
					  _color_g_seq_1, _color_g_seq_2, _color_g_seq_3, _color_g_seq_4, 
					  _color_b_seq_1, _color_b_seq_2, _color_b_seq_3, _color_b_seq_4,
					  _color_p_seq_1, _color_p_seq_2, _color_p_seq_3, _color_p_seq_4,
					  _color_r_seq_1, _color_r_seq_2, _color_r_seq_3, _color_r_seq_4,
					  _color_o_seq_1, _color_o_seq_2, _color_o_seq_3, _color_o_seq_4,
					  _color_gray_seq_1, _color_gray_seq_2, _color_gray_seq_3, _color_gray_seq_4};




//large number formatting
char[] c = new char[]{'k', 'm', 'b', 't'};
String[] suffix = new String[]{"","k", "m", "b", "t"};
int MAX_LENGTH = 4;


int _draw_counter = 0; //counter


void setupDisplay(){
	println("display size:"+displayWidth+" "+displayHeight);

	_header_dx = 0;
	_header_dy = 0;
	_header_width = displayWidth;

	//karyotype
	_chr_width = displayWidth;
	_chr_height = _karyotype_margin*2 + _karyotype_height;
	_chr_dx = 0;
	_chr_dy = _header_height;

	//determine ratio
	int remaining_height_pixel = _STAGE_HEIGHT - _header_height - _chr_height;

	//scatter plot section
	_scatter_dx = 0;
	_scatter_dy = _header_height + _chr_height;
	// _scatter_height = round(remaining_height_pixel*0.7);
	_scatter_height = remaining_height_pixel - _MARGIN*15; //configuration height
	_scatter_width = displayWidth;

	//check if domain is loaded
	int plot_height;
	if(hasDomainFile){
		int remaining = _scatter_height - _domain_d_height;
		plot_height  = remaining/2;
		_domain_dx = 0;
		_domain_dy = _header_height + _chr_height + plot_height*2;
	}else{
		plot_height = _scatter_height/2;
	}

	_top_plot_x = _SIDE_MARGIN;
	_top_plot_y = _scatter_dy +_plot_top_margin;
	_top_plot_width = _scatter_width -_SIDE_MARGIN -_RIGHT_MARGIN;
	_top_plot_height = plot_height - _plot_top_margin - _plot_bottom_margin; 

	_bottom_plot_x = _SIDE_MARGIN;
	_bottom_plot_y = _scatter_dy+plot_height+_plot_top_margin;
	_bottom_plot_width = _top_plot_width;
	_bottom_plot_height = _top_plot_height;

	//controller
	_cont_dx = 0;
	_cont_dy = _scatter_dy + _scatter_height;
	_cont_width = _STAGE_WIDTH;
	_cont_height = _STAGE_HEIGHT - _cont_dy;


	//header buttons
	int runningX = _SIDE_MARGIN;
	int runningY = 0;
	chr_btns = new Rectangle[chrKeyArray.size()];
	for(int i = 0; i< chrKeyArray.size(); i++){
		String label = chrKeyArray.get(i).replace("hs", "chr");
		int text_width = round(textWidth(label));
		chr_btns[i] = new Rectangle(runningX, runningY, text_width, _header_height);
		runningX += text_width +_MARGIN*2;
	}

	//update stacking
	update_stacking();


}

void draw(){
	background(240);
	// line(_chr_dx, _chr_dy, _chr_dx+_chr_width, _chr_dy);
	// println("top:"+_top_plot_x+","+_top_plot_y+","+_top_plot_width+","+_top_plot_height);
	if(!filesSelected){
		drawDataLoadingPage();
	}else{
		if(isLoadingData){
			//loading
			fill(180);
			textAlign(CENTER, CENTER);
			text("Loading Data ... ", displayWidth/2, displayHeight/2);
		}else if(isPreprocessing){
			fill(180);
			textAlign(CENTER, CENTER);
			text("Processing Data ... ", displayWidth/2, displayHeight/2);
		}else{
			//header area
			fill(180);
			noStroke();
			rect(_header_dx, _header_dy, _header_width, _header_height);
			//header text
			drawHeader();
			
			//karyotype
			image(_k, _chr_dx, _chr_dy);

			//plot area
			fill(255);
			noStroke();
			rectMode(CORNER);
			rect(_top_plot_x, _top_plot_y, _top_plot_width, _top_plot_height);
			rect(_bottom_plot_x, _bottom_plot_y, _bottom_plot_width, _bottom_plot_height);
			
			//drawing grid
			drawGrid();

			// //update each sample view and add to screen
			boolean updated = false;

			for(int i = 0; i < samples.length; i++){
			// for(int i = 0; i < 1; i++){
				Sample s = samples[i];
				if(s.isShowing){
					if(showingRawData){
						updated |= s.updateRawDataView();
						image(s._raw, _scatter_dx, _scatter_dy);
					}
					if(showingSegData){
						updated |= s.updateSegDataView();
						image(s._seg, _scatter_dx, _scatter_dy);  
					}
				}
				// s.updateLoessDataView();
			}
			//domain
			if(hasDomainFile){
				image(_domain, _domain_dx, _domain_dy);
			}

			//GUI
			image(_gui, _cont_dx, _cont_dy);


			//draw label and overflow
			drawLabelAndOverflow();

			drawInteraction();

			//
			drawMousePosition();


			drawMessage();

			if(!updated){
				_draw_counter ++;
				if(_draw_counter > 10){
					// println("debug: pause loop ------");
					noLoop();
				}
			}
		}
		
	}
}

void drawGrid(){
	//logR
	//y axis
	int tick_count = 6;
	float span_float = _max_logR - _min_logR;
	float step_int = pow(10, floor(log(span_float/tick_count)/log(10)));
	float err_int = (float)tick_count/(float)span_float * step_int;

	if(err_int <= 0.15){
		step_int *= 10;
	}else if(err_int <= 0.35){
		step_int *= 5;
	}else if(err_int <= 0.75){
		step_int *= 2;
	}
	// println("step ="+step_int+"  err="+err_int );
	float extent_1_int = ceil(_min_logR/step_int)*step_int;
	float extent_2_int = floor(_max_logR/step_int)*step_int+step_int*0.5;

	//font
	textFont(label_font, 10);

	for(float f = extent_1_int; f < extent_2_int; f+=step_int){
		float dy = map(f, _min_logR, _max_logR,  _top_plot_y+_top_plot_height, _top_plot_y);
		stroke(240);
		strokeWeight(1);
		noFill();
		line(_top_plot_x, dy, _top_plot_x+_top_plot_width, dy);
		//tick value
		// fill(120);
		// textAlign(RIGHT, CENTER);
		// text(nf(f, 0, 1), _top_plot_x, dy);
	}

	//copy number
	tick_count = 6;
	span_float = _max_copyNumber - _min_copyNumber;
	step_int = pow(10, floor(log(span_float/tick_count)/log(10)));
	err_int = (float)tick_count/(float)span_float * step_int;

	if(err_int <= 0.15){
		step_int *= 10;
	}else if(err_int <= 0.35){
		step_int *= 5;
	}else if(err_int <= 0.75){
		step_int *= 2;
	}
	// println("step ="+step_int+"  err="+err_int );
	extent_1_int = ceil(_min_copyNumber/step_int)*step_int;
	extent_2_int = floor(_max_copyNumber/step_int)*step_int+step_int*0.5;

	//font
	textFont(label_font, 10);

	for(float f = extent_1_int; f < extent_2_int; f+=step_int){
		float dy = map(f, _min_copyNumber, _max_copyNumber,  _bottom_plot_y+_bottom_plot_height, _bottom_plot_y);
		stroke(240);
		strokeWeight(1);
		noFill();
		line(_bottom_plot_x, dy, _bottom_plot_x+_bottom_plot_width, dy);
		//tick value
		// fill(120);
		// textAlign(RIGHT, CENTER);
		// text(nf(f, 0, 0), _bottom_plot_x, dy);
	}
	//x axis
	// tick_count = 10;
	// int span_int = _end_pos - _start_pos;
	// step_int = pow(10, floor(log(span_int/tick_count)/log(10)));
	// err_int = (float)tick_count/(float)span_int * step_int;


	// if(err_int <= 0.15){
	// 	step_int *= 10;
	// }else if(err_int <= 0.35){
	// 	step_int *= 5;
	// }else if(err_int <= 0.75){
	// 	step_int *= 2;
	// }
	// // println("step ="+step_int+"  err="+err_int );
	// extent_1_int = ceil(_start_pos/step_int)*step_int;
	// extent_2_int = floor(_end_pos/step_int)*step_int+step_int*0.5;
	// for(float f = extent_1_int; f < extent_2_int; f+=step_int){
	// 	float dx = map(f, _start_pos, _end_pos,  _top_plot_x, _top_plot_x +_top_plot_width);
	// 	stroke(220);
	// 	strokeWeight(1);
	// 	noFill();
	// 	line(dx, _top_plot_y, dx, _top_plot_y+_top_plot_height);
	// 	//tick value
	// 	fill(120);
	// 	textAlign(CENTER, TOP);
	// 	text(largeNumberFormat(f,0), dx, _top_plot_y+_top_plot_height);
	// }

	//set font back
	textFont(font, 12);
}

void drawLabelAndOverflow(){
	//over flow
	noStroke();
	fill(240);
	rect(_scatter_dx, _scatter_dy, _SIDE_MARGIN, _scatter_height);
	rect(_top_plot_x+_top_plot_width, _scatter_dy, _SIDE_MARGIN, _scatter_height);

	//Labels
	//logR
	//y axis
	int tick_count = 6;
	float span_float = _max_logR - _min_logR;
	float step_int = pow(10, floor(log(span_float/tick_count)/log(10)));
	float err_int = (float)tick_count/(float)span_float * step_int;

	if(err_int <= 0.15){
		step_int *= 10;
	}else if(err_int <= 0.35){
		step_int *= 5;
	}else if(err_int <= 0.75){
		step_int *= 2;
	}
	// println("step ="+step_int+"  err="+err_int );
	float extent_1_int = ceil(_min_logR/step_int)*step_int;
	float extent_2_int = floor(_max_logR/step_int)*step_int+step_int*0.5;

	//font
	textFont(label_font, 10);

	for(float f = extent_1_int; f < extent_2_int; f+=step_int){
		float dy = map(f, _min_logR, _max_logR,  _top_plot_y+_top_plot_height, _top_plot_y);
		//tick value
		fill(120);
		textAlign(RIGHT, CENTER);
		text(nf(f, 0, 1), _top_plot_x, dy);
	}

	//copy number
	tick_count = 6;
	span_float = _max_copyNumber - _min_copyNumber;
	step_int = pow(10, floor(log(span_float/tick_count)/log(10)));
	err_int = (float)tick_count/(float)span_float * step_int;

	if(err_int <= 0.15){
		step_int *= 10;
	}else if(err_int <= 0.35){
		step_int *= 5;
	}else if(err_int <= 0.75){
		step_int *= 2;
	}
	// println("step ="+step_int+"  err="+err_int );
	extent_1_int = ceil(_min_copyNumber/step_int)*step_int;
	extent_2_int = floor(_max_copyNumber/step_int)*step_int+step_int*0.5;

	//font
	textFont(label_font, 10);

	for(float f = extent_1_int; f < extent_2_int; f+=step_int){
		float dy = map(f, _min_copyNumber, _max_copyNumber,  _bottom_plot_y+_bottom_plot_height, _bottom_plot_y);
		//tick value
		fill(120);
		textAlign(RIGHT, CENTER);
		text(nf(f, 0, 0), _bottom_plot_x, dy);
	}

	//labels
	textAlign(CENTER, TOP);
	pushMatrix();
	translate(0  + _MARGIN, _top_plot_y+_top_plot_height/2);
	rotate(-HALF_PI);
	text("LogR", 0, 0);
	popMatrix();
	textAlign(CENTER, TOP);
	pushMatrix();
	translate(0  + _MARGIN, _bottom_plot_y+_bottom_plot_height/2);
	rotate(-HALF_PI);
	text("Copy Number", 0, 0);
	popMatrix();

	if(hasDomainFile){
		textAlign(LEFT, TOP);
		pushMatrix();
		translate(0  + _MARGIN, _domain_dy+_MARGIN+_domain_bar_height);
		rotate(-HALF_PI);
		text("Domain", 0, 0);
		popMatrix();
	}
}


void drawHeader(){
	//header background
	textAlign(CENTER, CENTER);
	for(int i = 0; i< chrKeyArray.size(); i++){
		String label = chrKeyArray.get(i).replace("hs", "chr");
		Rectangle rect = chr_btns[i];
		if(i == _selected_chr_index){
			fill(20);
		}else{
			fill(255);
		}
		text(label, (float)rect.getCenterX(), (float)rect.getCenterY());
	}
}

void drawInteraction(){
	//font
	textFont(label_font, 10);
	//draw handles
	String left_cb = _selected_chr.getCytobandName(_start_pos);
	String right_cb = _selected_chr.getCytobandName(_end_pos);

	//left
	fill(80);
	textAlign(RIGHT, BOTTOM);
	text(left_cb, left_handle_x -2, _k_d_rect.y);
	//bp
	text(largeNumberFormat(_start_pos), left_handle_x-2, _k_d_rect.y+_k_d_rect.height+_MARGIN+2);


	//right
	textAlign(LEFT, BOTTOM);
	text(right_cb, right_handle_x +2, _k_d_rect.y);
	//bp
	text(largeNumberFormat(_end_pos), right_handle_x +2, _k_d_rect.y+_k_d_rect.height+_MARGIN+2);

	//selected region
	if(_start_pos > _selected_chr.start || _end_pos < _selected_chr.end){
		//draw selected region
		stroke(255);
		strokeWeight(3);
		fill(255, 60);
		rectMode(CORNERS);
		rect(left_handle_x, _k_d_rect.y-3, right_handle_x, _k_d_rect.y+_k_d_rect.height+3);
		rectMode(CORNER);
	}


	//hovering
	if(hovering_l_handle){
		stroke(color_cyan);
		strokeWeight(3);
		line(left_handle_x, _k_d_rect.y, left_handle_x, _k_d_rect.y+_k_d_rect.height);
	}else if(hovering_r_handle){
		stroke(color_cyan);
		strokeWeight(3);
		line(right_handle_x, _k_d_rect.y, right_handle_x, _k_d_rect.y+_k_d_rect.height);
	}

	//dragging color
	if(color_picked){
		rectMode(CENTER);
		fill(_color_array[picked_color_index]);
		stroke(_color_array[picked_color_index]);
		rect(mouseX, mouseY, _MARGIN, _MARGIN);
		rectMode(CORNER);
	}

	//set font back
	textFont(font, 12);
}

void drawMousePosition(){
	textFont(label_font, 10);
	if(region_selected){
		//draw highlighted region
		noFill();
		stroke(color_magenta);
		strokeWeight(1);
		line(region_left, _scatter_dy, region_left, _scatter_dy+_scatter_height);
		line(region_right, _scatter_dy, region_right, _scatter_dy+_scatter_height);

		fill(color_magenta);
		textAlign(RIGHT, BOTTOM);
		text(region_left_bp, region_left, _scatter_dy+_scatter_height);
		textAlign(LEFT, BOTTOM);
		text(region_right_bp, region_right+2, _scatter_dy+_scatter_height);

		//length
		int length = region_right_bp - region_left_bp;
		float mid_point = (region_left+region_right)/2;
		textAlign(CENTER, TOP);
		text(largeNumberFormat(length)+" bp", mid_point, _scatter_dy+_scatter_height+2);

	}else{
		//hoverling action
		if(_scatter_dy < mouseY  && mouseY < _scatter_dy+_scatter_height){
			if(_top_plot_x < mouseX && mouseX < _top_plot_x+_top_plot_width){
				noFill();
				stroke(color_magenta);
				strokeWeight(1);
				line(mouseX, _scatter_dy, mouseX, _scatter_dy+_scatter_height);

				int current_bp = getBpPosition(mouseX);//round(map(mouseX, _top_plot_x, _top_plot_x+_top_plot_width, _start_pos,_end_pos));
				fill(color_magenta);
				textAlign(RIGHT, BOTTOM);
				text(current_bp, mouseX, _scatter_dy+_scatter_height);
			}

		}
		
	}
	textFont(font, 12);
}


String largeNumberFormat(double n, int iteration){
	// double d = ((long) n / 100) / 10.0;
	// boolean isRound = (d * 10) %10 == 0;//true if the decimal part is equal to 0 (then it's trimmed anyway)
	
	double d = ((long) n / 100) / 10.0;
	boolean isRound = (d * 10) %10 == 0;//true if the decimal part is equal to 0 (then it's trimmed anyway)
		return (d < 1000? //this determines the class, i.e. 'k', 'm' etc
	        ((d > 99.9 || isRound || (!isRound && d > 9.99)? //this decides whether to trim the decimals
	         (int) d * 10 / 10 : d + "" // (int) d * 10 / 10 drops the decimal
	         ) + "" + c[iteration]) 
	        : largeNumberFormat((float)d, iteration+1));
}

String largeNumberFormat(double number) {
    String r = new DecimalFormat("##0E0").format(number);
    r = r.replaceAll("E[0-9]", suffix[Character.getNumericValue(r.charAt(r.length()-1))/3]);
    return r.length()>MAX_LENGTH ?  r.replaceAll("\\.[0-9]+", "") : r;
}


void drawMessage(){
	if(saved_output){
		fill(180);
		textAlign(LEFT, TOP);
		// textFont(label_font, 10);
		text("saved note", output_x, _cont_dy+output_btn.y);
		// textFont(font, 12);
	}
}

void redraw_seg(){
	for(int i = 0; i < samples.length; i++){
		Sample s = samples[i];
		if(s.isShowing){
			s.redraw_seg();
		}
	}
}


