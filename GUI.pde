//legend  and options
PGraphics _gui;
boolean gui_expanded = false;

Rectangle[] datatype_btns; //seg or raw
String[]  datatypes_labels = {"SEG", "RAW"};
int gap_bw_btns = 2;

String[] gui_columns = {"TYPES", "SAMPLES","DOMAIN", "COLOUR", "REPRESENTATION", "STACKING"};
int[] gui_label_x;
int gui_label_y;
int gui_label_height = 12;

Rectangle[] sample_btns;
Rectangle[] domain_btns;
Rectangle[] color_pallet;

String[] encoding_labels = {"ELLIPSE", "DOT", "LINE"};
boolean[] encoding_selected = {true, false, false};
Rectangle[] encoding_btns;

//stacking
String[] stacking_labels = {"CN"};
Rectangle[] stacking_btns;


//output 
Rectangle output_file_name;
String[] output_header= {"chr", "start", "stop", "note"};
Rectangle output_note_rect;
Rectangle output_btn;
int output_x;
int[] output_label_x;
int output_label_y;

boolean output_file_name_active = false;
boolean output_note_active = false;
String output_file_url= "";
String output_note_input = "";



void setupGUI(){
	_gui = createGraphics(_cont_width, _cont_height);
	//set up button positions etc...
	//data type
	int runningX = _SIDE_MARGIN;
	int runningY = _MARGIN*2; //header;
	gui_label_x = new int[gui_columns.length];
	//data types
	gui_label_x[0] = runningX;
	gui_label_y = runningY;
	runningY += gui_label_height;

	datatype_btns = new Rectangle[2];
	datatype_btns[0] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
	runningY += _MARGIN + gap_bw_btns;
	datatype_btns[1] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
	runningY += _MARGIN + gap_bw_btns;

	//sample buttons
	runningX += round(textWidth(gui_columns[0]))+_MARGIN*2;
	runningY = _MARGIN*2+gui_label_height;
	gui_label_x[1] = runningX;
	sample_btns = new Rectangle[samples.length];
	for(int i = 0; i < samples.length;i++){
		if(i%6 == 0 && i >0){ //more than 6 samples
			runningX += round(textWidth(gui_columns[1]));
			runningY = _MARGIN*2+gui_label_height;
		}
		sample_btns[i] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
		runningY += _MARGIN + gap_bw_btns;
	}

	//domain buttons
	if(hasDomainFile){
		runningX += round(textWidth(gui_columns[1]))+_MARGIN*2;
		runningY = _MARGIN*2+gui_label_height;
		gui_label_x[2] = runningX;
		domain_btns = new Rectangle[2];
		domain_btns[0] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
		runningY += _MARGIN + gap_bw_btns;
		domain_btns[1] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
	}


	//color pallet
	color_pallet = new Rectangle[_color_array.length];
	if(hasDomainFile){
		runningX += round(textWidth(gui_columns[2]))+_MARGIN*2;
	}else{
		runningX += round(textWidth(gui_columns[1]))+_MARGIN*2;		
	}
	runningY = _MARGIN*2+gui_label_height;
	gui_label_x[3] = runningX;

	int indexCounter = 0;
	int initial_x = runningX;
	//sequential 6 sets
	for(int i = 0 ; i < 6; i++){
		//4 colours
		for(int j = 0; j<4; j++){
			color_pallet[indexCounter] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
			runningX += _MARGIN+gap_bw_btns;
			indexCounter ++;
		}
		//next row
		runningX = initial_x;
		runningY += _MARGIN+gap_bw_btns;
	}

	//encoding
	runningX += round(textWidth(gui_columns[3]))+_MARGIN*2;
	runningY = _MARGIN*2+gui_label_height;
	gui_label_x[4] = runningX;
	encoding_btns = new Rectangle[encoding_labels.length];
	for(int i = 0; i < encoding_labels.length; i++){
		encoding_btns[i] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
		runningY += _MARGIN+gap_bw_btns;
	}

	//stacking
	runningX += round(textWidth(gui_columns[4]));
	runningY = _MARGIN*2+gui_label_height;
	gui_label_x[5] = runningX;
	stacking_btns = new Rectangle[stacking_labels.length];
	for(int i = 0; i < stacking_labels.length; i++){
		stacking_btns[i] = new Rectangle(runningX, runningY, _MARGIN, _MARGIN);
		runningY += _MARGIN+gap_bw_btns;
	}

	//output file name
	runningX = _STAGE_WIDTH - _SIDE_MARGIN - fileName_width;
	runningY = _MARGIN*2;
	output_x = runningX;
	output_file_name = new Rectangle(runningX, runningY, fileName_width, round(_MARGIN*1.5));
	
	//ouptut
	runningY += _MARGIN*2;
	output_label_x = new int[3];
	output_label_x[0] = runningX;
	runningX += _MARGIN*5;
	output_label_x[1] = runningX;
	runningX += _MARGIN*10;
	output_label_x[2] = runningX;
	output_label_y = runningY;

	runningX = _STAGE_WIDTH - _SIDE_MARGIN - fileName_width;
	runningY += _MARGIN*2.5;

	//note
	output_note_rect = new Rectangle(runningX, runningY, fileName_width, round(_MARGIN*1.5));
	runningY+= _MARGIN*2 ;

	//output button
	runningX = _STAGE_WIDTH - _SIDE_MARGIN - btn_width;
	output_btn = new Rectangle(runningX, runningY, btn_width, btn_height);

	//output file neme
	output_file_url = "analysis_"+timestamp();

}

void updateGUI(){
	_gui.beginDraw();
	_gui.clear();
	_gui.background(220);
	//font
	_gui.textFont(label_font, 10);
	//datatype buttons
	_gui.fill(120);
	_gui.textAlign(LEFT, TOP);
	_gui.text(gui_columns[0], gui_label_x[0], gui_label_y);
	//segmentation
	_gui.noFill();
	_gui.stroke(120);
	_gui.rect(datatype_btns[0].x, datatype_btns[0].y, datatype_btns[0].width, datatype_btns[0].height);
	if(showingSegData){
		//draw cross
		_gui.line(datatype_btns[0].x, datatype_btns[0].y,datatype_btns[0].x+datatype_btns[0].width, datatype_btns[0].y +datatype_btns[0].height);
		_gui.line(datatype_btns[0].x+datatype_btns[0].width, datatype_btns[0].y ,datatype_btns[0].x, datatype_btns[0].y +datatype_btns[0].height);
	}
	_gui.fill(120);
	_gui.textAlign(LEFT, CENTER);
	_gui.text(datatypes_labels[0], datatype_btns[0].x+datatype_btns[0].width + 5, (float)datatype_btns[0].getCenterY());

	//logR
	_gui.noFill();
	_gui.stroke(120);
	_gui.rect(datatype_btns[1].x, datatype_btns[1].y, datatype_btns[1].width, datatype_btns[1].height);
	if(showingRawData){
		//draw cross
		_gui.line(datatype_btns[1].x, datatype_btns[1].y,datatype_btns[1].x+datatype_btns[1].width, datatype_btns[1].y +datatype_btns[1].height);
		_gui.line(datatype_btns[1].x+datatype_btns[1].width, datatype_btns[1].y ,datatype_btns[1].x, datatype_btns[1].y +datatype_btns[1].height);
	}
	_gui.fill(120);
	_gui.textAlign(LEFT, CENTER);
	_gui.text(datatypes_labels[1], datatype_btns[1].x+datatype_btns[1].width + 5, (float)datatype_btns[1].getCenterY());


	//sample buttons
	_gui.fill(120);
	_gui.textAlign(LEFT, TOP);
	_gui.text(gui_columns[1], gui_label_x[1], gui_label_y);
	for(int i = 0; i < samples.length;i++){
		Sample s = samples[i];
		Rectangle rect = sample_btns[i];
		_gui.stroke(s.stroke_color);
		if(s.isShowing){
			_gui.fill(s.stroke_color);
		}else{
			_gui.noFill();
		}
		_gui.rect(rect.x, rect.y, rect.width, rect.height);
		//text
		_gui.fill(120);
		_gui.textAlign(LEFT, CENTER);
		_gui.text(s.label, rect.x+rect.width + 5, (float)rect.getCenterY());
	}
	//domain buttons
	if(hasDomainFile){
		_gui.fill(120);
		_gui.textAlign(LEFT, TOP);
		_gui.text(gui_columns[2], gui_label_x[2], gui_label_y);
		Rectangle rect = domain_btns[0];
		_gui.stroke(_late_domain_color);
		if(showing_late_domain){
			_gui.fill(_late_domain_color);
		}else{
			_gui.noFill();
		}
		_gui.rect(rect.x, rect.y, rect.width, rect.height);
		//text
		_gui.fill(120);
		_gui.textAlign(LEFT, CENTER);
		_gui.text("LATE", rect.x+rect.width + 5, (float)rect.getCenterY());

		//early domain
		rect = domain_btns[1];
		_gui.stroke(_early_domain_color);
		if(showing_early_domain){
			_gui.fill(_early_domain_color);
		}else{
			_gui.noFill();
		}
		_gui.rect(rect.x, rect.y, rect.width, rect.height);
		//text
		_gui.fill(120);
		_gui.textAlign(LEFT, CENTER);
		_gui.text("EARLY", rect.x+rect.width + 5, (float)rect.getCenterY());
	}

	//draw color pallet
	//sequential
	_gui.fill(120);
	_gui.textAlign(LEFT, TOP);
	_gui.text(gui_columns[3], gui_label_x[3], gui_label_y);
	for(int i= 0; i < color_pallet.length; i++){
		Rectangle r = color_pallet[i];
		int col = _color_array[i];
		_gui.fill(col);
		_gui.stroke(col);
		_gui.strokeWeight(1);
		_gui.rect(r.x, r.y, r.width, r.height);
	}

	//reporesntaion
	_gui.fill(120);
	_gui.textAlign(LEFT, TOP);
	_gui.text(gui_columns[4], gui_label_x[4], gui_label_y);
	for(int i =0; i<encoding_labels.length; i++){
		_gui.noFill();
		_gui.stroke(120);
		_gui.rect(encoding_btns[i].x, encoding_btns[i].y, encoding_btns[i].width, encoding_btns[i].height);
		if(encoding_selected[i]){
			//draw cross
			_gui.line(encoding_btns[i].x, encoding_btns[i].y,encoding_btns[i].x+encoding_btns[i].width, encoding_btns[i].y +encoding_btns[i].height);
			_gui.line(encoding_btns[i].x+encoding_btns[i].width, encoding_btns[i].y ,encoding_btns[i].x, encoding_btns[i].y +encoding_btns[i].height);
		}
		_gui.fill(120);
		_gui.textAlign(LEFT, CENTER);
		_gui.text(encoding_labels[i], encoding_btns[i].x+encoding_btns[i].width + 5, (float)encoding_btns[i].getCenterY());
	}

	//stacking
	_gui.fill(120);
	_gui.textAlign(LEFT, TOP);
	_gui.text(gui_columns[5], gui_label_x[5], gui_label_y);
	for(int i =0; i<stacking_labels.length; i++){
		_gui.noFill();
		_gui.stroke(120);
		_gui.rect(stacking_btns[i].x, stacking_btns[i].y, stacking_btns[i].width, stacking_btns[i].height);
		if(isStacking){
			//draw cross
			_gui.line(stacking_btns[i].x, stacking_btns[i].y,stacking_btns[i].x+stacking_btns[i].width, stacking_btns[i].y +stacking_btns[i].height);
			_gui.line(stacking_btns[i].x+stacking_btns[i].width, stacking_btns[i].y ,stacking_btns[i].x, stacking_btns[i].y +stacking_btns[i].height);
		}
		_gui.fill(120);
		_gui.textAlign(LEFT, CENTER);
		_gui.text(stacking_labels[i], stacking_btns[i].x+stacking_btns[i].width + 5, (float)stacking_btns[i].getCenterY());
	}



	//output
	//file name
	_gui.fill(240);
	if(output_file_name_active){
		_gui.stroke(180);
		_gui.strokeWeight(1);
	}else{
		_gui.noStroke();	
	}
	_gui.rect(output_file_name.x, output_file_name.y, output_file_name.width, output_file_name.height);
	_gui.fill(80);
	_gui.textAlign(LEFT, CENTER);
	_gui.text(output_file_url, output_file_name.x+2, (float) output_file_name.getCenterY());

	//test labels
	_gui.fill(120);
	_gui.textAlign(RIGHT, CENTER);
	_gui.text("FILE NAME: ", output_file_name.x, (float)output_file_name.getCenterY());
	_gui.text("OUTPUT: ", output_label_x[0], output_label_y + _MARGIN/2);
	_gui.textAlign(LEFT, TOP);
	_gui.text(output_header[0], output_label_x[0], output_label_y);
	_gui.text(output_header[1], output_label_x[1], output_label_y);
	_gui.text(output_header[2], output_label_x[2], output_label_y);

	//note
	_gui.fill(240);
	if(output_note_active){
		_gui.stroke(180);
		_gui.strokeWeight(1);
	}else{
		_gui.noStroke();
	}
	_gui.rect(output_note_rect.x, output_note_rect.y, output_note_rect.width, output_note_rect.height);
	//note input
	_gui.fill(color_magenta);
	_gui.textAlign(LEFT, CENTER);
	_gui.text(output_note_input, output_note_rect.x +2, (float)output_note_rect.getCenterY());



	_gui.fill(120);
	_gui.textAlign(RIGHT, CENTER);
	_gui.text("NOTE: ", output_note_rect.x, (float)output_note_rect.getCenterY());

	//output button
	_gui.fill(120);
	_gui.noStroke();
	_gui.rect(output_btn.x, output_btn.y, output_btn.width, output_btn.height);
	_gui.fill(255);
	_gui.textAlign(CENTER, CENTER);
	_gui.text("SAVE", (float)output_btn.getCenterX(), (float)output_btn.getCenterY());

	if(region_selected){
		_gui.fill(color_magenta);
		_gui.textAlign(LEFT, TOP);
		String chr = chrKeyArray.get(_selected_chr_index).replace("hs", "chr");
		_gui.text(chr, output_label_x[0], output_label_y+ _MARGIN);
		_gui.text(region_left_bp, output_label_x[1], output_label_y+ _MARGIN);
		_gui.text(region_right_bp, output_label_x[2], output_label_y+ _MARGIN);
	}


	_gui.endDraw();
}
