class Chromosome{
	String key, label; //(key = hs1, label = chr1)
	int start, end;
	ArrayList<Cytoband> cytobands;
	Cytoband startAcen, endAcen;

	ArrayList<DataRow> rows;
	HashMap<Integer, DataRow> dataRowMap;


	ArrayList<Range> domains;

	Chromosome(String key, String label, int start, int end){
		this.key = key;
		this.label = label;
		this.start = start;
		this.end = end;
		this.cytobands = new ArrayList<Cytoband>();
		this.rows = new ArrayList<DataRow>();
		this.dataRowMap = new HashMap<Integer, DataRow>();

		this.domains = new ArrayList<Range>();
	}

	//copy constructor
	Chromosome(Chromosome that){
		this.key = that.key;
		this.label = that.label;
		this.start = that.start;
		this.end = that.end;
		this.cytobands = that.cytobands;
	}


	void add_cytoband(Cytoband cb){
		this.cytobands.add(cb);
	}


	String getCytobandName(int pos){
		for(Cytoband cb: cytobands){
			if(cb.contains(pos)){
				return cb.bandID;
			}
		}
		println("finidng position error: pos="+pos +" end of chromosome ="+end);
		return "???";
	}
} 
