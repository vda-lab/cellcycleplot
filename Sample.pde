class Sample{
	int index;
	String label;
	PGraphics _raw;
	PGraphics _seg; //segmentation

	int dataPointCounter = 0;
	int numOfDotsPerFrame = 100;

	//dot 
	int stroke_color = color(120, 120);
	int line_alpha = 255;
	float outline_weight = 1;
	int seg_alpha = 255;
	int diameter = 4;

	//Rectangle
	Rectangle logR_rect;
	Rectangle cn_rect;

	//Segment information
	HashMap<String, ArrayList<Segment>> logSegMap;
	HashMap<String, ArrayList<Segment>> cnSegMap;
	Segment prev_segment = null;
	Segment prev_segment_cn = null;
	String current_chr =""; //for loading
	String current_chr_cn = ""; //for loading
	boolean segUpdated = false;



	//to display or not
	boolean isShowing = true;

	//stacking value
	float stacking = 0;




	Sample(int index, String label){
		this.index = index;
		this.label = label;

		this.logSegMap = new HashMap<String, ArrayList<Segment>>();
		this.cnSegMap = new HashMap<String, ArrayList<Segment>>();
	}


	void setupDisplay(){
		// println("debug: setupDiplay() for sample:"+index);
		_raw = createGraphics(_scatter_width, _scatter_height);
		_seg = createGraphics(_scatter_width, _scatter_height);
		logR_rect = new Rectangle(_top_plot_x, _top_plot_y - _scatter_dy, _top_plot_width, _top_plot_height);
		cn_rect = new Rectangle(_bottom_plot_x, _bottom_plot_y - _scatter_dy, _bottom_plot_width, _bottom_plot_height);
	}	



	boolean updateRawDataView(){
		// println("debug: udpateRowDataView() for sample:"+index +"  datapointCounter ="+dataPointCounter);
		boolean updated = false;
		_raw.beginDraw();
		_raw.smooth();
		if(dataPointCounter < _selected_chr.rows.size()){
			updated = true;
			//draw dots
			int currentIndex = dataPointCounter;
			int endIndex =  min(currentIndex + numOfDotsPerFrame, _selected_chr.rows.size());

			if(encoding_selected[0]){
				//ellipse
				_raw.strokeWeight(outline_weight);
				_raw.stroke(stroke_color, line_alpha);
				_raw.noFill();
				_raw.ellipseMode(CENTER);
				boolean drewSomething = false;
				for(int i = currentIndex; i < endIndex; i++){
					DataRow dr = _selected_chr.rows.get(i);
					int pos = dr.position;
					if(_start_pos  <= pos  && pos <= _end_pos){
						//LogR
						float logR_value  = dr.logR[index];
						float dx = map(pos, _start_pos, _end_pos, logR_rect.x, logR_rect.x+logR_rect.width);
						float log_dy = constrain(map(logR_value, _min_logR, _max_logR, logR_rect.y+logR_rect.height, logR_rect.y), logR_rect.y,logR_rect.y+logR_rect.height);
						_raw.ellipse(dx, log_dy, diameter, diameter);
						//Copy umber
						float cn_value = dr.copyNumber[index];
						float cn_dy = constrain(map(cn_value, _min_copyNumber, _max_copyNumber, cn_rect.y+cn_rect.height, cn_rect.y), cn_rect.y, cn_rect.y+cn_rect.height);
						_raw.ellipse(dx, cn_dy, diameter, diameter);	
						//flag
						drewSomething = true;				
					}
					dataPointCounter++;
				}
			}else if(encoding_selected[1]){
				//dot
				_raw.strokeWeight(5);
				_raw.stroke(stroke_color, line_alpha);
				_raw.noFill();
				// _raw.beginShape();
				boolean drewSomething = false;
				for(int i = currentIndex; i < endIndex; i++){
					DataRow dr = _selected_chr.rows.get(i);
					int pos = dr.position;
					if(_start_pos  <= pos  && pos <= _end_pos){
						//LogR
						float logR_value  = dr.logR[index];
						float dx = map(pos, _start_pos, _end_pos, logR_rect.x, logR_rect.x+logR_rect.width);
						float log_dy = constrain(map(logR_value, _min_logR, _max_logR, logR_rect.y+logR_rect.height, logR_rect.y), logR_rect.y,logR_rect.y+logR_rect.height);
						_raw.point(dx, log_dy);
						//Copy umber
						float cn_value = dr.copyNumber[index];
						float cn_dy = constrain(map(cn_value, _min_copyNumber, _max_copyNumber, cn_rect.y+cn_rect.height, cn_rect.y), cn_rect.y, cn_rect.y+cn_rect.height);
						_raw.point(dx, cn_dy);
						//flag
						drewSomething = true;				
					}
					dataPointCounter++;
				}
			}else if(encoding_selected[2]){
				//line
				_raw.strokeWeight(outline_weight);
				_raw.stroke(stroke_color, line_alpha);
				_raw.noFill();

				//LogR
				_raw.beginShape();
				boolean drewSomething = false;
				for(int i = currentIndex; i < endIndex; i++){
					DataRow dr = _selected_chr.rows.get(i);
					int pos = dr.position;
					if(_start_pos  <= pos  && pos <= _end_pos){
						float logR_value  = dr.logR[index];
						float dx = map(pos, _start_pos, _end_pos, logR_rect.x, logR_rect.x+logR_rect.width);
						float log_dy = constrain(map(logR_value, _min_logR, _max_logR, logR_rect.y+logR_rect.height, logR_rect.y), logR_rect.y,logR_rect.y+logR_rect.height);
						_raw.vertex(dx, log_dy);
						//flag
						drewSomething = true;				
					}
					dataPointCounter++;
				}
				_raw.endShape();

				//copy number
				_raw.beginShape();
				for(int i = currentIndex; i < endIndex; i++){
					DataRow dr = _selected_chr.rows.get(i);
					int pos = dr.position;
					if(_start_pos  <= pos  && pos <= _end_pos){
						float cn_value = dr.copyNumber[index];
						float dx = map(pos, _start_pos, _end_pos, logR_rect.x, logR_rect.x+logR_rect.width);
						float cn_dy = constrain(map(cn_value, _min_copyNumber, _max_copyNumber, cn_rect.y+cn_rect.height, cn_rect.y), cn_rect.y, cn_rect.y+cn_rect.height);
						_raw.vertex(dx, cn_dy);
					}
				}
				_raw.endShape();
			}
		}else{
			//finished drawing
		}	


		_raw.endDraw();
		return updated;
	}

	boolean updateSegDataView(){
		if(segUpdated){
			return false;
		}else{
			_seg.beginDraw();
			_seg.smooth();
			//LogR
			ArrayList<Segment> segments = (ArrayList<Segment>)logSegMap.get(_selected_chr.key);
			// println("debug: "+label+" has "+segments.size()+" segments");
			//draw connected
			_seg.stroke(stroke_color, seg_alpha);
			_seg.strokeCap(SQUARE);
			_seg.noFill();
			_seg.beginShape();
			float prev_dy = 0;
			if(segments != null){
				for(int i = 0; i<segments.size(); i++){
					Segment seg = segments.get(i);
					// println("debug:"+index+":"+seg.toString());
					float start_dx = map(seg.start, _start_pos, _end_pos, logR_rect.x, logR_rect.x+logR_rect.width);
					float end_dx = map(seg.end, _start_pos, _end_pos, logR_rect.x, logR_rect.x+logR_rect.width);
					float dy = constrain(map(seg.value, _min_logR, _max_logR, logR_rect.y+logR_rect.height, logR_rect.y), logR_rect.y,logR_rect.y+logR_rect.height);
					_seg.strokeWeight(seg_line_weight);
					_seg.line(start_dx, dy, end_dx, dy);

					if(i==0){
						prev_dy = dy;
					}else{
						_seg.strokeWeight(1);
						_seg.line(start_dx, prev_dy, start_dx, dy);
						prev_dy = dy;
					}
				}
			}
			_seg.endShape();

			//Copy number
			segments = (ArrayList<Segment>)cnSegMap.get(_selected_chr.key);
			// println("debug: "+label+" has "+segments.size()+" cn segments");
			//draw connected
			_seg.stroke(stroke_color,seg_alpha);
			_seg.strokeCap(SQUARE);
			_seg.noFill();
			_seg.beginShape();
			prev_dy = 0;
			if(segments != null){
				for(int i = 0; i<segments.size(); i++){
					Segment seg = segments.get(i);
					float start_dx = map(seg.start, _start_pos, _end_pos, cn_rect.x, cn_rect.x+cn_rect.width);
					float end_dx = map(seg.end, _start_pos, _end_pos, cn_rect.x, cn_rect.x+cn_rect.width);
					float dy = constrain(map(seg.value, _min_copyNumber, _max_copyNumber, cn_rect.y+cn_rect.height, cn_rect.y), cn_rect.y, cn_rect.y+cn_rect.height);
					
					if(isStacking){
						dy += stacking;
					}
					_seg.strokeWeight(seg_line_weight);
					_seg.line(start_dx, dy, end_dx, dy);

					if(i==0){
						prev_dy = dy;
					}else{
						_seg.strokeWeight(1);
						_seg.line(start_dx, prev_dy, start_dx, dy);
						prev_dy = dy;
					}
				}
			}
			_seg.endShape();


			_seg.endDraw();
			segUpdated = true;
			return true;
		}
	}




	//reset drawing counters
	void redraw(){
		dataPointCounter = 0;
		segUpdated = false;
		//clear canvas
		_raw.clear();
		_seg.clear();
	}

	void redraw_seg(){
		segUpdated = false;
		//clear canvas
		_seg.clear();
	}

	void load_logR_seg(float value, DataRow dr, String chr_key){
		if(prev_segment == null){
			//first time
			// println("debug: load_logR_seg(): "+chr_key+" value="+value+ " pos ="+dr.position);
			prev_segment = new Segment(value);
			prev_segment.addRow(dr);
			current_chr = chr_key;
			ArrayList<Segment> segs = new ArrayList<Segment>();
			this.logSegMap.put(chr_key, segs);
			segs.add(prev_segment);
		}else{
			//check if it is same chromosome
			if(this.current_chr.equals(chr_key)){
				//same
				//check if the value is the same
				if(value == prev_segment.value){
					//same
					prev_segment.addRow(dr);
				}else{
					//different value
					prev_segment.end = dr.position-1; //save the end of position
					prev_segment = new Segment(value);
					prev_segment.addRow(dr);
					ArrayList<Segment> segs = (ArrayList<Segment>)logSegMap.get(chr_key);
					segs.add(prev_segment);
				}
			}else{
				//different chromosome
				prev_segment = new Segment(value);
				prev_segment.addRow(dr);
				current_chr = chr_key;
				ArrayList<Segment> segs = new ArrayList<Segment>();
				segs.add(prev_segment);
				this.logSegMap.put(chr_key, segs);
			}
		}
	}

	void load_cn_seg(float value, DataRow dr, String chr_key){
		if(prev_segment_cn == null){
			//first time
			prev_segment_cn = new Segment(value);
			prev_segment_cn.addRow(dr);
			current_chr_cn = chr_key;
			ArrayList<Segment> segs = new ArrayList<Segment>();
			this.cnSegMap.put(chr_key, segs);
			segs.add(prev_segment_cn);
		}else{
			//check if it is same chromosome
			if(this.current_chr_cn.equals(chr_key)){
				//same
				//check if the value is the same
				if(value == prev_segment_cn.value){
					//same
					prev_segment_cn.addRow(dr);
				}else{
					//different value
					prev_segment_cn.end = dr.position-1; //save the end of position
					prev_segment_cn = new Segment(value);
					prev_segment_cn.addRow(dr);
					ArrayList<Segment> segs = (ArrayList<Segment>)cnSegMap.get(chr_key);
					segs.add(prev_segment_cn);
				}
			}else{
				//different chromosome
				prev_segment_cn = new Segment(value);
				prev_segment_cn.addRow(dr);
				current_chr_cn = chr_key;
				ArrayList<Segment> segs = new ArrayList<Segment>();
				segs.add(prev_segment_cn);
				this.cnSegMap.put(chr_key, segs);
			}
		}
	}
}
