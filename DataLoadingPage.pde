String dl_t_1 = "Number of samples:";
String[] dl_labels = {"Sample Name", "LogR", "Copy Number"};
String[] dl_btn_labels ={"Select", "Load"};


int numOfSamples = 6;

int dl_t_1_x, dl_t_1_y;
int[] dl_labels_x;
int dl_labels_y;

int btn_width;
int btn_height;
int sampleName_width;
int fileName_width;

Rectangle sampleCount_rect;
ArrayList<Rectangle[]> dl_rectangles;
Rectangle load_btn;

//data input
String[] sample_names;
File[] sample_log_file;
File[] sample_cn_file;
String sample_count_input = "6";
File domain_file;
File conf_file;


boolean sampleCount_highlight = false;
boolean sampleName_highlight = false;
int sampleName_hightlight_index = -1;
int sample_selected_index = -1;

String domain_label = "Optional: Domain File";
Rectangle domain_file_name_rect;
Rectangle domain_file_btn;

String configuration_label = "Optional: Configuration File";
Rectangle conf_file_name_rect;
Rectangle conf_file_btn;

String configuration_save ="Save the configuration";
Rectangle conf_save_name_rect;
Rectangle conf_save_btn;
String conf_save_name ="";



void setupDataLoadingPage(){
	//reset input valuses
	sample_names = new String[numOfSamples];
	sample_log_file = new File[numOfSamples];
	sample_cn_file = new File[numOfSamples];

	Arrays.fill(sample_names,"");
	Arrays.fill(sample_log_file,null);
	Arrays.fill(sample_cn_file,null);

	btn_width = _MARGIN*10;
	btn_height = _MARGIN*2;
	sampleName_width = _MARGIN*10;
	fileName_width = _MARGIN*30;

	int runningX = _SIDE_MARGIN + _MARGIN*2;
	int runningY = _MARGIN*8;

	dl_t_1_x = runningX;
	dl_t_1_y = runningY;

	float textWidth = textWidth(dl_t_1);
	runningX += textWidth + _MARGIN;
	sampleCount_rect = new Rectangle(runningX, runningY, btn_width, btn_height);

	//rectangles
	runningX = _SIDE_MARGIN;
	runningY += btn_height +_MARGIN;
	//label position
	dl_labels_y = runningY;
	Rectangle[] rects = new Rectangle[5];
	//numbering
	runningX += _MARGIN*2;
	runningY += _MARGIN*2;
	rects[0] = new Rectangle(runningX, runningY, sampleName_width, btn_height);
	runningX += sampleName_width+_MARGIN*3;
	rects[1] = new Rectangle(runningX, runningY, fileName_width, btn_height);
	runningX += fileName_width + _MARGIN;
	rects[2] = new Rectangle(runningX, runningY, btn_width, btn_height);
	runningX += btn_width +_MARGIN*3;
	rects[3] = new Rectangle(runningX, runningY, fileName_width, btn_height);
	runningX += fileName_width + _MARGIN;
	rects[4] = new Rectangle(runningX, runningY, btn_width, btn_height);


	dl_rectangles = new ArrayList<Rectangle[]>();
	for(int i = 0; i < numOfSamples; i++){
		Rectangle[] rectangles = new Rectangle[5];
		rectangles[0] = new Rectangle(rects[0].x, runningY, sampleName_width, btn_height);
		rectangles[1] = new Rectangle(rects[1].x, runningY, fileName_width, btn_height);
		rectangles[2] = new Rectangle(rects[2].x, runningY, btn_width, btn_height);
		rectangles[3] = new Rectangle(rects[3].x, runningY, fileName_width, btn_height);
		rectangles[4] = new Rectangle(rects[4].x, runningY, btn_width, btn_height);
		dl_rectangles.add(rectangles);
		runningY += btn_height +_MARGIN;
	}
	
	dl_labels_x = new int[3];
	dl_labels_x[0] = rects[0].x; //sample name
	dl_labels_x[1] = rects[1].x; //logR
	dl_labels_x[2] = rects[3].x; //copy number

	//domain file
	runningY += _MARGIN*3;
	domain_file_name_rect = new Rectangle(rects[0].x, runningY, fileName_width+_MARGIN*2, btn_height);
	domain_file_btn = new Rectangle(rects[0].x+fileName_width+_MARGIN*3, runningY, btn_width, btn_height);
	runningY += btn_height + _MARGIN;
	//conf file
	runningY += _MARGIN*3;
	conf_file_name_rect = new Rectangle(rects[0].x, runningY, fileName_width+_MARGIN*2, btn_height);
	conf_file_btn = new Rectangle(rects[0].x+fileName_width+_MARGIN*3, runningY, btn_width, btn_height);
	runningY += btn_height + _MARGIN;

	//conf save
	runningY += _MARGIN*3;
	conf_save_name_rect = new Rectangle(rects[0].x, runningY, fileName_width+_MARGIN*2, btn_height);
	conf_save_btn = new Rectangle(rects[0].x+fileName_width+_MARGIN*3, runningY, btn_width, btn_height);
	//file name
	conf_save_name = "conf_"+timestamp_detail();
	// println("debug:configuration file name :"+conf_save_name);



	//load btn
	load_btn = new Rectangle(rects[4].x, runningY+_MARGIN, btn_width, btn_height);
}


void drawDataLoadingPage(){
	background(240);
	//number of sample
	fill(120);
	textAlign(LEFT, CENTER);
	text(dl_t_1, dl_t_1_x, (float)sampleCount_rect.getCenterY());
	//text box
	fill(255);
	if(sampleCount_highlight){
		strokeWeight(2);
		stroke(color_cyan);
	}else{
		strokeWeight(1);
		stroke(180);
	}
	rect(sampleCount_rect.x, sampleCount_rect.y, sampleCount_rect.width, sampleCount_rect.height);
	//number of samples
	fill(120);
	textAlign(RIGHT, CENTER);
	text(sample_count_input, sampleCount_rect.x+sampleCount_rect.width -4, (float)sampleCount_rect.getCenterY());

	//labels
	fill(120);
	textAlign(LEFT, TOP);
	for(int i = 0; i < dl_labels.length; i++){
		text(dl_labels[i], dl_labels_x[i], dl_labels_y);
	}

	//Rectangles
	for(int i = 0; i< dl_rectangles.size(); i++){
		Rectangle[] array = dl_rectangles.get(i);
		//text box
		fill(255);
		stroke(180);
		strokeWeight(1);
		rect(array[0].x, array[0].y, array[0].width, array[0].height);
		rect(array[1].x, array[1].y, array[1].width, array[1].height);
		rect(array[3].x, array[3].y, array[3].width, array[3].height);
		//buttons
		fill(180);
		noStroke();
		rect(array[2].x, array[2].y, array[2].width, array[2].height);
		rect(array[4].x, array[4].y, array[4].width, array[4].height);
		rect(load_btn.x, load_btn.y, load_btn.width, load_btn.height);
		//text
		fill(255);
		textAlign(CENTER, CENTER);
		text(dl_btn_labels[0], (float)array[2].getCenterX(), (float)array[2].getCenterY());
		text(dl_btn_labels[0], (float)array[4].getCenterX(), (float)array[4].getCenterY());
		text(dl_btn_labels[1], (float)load_btn.getCenterX(), (float)load_btn.getCenterY());
		
		//sample name
		fill(120);
		textAlign(LEFT, CENTER);
		text(sample_names[i], array[0].x +4, (float)array[2].getCenterY());
		

		//row number
		fill(120);
		textAlign(RIGHT, CENTER);
		text((i+1), array[0].x -4, (float)array[0].getCenterY());

		if(sampleName_highlight && i == sampleName_hightlight_index){
			noFill();
			stroke(color_cyan);
			strokeWeight(2);
			rect(array[0].x, array[0].y, array[0].width, array[0].height);
		}

		//logR file
		if(sample_log_file[i] != null){
			fill(120);
			textAlign(LEFT, CENTER);
			String file_url = sample_log_file[i].getAbsolutePath();
			if(file_url.length() > 40){
				file_url = "..."+file_url.substring((file_url.length()-37), (file_url.length()));
			}
			text(file_url, array[1].x+4, (float)array[1].getCenterY());
		}
		if(sample_cn_file[i] != null){
			fill(120);
			textAlign(LEFT, CENTER);
			String file_url = sample_cn_file[i].getAbsolutePath();
			if(file_url.length() > 40){
				file_url = "..."+file_url.substring((file_url.length()-37), (file_url.length()));
			}
			text(file_url, array[3].x+4, (float)array[3].getCenterY());
		}
	}

	//domain file
	fill(120);
	textAlign(LEFT, TOP);
	text(domain_label, domain_file_name_rect.x, domain_file_name_rect.y - _MARGIN*2);
	fill(255);
	stroke(180);
	strokeWeight(1);
	rect(domain_file_name_rect.x, domain_file_name_rect.y, domain_file_name_rect.width, domain_file_name_rect.height);	
	fill(180);
	noStroke();
	rect(domain_file_btn.x, domain_file_btn.y, domain_file_btn.width, domain_file_btn.height);
	//text
	fill(255);
	textAlign(CENTER, CENTER);
	text(dl_btn_labels[0], (float)domain_file_btn.getCenterX(), (float)domain_file_btn.getCenterY());
	//file name
	if(domain_file != null){
		fill(120);
		String file_url = domain_file.getAbsolutePath();
		if(file_url.length() > 40){
			file_url = "..."+file_url.substring((file_url.length()-37), (file_url.length()));
		}
		textAlign(LEFT, CENTER);
		text(file_url, domain_file_name_rect.x+4, (float)domain_file_name_rect.getCenterY());
	}


	//conf file
	fill(120);
	textAlign(LEFT, TOP);
	text(configuration_label, conf_file_name_rect.x, conf_file_name_rect.y - _MARGIN*2);
	fill(255);
	stroke(180);
	strokeWeight(1);
	rect(conf_file_name_rect.x, conf_file_name_rect.y, conf_file_name_rect.width, conf_file_name_rect.height);	
	fill(180);
	noStroke();
	rect(conf_file_btn.x, conf_file_btn.y, conf_file_btn.width, conf_file_btn.height);
	//btn text
	fill(255);
	textAlign(CENTER, CENTER);
	text(dl_btn_labels[0], (float)conf_file_btn.getCenterX(), (float)conf_file_btn.getCenterY());
	if(conf_file != null){
		fill(120);
		String file_url = conf_file.getAbsolutePath();
		if(file_url.length() > 40){
			file_url = "..."+file_url.substring((file_url.length()-37), (file_url.length()));
		}
		textAlign(LEFT, CENTER);
		text(file_url, conf_file_name_rect.x+4, (float)conf_file_name_rect.getCenterY());
	}

	//conf_save
	fill(120);
	textAlign(LEFT, TOP);
	text(configuration_save, conf_save_name_rect.x, conf_save_name_rect.y -_MARGIN*2);
	fill(255);
	stroke(180);
	strokeWeight(1);
	rect(conf_save_name_rect.x, conf_save_name_rect.y, conf_save_name_rect.width, conf_save_name_rect.height);
	fill(180);
	noStroke();
	rect(conf_save_btn.x, conf_save_btn.y, conf_save_btn.width, conf_save_btn.height);
	fill(255);
	textAlign(CENTER, CENTER);
	text("Save", (float)conf_save_btn.getCenterX(), (float)conf_save_btn.getCenterY());
	//conf_save file name
	fill(120);
	textAlign(LEFT, CENTER);
	text(conf_save_name, conf_save_name_rect.x+4, (float)conf_save_name_rect.getCenterY());
}


