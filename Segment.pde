class Segment{
	int start, end;
	ArrayList<DataRow> rows;
	float value;

	Segment(float value){
		this.value = value;
		this.rows = new ArrayList<DataRow>();
	}


	void addRow(DataRow dr){
		this.rows.add(dr);
		if(this.rows.size()==1){
			this.start = dr.position;
			this.end = dr.position;
			// println("\taddRow(): new row to segment: "+dr.position);

		}else{
			this.end = max(this.end, dr.position);
		}
	}

	String toString(){
		return "Segment:start="+start+" end="+end+" number of rows="+rows.size()+" value="+value;
	}
}
